<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('THEME_AUTH', 'default');
define('THEME_FRONT', 'actual');
define('EMAIL_CONTACT', 'milangotera@gmail.com');

class My_Controller extends CI_Controller {

	protected $data = array();

	public function __construct(){
		parent::__construct();
		$this->load->library(array('session','form_validation'));
		$this->load->helper(array('url','form','file','download'));
		$this->load->database('default');
		$this->load->model([
			'paginate_Model',
			'categorias_Model',
			'etiquetas_Model',
			'galerias_Model',
			'usuarios_Model',
			'roles_Model',
			'paises_Model',
			'entradas_Model',
			'canchas_Model',
			'servicios_Model',
			'comentarios_Model',
			'newsletter_Model',
			'menu_Model',
		]);

		// META HTML //
		$this->data['menu']   = 1;
		$this->data['meta_titulo']   = "W3yA :: Blog de Diseño Web, Tutoriales, Cursos, Recursos, Código y un poco de Entretenimiento.";
		$this->data['meta_description']   = "Web especializada en noticias, tutoriales y análisis de gadgets, tecnología, gaming, entretenimiento, ciencia e Internet.";
		$this->data['meta_keywords']   = "Web, Programacion, Internet, tutoriales, PHP, HTML5, Diseño Web, Computacion, tecnologia, Blog.";
		$this->data['meta_autor']   = "Milan Gotera Quijada.";
		$this->data['meta_autor_twitter']   = "@milang90";
		$this->data['meta_image']   = base_url()."public/theme/".THEME_FRONT."/images/crecer.jpg";
		$this->data['meta_site']   = "W3ya.com";
		$this->data['header_class']   = "header_sticky";

		// MENU PRINCIPAL
		$this->data['menu_itens'] = $this->menu_Model->list(null,null,['estatus'=>1],['column'=>'id', 'dir'=>'asc']);
		$this->data['login']   = [
			"signin" => $this->session->userdata('signin'),
			"id" => $this->session->userdata('id'),
			"username" => $this->session->userdata('username'),
			"nombre" => $this->session->userdata('nombre'),
			"email" => $this->session->userdata('email'),
			"web" => $this->session->userdata('web'),
			"apellido" => $this->session->userdata('apellido'),
			"roles" => $this->session->userdata('roles'),
			"avatar" => $this->session->userdata('avatar')
		];
	}

	protected function load_front
	(
		$content = null,
		$layout = null
	){
		$content = $content ? $content : 'theme/front/'.THEME_FRONT.'/blank';
		$layout  = $layout ? $layout : 'layout-default';
       	$layout                  = 'theme/front/'.THEME_FRONT.'/'.$layout;
       	$this->data['content']   = isset($this->data['content']) ? $this->data['content'] : $this->load->view($content ,$this->data, true);
       	$this->data['header']    = $this->load->view('theme/front/'.THEME_FRONT.'/header',$this->data, true);
       	$this->data['copyright'] = $this->load->view('theme/front/'.THEME_FRONT.'/copyright',$this->data, true);
		$this->data['footer']    = $this->load->view('theme/front/'.THEME_FRONT.'/footer',$this->data, true);
		$this->data['token']     = $this->token();
		$this->load->view($layout,$this->data);
    }

    protected function load_auth
	(
		$content = null,
		$layout = null,
		$header = null
	){
		$content = $content ? $content : 'theme/auth/'.THEME_AUTH.'/blank';
		$layout  = $layout ? $layout : 'layout-default';
		$header = $header ? $header : 'auth';
       	$layout                  = 'theme/auth/'.THEME_AUTH.'/'.$layout;
       	$this->data['content']   = $this->load->view($content ,$this->data, true);
       	$this->data['header']    = $this->load->view('theme/'.$header.'/'.THEME_AUTH.'/header',$this->data, true);
		$this->data['token']     = $this->token();
		$this->load->view($layout,$this->data);
    }

	public function token()
	{
		$token = md5(uniqid(rand(),true));
		$this->session->set_userdata('token',$token);
		return $token;
	}

}
