<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios_Model extends CI_Model {
  public function __construct() {
    parent::__construct();
  }

  public function list(
    $column = null,
    $start = null,
    $length = null,
    $where = null,
    $order = null
  ){
    $select     = $column ? $column : '*';
    $start      = $start ? $start : 0;
    $length     = $length ? $length : 0;
    $order = $order ? $order : ['column'=>'id', 'dir'=>'asc'];
    if($where){
      $this->db->where($where);
    }
    $this->db->select($select);
    $this->db->order_by($order['column'], $order['dir']);
    $consulta   = $this->db->get("user",$length,$start);
    $resultado  = $consulta->result();
    $total      = $consulta->num_rows();
    $data = [
      'total' => $total,
      'data' => $resultado
    ];
    return $data;
  }

  public function count($where = null)
  {
    if($where){
      $this->db->where($where);
    }
    $consult = $this->db->get('user');
    $total = $consult->num_rows();
    return $total;
  }

  public function insert($data = null)
  {
    $this->db->insert('user', $data);
    return $this->db->insert_id();
  }

  public function update($data = null, $where = null)
  {
    if($where){
      $this->db->where($where);
    }
    $this->db->update('user', $data);
    return true;
  }

  public function delete($where)
  {
    if($where){
      $this->db->where($where);
    }
    $this->db->delete('user');
    return true;
  }

    public function is_login($roles = null, $redirect = 'auth', $mensaje = 'Identificate para continuar.'){
        $login   = $this->session->userdata('signin') ? true : false;
        $grupo   = true;
        if($roles){
            $grupo = in_array($this->session->userdata('roles'), $roles);
        }
        $return  = false;
        if($login && $grupo){
            $return  = true;
        }
        if(!$return){
            if($mensaje)
                $this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>$mensaje]);
            if($redirect)
                redirect(base_url().$redirect);
        }
        return $return;
    }

}
