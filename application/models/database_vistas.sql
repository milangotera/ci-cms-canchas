CREATE OR REPLACE VIEW entrada_etiqueta AS
SELECT
C.id,
C.nombre,
C.seo,
E.id AS entrada
FROM etiqueta C
LEFT JOIN entrada_taxonomia T ON C.id = T.taxonomia AND T.tipo = 'etiqueta'
LEFT JOIN entrada E ON T.entrada = E.id;

CREATE OR REPLACE VIEW entrada_categoria AS
SELECT
C.id,
C.nombre,
C.seo,
E.id AS entrada
FROM categoria C
LEFT JOIN entrada_taxonomia T ON C.id = T.taxonomia AND T.tipo = 'categoria'
LEFT JOIN entrada E ON T.entrada = E.id;

CREATE OR REPLACE VIEW canchas_view AS
SELECT 
C.*,
(SELECT COUNT(0) FROM cancha_comentario WHERE cancha = C.id) AS user_comentarios,
U.username AS user_username,
U.nombre AS user_nombre,
U.apellido AS user_apellido,
P.nombre AS pais_nombre,
E.nombre AS pais_estado,
R.nombre AS pais_provincia,
D.nombre AS pais_distrito
FROM cancha C
LEFT JOIN user U ON C.user = U.id
LEFT JOIN pais P ON C.pais = P.id
LEFT JOIN pais_estado E ON C.estado = E.id
LEFT JOIN pais_provincia R ON C.provincia = R.id
LEFT JOIN pais_distrito D ON C.distrito = D.id
;

CREATE OR REPLACE VIEW canchas_comentarios_view AS
SELECT 
C.*,
U.username AS user_username,
U.nombre AS user_nombre,
U.apellido AS user_apellido,
U.avatar AS user_avatar
FROM cancha_comentario C
LEFT JOIN user U ON C.autor = U.id
;