<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paginate_Model extends CI_Model {
    
    public function __construct() {
        parent::__construct();
    }

    public function paginate(
        $table = null,
        $limit = 10,
        $where = null       
    ){
        if($table){
            if($where){
                $this->db->where($where);
            }
            $query  = $this->db->get($table);
            $itens  = $query->num_rows();
            $pages  = ceil($itens / $limit);
            return [
                'pages' => $pages,
                'itens' => $itens,
                'limit' => $limit,
            ];
        }
        return false;
    }

}
