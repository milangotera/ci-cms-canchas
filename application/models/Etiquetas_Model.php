<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Etiquetas_Model extends CI_Model {
  public function __construct() {
    parent::__construct();
  }

  public function list(
    $start = null,
    $length = null,
    $where = null,
    $order = null
  ){
    $select     = "id, nombre, seo, estatus";
    $start      = $start ? $start : 0;
    $length     = $length ? $length : 0;
    $order = $order ? $order : ['column'=>'id', 'dir'=>'asc'];
    if($where){
      $this->db->where($where);
    }
    $this->db->select($select);
    $this->db->order_by($order['column'], $order['dir']);
    $consulta   = $this->db->get("etiqueta",$length,$start);
    $resultado  = $consulta->result();
    $total      = $consulta->num_rows();
    $data = [
      'total' => $total,
      'data' => $resultado
    ];
    return $data;
  }

  public function count($where = null)
  {
    if($where){
      $this->db->where($where);
    }
    $consult = $this->db->get('etiqueta');
    $total = $consult->num_rows();
    return $total;
  }

  public function insert($data = null)
  {
    $this->db->insert('etiqueta', $data);
    return $this->db->insert_id();
  }

  public function update($data = null, $where = null)
  {
    if($where){
      $this->db->where($where);
    }
    $this->db->update('etiqueta', $data);
    return true;
  }

  public function delete($where)
  {
    if($where){
      $this->db->where($where);
    }
    $this->db->delete('etiqueta');
    return true;
  }

}
