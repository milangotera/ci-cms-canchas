<?php if($servicios['total']){ ?>
<?php foreach($servicios['data'] as $servicio){ ?>
<?php $estatus =  $servicio->estatus ? 1 : 0; ?>

<?php echo form_open('auth/servicios/editar/'.$servicio->id); ?>

<div class="album py-5 bg-light">
    <div class="container">

        <div class="row">

            <div class="col-12">
                <div class="card card-default">
                    <div class="card-header pd-12">
                        <div class="row">
                            <div class="col-sm-8">
                                <button class="btn btn-titulo"><i class="fa fa-angle-right" aria-hidden="true"></i> Editar Servicio</button>
                            </div>
                            <div class="col-sm-4">
                                <a type="button" class="btn btn-default float-right-md" href="<?=base_url();?>auth/servicios"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Volver</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive  pd-12">
                            
                                <?php if(validation_errors()){ ?>
                                <div class="alert alert-danger">
                                    <?php echo validation_errors('<p class="error">','</p>'); ?>
                                </div>
                                <?php } ?>

                                <?php if($this->session->flashdata('mensaje')) {
                                  $message = $this->session->flashdata('mensaje');
                                ?>
                                <div class="alert alert-<?php echo $message['class']; ?>">
                                    <span class="badge badge-pill badge-<?php echo $message['class']; ?>"><?php echo $message['class']; ?></span>
                                    <?php echo $message['text']; ?>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php } ?>
                                               
                                <div class="form-group">
                                    <label for="titulo">Nombre</label>
                                    <input type="text" name="nombre" class="form-control campo" id="nombre" placeholder="Nombre" value="<?php echo $servicio->nombre; ?>">
                                </div>

                                <div class="form-group">
                                    <label for="titulo">Seo</label>
                                    <input type="text" name="seo" class="form-control campo" id="seo" placeholder="Seo Url" value="<?php echo $servicio->seo; ?>">
                                </div>

                                <div class="form-group">
                                    <label for="titulo">icono</label>
                                    <input type="text" name="icono" class="form-control campo" id="icono" placeholder="Nombre" value="<?php echo $servicio->icono; ?>">
                                </div>

                                <div class="form-group">
                                    <label for="foto">Estatus</label>
                                    <select name="estatus"  id="estatus" class="form-control select">
                                        <option value="1" <?php if($estatus){ ?> selected <?php } ?>>Activo</option>
                                        <option value="0" <?php if(!$estatus){ ?> selected <?php } ?>>Inactivo</option>
                                    </select>
                                </div>

                        </div>
                    </div>
                    <div class="card-footer pd-12">
                        <div class="row">
                            <div class="col-sm-8">
                                <button type="submit" class="btn btn-success"><i class="fa fa-save" aria-hidden="true"></i> Guardar</button>
                            </div>
                            <div class="col-sm-4">
                                <a type="button" class="btn btn-default float-right-md" href="<?=base_url();?>auth/servicios"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Volver</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <br>
    <br>
    <br>
</div>

</form>
<?php }} ?>
<script>
    $(document).ready(function() {

        $("#nombre").keyup(function(){
            mySeoURL("#nombre","#seo");
        });

        $('form').attr('autocomplete', "off");

     });
</script>