
<form action="<?=base_url();?>auth/usuarios/nuevo" method="post" autocomplete="off">

<div class="album py-5 bg-light">
    <div class="container">

        <div class="row">

            <div class="col-12">
                <div class="card card-default">
                    <div class="card-header pd-12">
                        <div class="row">
                            <div class="col-sm-8">
                                <button class="btn btn-titulo"><i class="fa fa-angle-right" aria-hidden="true"></i> Nuevo Usuario</button>
                            </div>
                            <div class="col-sm-4">
                                <a type="button" class="btn btn-default float-right-md" href="<?=base_url();?>auth/usuarios"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Volver</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive  pd-12">
                                
                                <?php if(validation_errors()){ ?>
                                <div class="alert alert-danger">
                                    <?php echo validation_errors('<p class="error">','</p>'); ?>
                                </div>
                                <?php } ?>

                                <?php if($this->session->flashdata('mensaje')) {
                                  $message = $this->session->flashdata('mensaje');
                                ?>
                                <div class="alert alert-<?php echo $message['class']; ?>">
                                    <span class="badge badge-pill badge-<?php echo $message['class']; ?>"><?php echo $message['class']; ?></span>
                                    <?php echo $message['text']; ?>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php } ?>
                                               
                                <div class="form-group">
                                    <label for="titulo">Nombre</label>
                                    <input type="text" name="nombre" class="form-control campo" id="nombre" placeholder="Nombre" value="<?php echo set_value('nombre'); ?>">
                                </div>

                                <div class="form-group">
                                    <label for="titulo">Apellido</label>
                                    <input type="text" name="apellido" class="form-control campo" id="apellido" placeholder="Apellido" value="<?php echo set_value('apellido'); ?>">
                                </div>

                                <div class="form-group">
                                    <label for="titulo">Sexo</label>
                                    <select name="sexo"  id="sexo" class="form-control select">
                                        <option value="M">M</option>
                                        <option value="F">F</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="foto">Pais</label>
                                    <select name="pais"  id="grupo" class="form-control select">
                                        <?php if($paises['total']){ ?>
                                        <?php foreach($paises['data'] as $pais){ ?>
                                        <option value="<?php echo $pais->nombre; ?>"><?php echo $pais->nombre; ?></option>
                                        <?php }} ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="titulo">Fecha de Nacimiento</label>
                                    <input type="date" name="fecha" class="form-control campo" id="fecha" placeholder="Nacimiento" value="<?php echo $fecha; ?>">
                                </div>

                                <div class="form-group">
                                    <label for="titulo">Email</label>
                                    <input type="email" name="email" class="form-control campo" id="email" placeholder="Email" value="<?php echo set_value('email'); ?>">
                                </div>

                                <div class="form-group">
                                    <label for="titulo">Usuario</label>
                                    <input type="text" name="username" class="form-control campo" id="username" placeholder="Usuario" value="<?php echo set_value('username'); ?>">
                                </div>

                                <div class="form-group">
                                    <label for="titulo">Password</label>
                                    <input type="text" name="password" class="form-control campo" id="password" placeholder="Password" value="<?php echo set_value('password'); ?>">
                                </div>

                                <div class="form-group">
                                    <label for="foto">Grupo</label>
                                    <select name="grupo"  id="grupo" class="form-control select">
                                        <?php if($roles['total']){ ?>
                                        <?php foreach($roles['data'] as $role){ ?>
                                        <option value="<?php echo $role->nombre; ?>"><?php echo $role->descripcion; ?></option>
                                        <?php }} ?>
                                    </select>
                                </div>                              

                                <div class="form-group">
                                    <label for="titulo">Biograf&iacute;a</label>
                                    <textarea class="form-control textarea" name="about" id="about"></textarea>
                                </div>

                                <div class="form-group">
                                    <label for="titulo">Web</label>
                                    <input type="text" name="web" class="form-control campo" id="web" placeholder="Web" value="<?php echo set_value('web'); ?>">
                                </div>

                                <div class="form-group">
                                    <label for="titulo">Facebook</label>
                                    <input type="text" name="facebook" class="form-control campo" id="facebook" placeholder="Facebook" value="<?php echo set_value('facebook'); ?>">
                                </div>

                                <div class="form-group">
                                    <label for="titulo">Twitter</label>
                                    <input type="text" name="twitter" class="form-control campo" id="twitter" placeholder="Twitter" value="<?php echo set_value('twitter'); ?>">
                                </div>

                                <div class="form-group">
                                    <label for="titulo">Google+</label>
                                    <input type="text" name="google" class="form-control campo" id="google" placeholder="Twitter" value="<?php echo set_value('google'); ?>">
                                </div>

                                <div class="form-group">
                                    <label for="titulo">Linkedin</label>
                                    <input type="text" name="linkedin" class="form-control campo" id="linkedin" placeholder="Linkedin" value="<?php echo set_value('linkedin'); ?>">
                                </div>

                                <div class="form-group">
                                    <label for="titulo">Whatsapp</label>
                                    <input type="text" name="watsapp" class="form-control campo" id="watsapp" placeholder="Whatsapp" value="<?php echo set_value('watsapp'); ?>">
                                </div>

                                <div class="form-group">
                                    <label for="foto">Estatus</label>
                                    <select name="estatus"  id="estatus" class="form-control select">
                                        <option value="1">Activo</option>
                                        <option value="0">Inicativo</option>
                                    </select>
                                </div>

                        </div>
                    </div>
                    <div class="card-footer pd-12">
                        <div class="row">
                            <div class="col-sm-8">
                                <button type="submit" class="btn btn-success"><i class="fa fa-save" aria-hidden="true"></i> Guardar</button>
                            </div>
                            <div class="col-sm-4">
                                <a type="button" class="btn btn-default float-right-md" href="<?=base_url();?>auth/usuarios"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Volver</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <br>
    <br>
    <br>
</div>

</form>

<script>
    $(document).ready(function() {

        $("#nombre").keyup(function(){
            mySeoURL("#nombre","#seo");
        });

        $('form').attr('autocomplete', "off");

     });
</script>