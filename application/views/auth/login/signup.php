
        <div class="bg_color_2 bg-1" style="background-image: url('<?php echo base_url()."public/theme/front/".THEME_FRONT; ?>/img/banner/2.jpg');background-repeat: no-repeat; background-position: 0% 0%; background-size: 100% 100%;">
            <div class="container margin_60_35">
                <div id="login-2">
                    <h1>Crear una cuenta!</h1>
                    <form method="post" action="<?php echo base_url(); ?>auth/signup">
                        <div class="box_form clearfix">
                            <div class="box_login">
                                <a href="#" class="social_bt facebook">Registrar con Facebook</a>
                                <a href="#" class="social_bt google">Registrar con Google</a>
                                <a href="#" class="social_bt linkedin">Registrar con Linkedin</a>
                            </div>
                            <div class="box_login last">
                                <?php if(validation_errors()){ ?>
                                    <div class="alert alert-danger">
                                    <?php echo validation_errors('<p class="error">','</p>'); ?>
                                </div>
                                <?php } ?>
                                <?php if($this->session->flashdata('mensaje')) {
                                    $message = $this->session->flashdata('mensaje');
                                ?>
                                <div class="alert alert-<?php echo $message['class']; ?>">
                                    <?php echo $message['text']; ?>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php } ?>
                                <div class="form-group">
                                    <input placeholder="Username" name="username" id="username" type="text" class="form-control" value="<?php echo set_value('username'); ?>">
                                </div>
                                <div class="form-group">
                                    <input placeholder="Email" name="email" id="email" type="email" class="form-control" value="<?php echo set_value('email'); ?>">
                                </div>
                                <div class="form-group">
                                    <input placeholder="Password" name="password" id="password" type="password" class="form-control">
                                </div>
                                <div class="form-group">
                                    <input class="btn_1" type="submit" value="Registrarme">
                                </div>
                            </div>
                        </div>
                    </form>
                    <p class="text-center link_bright">¿Tienes una cuenta? <a href="<?=base_url();?>auth/login"><strong>Iniciar con su cuenta!</strong></a></p>
                </div>
                <!-- /login -->
            </div>
        </div>

    <script>
    $(document).ready(function() {

        $('form').attr('autocomplete', "off");

     });
    </script>