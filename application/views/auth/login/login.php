        <div class="bg_color_2 bg-1" style="background-image: url('<?php echo base_url()."public/theme/front/".THEME_FRONT; ?>/img/banner/1.jpg');background-repeat: no-repeat; background-position: 0% 0%; background-size: 100% 100%;">
            <div class="container margin_60_35">
                <div id="login-2">
                    <h1>Por favor, inicie sesión en W3ya!</h1>
                    <form method="post" action="<?php echo base_url(); ?>auth/login">
                        <div class="box_form clearfix">
                            <div class="box_login">
                                <a href="#" class="social_bt facebook">Inicie con Facebook</a>
                                <a href="#" class="social_bt google">Inicie con Google</a>
                                <a href="#" class="social_bt linkedin">Inicie con Linkedin</a>
                            </div>
                            <div class="box_login last">
                                <?php if(validation_errors()){ ?>
                                    <div class="alert alert-danger">
                                    <?php echo validation_errors('<p class="error">','</p>'); ?>
                                </div>
                                <?php } ?>
                                <?php if($this->session->flashdata('mensaje')) {
                                    $message = $this->session->flashdata('mensaje');
                                ?>
                                <div class="alert alert-<?php echo $message['class']; ?>">
                                    <?php echo $message['text']; ?>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php } ?>
                                <div class="form-group">
                                    <input placeholder="Email" name="email" id="email" type="email" class="form-control" value="<?php echo set_value('email'); ?>">
                                </div>
                                <div class="form-group">
                                    <input placeholder="Password" name="password" id="password" type="password" class="form-control">
                                    <a href="#" class="forgot"><small>Se te olvidó tu contraseña?</small></a>
                                </div>
                                <div class="form-group">
                                    <input class="btn_1" type="submit" value="Ingresar">
                                </div>
                            </div>
                        </div>
                    </form>
                    <p class="text-center link_bright">Aun no tiene una cuenta? <a href="<?php echo base_url(); ?>auth/signup"><strong>Regístrate ahora!</strong></a></p>
                </div>
                <!-- /login -->
            </div>
        </div>

    <script>
    $(document).ready(function() {

        $('form').attr('autocomplete', "off");

     });
    </script>