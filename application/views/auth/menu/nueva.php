<?php echo form_open('auth/menu/nuevo'); ?>

<div class="album py-5 bg-light">
    <div class="container">

        <div class="row">

            <div class="col-12">
                <div class="card card-default">
                    <div class="card-header pd-12">
                        <div class="row">
                            <div class="col-sm-8">
                                <button class="btn btn-titulo"><i class="fa fa-angle-right" aria-hidden="true"></i> Nuevo Menu</button>
                            </div>
                            <div class="col-sm-4">
                                <a type="button" class="btn btn-default float-right-md" href="<?=base_url();?>auth/menu"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Volver</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive  pd-12">
                                
                                <?php if(validation_errors()){ ?>
                                <div class="alert alert-danger">
                                    <?php echo validation_errors('<p class="error">','</p>'); ?>
                                </div>
                                <?php } ?>

                                <?php if($this->session->flashdata('mensaje')) {
                                  $message = $this->session->flashdata('mensaje');
                                ?>
                                <div class="alert alert-<?php echo $message['class']; ?>">
                                    <span class="badge badge-pill badge-<?php echo $message['class']; ?>"><?php echo $message['class']; ?></span>
                                    <?php echo $message['text']; ?>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php } ?>
                                               
                                <div class="form-group">
                                    <label for="titulo">Nombre</label>
                                    <input type="text" name="nombre" class="form-control campo" id="nombre" placeholder="Nombre" value="<?php echo set_value('nombre'); ?>">
                                </div>

                                <div class="form-group">
                                    <label for="titulo">Icono</label>
                                    <input type="text" name="icono" class="form-control campo" id="icono" placeholder="fa fa-home" value="<?php echo set_value('icono'); ?>">
                                </div>

                                <div class="form-group">
                                    <label for="titulo">Path</label>
                                    <input type="text" name="path" class="form-control campo" id="path" placeholder="Path Url" value="<?php echo set_value('path'); ?>">
                                </div>

                                <div class="form-group">
                                    <label for="foto">Ventana</label>
                                    <select name="target"  id="target" class="form-control select" value="<?php echo set_value('target'); ?>">
                                        <option value="_self">Actual</option>
                                        <option value="_blank">Nueva</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="foto">Externo</label>
                                    <select name="externo"  id="externo" class="form-control select" value="<?php echo set_value('externo'); ?>">
                                        <option value="0">No</option>
                                        <option value="1">Si</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="foto">Estatus</label>
                                    <select name="estatus"  id="estatus" class="form-control select" value="<?php echo set_value('estatus'); ?>">
                                        <option value="1">Activo</option>
                                        <option value="0">Inactivo</option>
                                    </select>
                                </div>

                        </div>
                    </div>
                    <div class="card-footer pd-12">
                        <div class="row">
                            <div class="col-sm-8">
                                <button type="submit" class="btn btn-success"><i class="fa fa-save" aria-hidden="true"></i> Guardar</button>
                            </div>
                            <div class="col-sm-4">
                                <a type="button" class="btn btn-default float-right-md" href="<?=base_url();?>auth/menu"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Volver</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <br>
    <br>
    <br>
</div>

</form>