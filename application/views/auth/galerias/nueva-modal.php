<form method="post" id="formulario_galeria" action="<?=base_url();?>auth/galerias/nueva">

    <div class="card-body">
        <div class="table-responsive  pd-12">
                
                <?php if(validation_errors()){ ?>
                <div class="alert alert-danger">
                    <?php echo validation_errors('<p class="error">','</p>'); ?>
                </div>
                <?php } ?>

                <?php if($this->session->flashdata('mensaje')) {
                  $message = $this->session->flashdata('mensaje');
                ?>
                <div class="alert alert-<?php echo $message['class']; ?>">
                    <span class="badge badge-pill badge-<?php echo $message['class']; ?>"><?php echo $message['class']; ?></span>
                    <?php echo $message['text']; ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <?php } ?>
                               
                <div class="form-group">
                    <label for="titulo">Nombre</label>
                    <input type="text" name="nombre" class="form-control campo" id="nombre_galeria" placeholder="Nombre" value="<?php echo set_value('nombre'); ?>">
                </div>

                <div class="form-group pd-12 bg-light bd-1">
                    <label for="foto" style="width: 100%; float: left;">Foto principal</label>
                    <input type="file" name="foto" class="form-control-file campo" id="foto_galeria" placeholder="Foto principal" value="">
                    <img id="blah_galeria" src="#" alt="Vista Previa" style="width: 300px; float: left; display: none; cursor: pointer;" title="Cambiar foto" />
                    <dir style="clear: both;"></dir>
                </div>

                <div class="form-group">
                    <label for="foto">Tama&ntilde;os</label>
                    <select name="tamano"  id="tamano_galeria" class="form-control select" value="<?php echo set_value('estatus'); ?>">
                        <option value="800x400">Horizontal (800x400)</option>
                        <option value="400x800">Vertical   (400x800)</option>
                        <option value="800x800">Parejo     (800x800)</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="foto">Estatus</label>
                    <select name="estatus"  id="estatus_galeria" class="form-control select" value="<?php echo set_value('estatus'); ?>">
                        <option value="1">Activo</option>
                        <option value="0">Inicativo</option>
                    </select>
                </div>

        </div>
    </div>

    <div class="row">
        <div class="col-sm-8">
            <button type="button" class="btn btn-success" id="guardar"><i class="fa fa-save" aria-hidden="true"></i> Guardar</button>
        </div>
        <div class="col-sm-4">
            <a type="button" class="btn btn-default float-right-md" href="#" id="volver"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Volver</a>
        </div>
    </div>


</form>

<script>
    $(document).ready(function() {

        function getFiles()
        {
            var idImagenes=document.getElementById("foto_galeria");
            var archivos=idFiles.files;
            var data=new FormData();
            for(var i=0;i<archivos.length;i++)
            {
                data.append("foto",archivos[i]);
            }
            return data;
        }
         
        /**
         * Función que recorre todo el formulario para apadir en el FormData los valores del formulario
         * @param string id hace referencia al id del formulario
         * @param FormData data hace referencia al FormData
         * @return FormData
         */
        function getFormData(id,data)
        {
            $("#"+id).find("input,select").each(function(i,v) {
                if(v.type!=="file") {
                    if(v.type==="checkbox" && v.checked===true) {
                        data.append(v.name,"on");
                    }else{
                        data.append(v.name,v.value);
                    }
                }
            });
            return data;
        }

        $("#nombre_galeria").keyup(function(){
            mySeoURL("#nombre_galeria","#seo_galeria");
        });

        $('form').attr('autocomplete', "off");

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#blah_galeria').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
                $("#blah_galeria").show();
            }
        }

        $("#foto_galeria").change(function(){
            readURL(this);
            $("#foto_galeria").hide();
        });

        $("#blah_galeria").click(function(){
            $("#foto_galeria").show();
            $("#blah_galeria").hide();
            $('#foto_galeria').val('');
        });

        $("#volver").click(function(){
            $("#modal-body").load($("#base_url").html()+'auth/galerias/page/1/modal');
        });

        $('#guardar').click(function(){
            var formData = new FormData($("#formulario_galeria")[0]);
            $.ajax({
                url: $("#base_url").html()+'auth/galerias/nueva/modal',  
                type: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function(){
                    //       
                },
                success: function(data){
                    $("#modal-body").html(data);
                },
                error: function(){
                    //
                }
            });
        });
        
     });
</script>