<style type="text/css">
.item {
    float: left;
    position: relative;
    line-height: 1em;
}

.grid-sizer { width: 20%; }
.item { width: 20%; }

.item, .image { padding: 5px; }
.image { border: solid 1px #ccc; }

@media screen and (max-width: 1224px) {
  /* 10 columns for larger screens */
  .grid-sizer { width: 25%; }
  .item { width: 25%; }
}

@media screen and (max-width: 720px) {
  /* 10 columns for larger screens */
  .grid-sizer { width: 33.33%; }
  .item { width: 33.33%; }
}

@media screen and (max-width: 480px) {
  /* 10 columns for larger screens */
  .grid-sizer { width: 50%; }
  .item { width: 50%; }
}

.image{
    max-width: 100%;
    margin: 0;
    display: block;
}

.image:after {
    clear:both;
}

.onhandover {
    width: 100%;
    height: 100%;
    background-color: rgba(0,0,0,0.5);
    position: absolute;
    top: 0;
    left: 0;
    text-decoration: none;
    color: #fff;
    display: none;
}

.onhandover .title, .onhandover .description{
    color: #fff;
}

.onhandover .title {
    text-align: right;
    font-size: 30px;
}

.onhandover .title button{
    padding: 8px;
    padding-right: 12px;
    color: #fff;
}

.onhandover .description {
    position: absolute;
    bottom: 0;
    left: 0;
    background-color: rgba(0,0,0,0.80);
    width: 100%;
    margin: 0;
    padding: 5px !important;
    font-size: 12px;
}

.onhandover .description p {
    text-align: center;
}

.item:hover .onhandover {
    display: block;
}
</style>
<script src="<?=base_url();?>public/theme/<?=THEME_ASSETS;?>/library/masonry/masonry.pkgd.min.js"></script>
<script type="text/javascript">
$(window).load( function() {

    $("#masonry").masonry({
        "itemSelector": ".item",
        "columnWidth": ".grid-sizer",
    });

});
</script>
<div class="album py-5 bg-light">
    <div class="container">

        <div class="row">

            <div class="col-12">
                <div class="card card-default">
                    <div class="card-header pd-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <button class="btn btn-titulo"><i class="fa fa-angle-right" aria-hidden="true"></i> Galer&iacute;as</button>
                            </div>
                            <div class="col-sm-6">
                                <a type="button" class="btn btn-default float-right-md" href="<?=base_url();?>auth/galerias/nueva"><i class="fa fa-plus-circle" aria-hidden="true"></i> Nueva</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <?php if($this->session->flashdata('mensaje')) {
                          $message = $this->session->flashdata('mensaje');
                        ?>
                        <div class="alert alert-<?php echo $message['class']; ?>">
                            <span class="badge badge-pill badge-<?php echo $message['class']; ?>"><?php echo $message['class']; ?></span>
                            <?php echo $message['text']; ?>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <?php } ?>
                        <?php if(!count($galerias['data'])){ ?>
                            <div class="alert alert-danger">
                                <span class="badge badge-pill badge-danger">danger</span>
                                No existen los datos que buscas.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                        <?php } ?>
                        <div id="masonry">
                            <div class="grid-sizer"></div>
                            <?php if(count($galerias['data'])){ ?>
                            <?php foreach($galerias['data'] as $galeria){ ?>
                            <?php $estatus =  $galeria->estatus ? "Activo" : "Inactivo"; ?>
                                <div class="item">
                                    <img src="<?=base_url();?><?=$galeria->path;?>" class="image"  alt="<?=$galeria->nombre;?>">
                                    <a class="onhandover" href="<?=base_url();?>auth/galerias/borrar/<?=$galeria->id;?>">
                                        <h3 class="title">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </h3>
                                        <div class="description"><?=$galeria->nombre;?></div>
                                    </a>
                                </div>
                            <?php }} ?>

                        </div>
                    </div>
                    <div class="card-footer">
                        
                        <div class="row">
                            <!--<div class="col-sm-12"><center>Mostrando p&aacute;gina 1 de 1</center></div>-->
                            <nav class="pagination-center">
                                <ul class="pagination">
                                    <li class="page-item">
                                        <a class="page-link" href="<?=base_url();?>auth/galerias/page/<?=($paginate['active']-1);?>/">
                                            <i class="fa fa-chevron-circle-left" aria-hidden="true"></i> 
                                            <span class="hide-736">Anterior</span>
                                        </a>
                                    </li>
                                    <li class="page-item">
                                        <a class="page-link" href="#"><span class="hide-736">Mostrando p&aacute;gina</span> <?=($paginate['active']);?> de <?=($paginate['pages']);?></a>
                                    </li>
                                    <li class="page-item">
                                        <a class="page-link" href="<?=base_url();?>auth/galerias/page/<?=($paginate['active']+1);?>/">
                                            <span class="hide-736">Siguiente</span> 
                                            <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                </ul>
                                <div style="clear: both;"></div>
                            </nav>
                        </div>

                    </div>
                </div>
            </div>

        </div>

    </div>
</div>