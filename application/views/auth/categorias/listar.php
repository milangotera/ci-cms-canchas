<div class="album py-5 bg-light">
    <div class="container">

        <div class="row">

            <div class="col-12">
                <div class="card card-default">
                    <div class="card-header pd-12">
                        <div class="row">
                            <div class="col-sm-8">
                                <button class="btn btn-titulo"><i class="fa fa-angle-right" aria-hidden="true"></i> Categor&iacute;as</button>
                            </div>
                            <div class="col-sm-4">
                                <a type="button" class="btn btn-default float-right-md" href="<?=base_url();?>auth/categorias/nueva"><i class="fa fa-plus-circle" aria-hidden="true"></i> Nueva</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <?php if($this->session->flashdata('mensaje')) {
                          $message = $this->session->flashdata('mensaje');
                        ?>
                        <div class="alert alert-<?php echo $message['class']; ?>">
                            <span class="badge badge-pill badge-<?php echo $message['class']; ?>"><?php echo $message['class']; ?></span>
                            <?php echo $message['text']; ?>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <?php } ?>
                        <?php if(!count($categorias['data'])){ ?>
                            <div class="alert alert-danger">
                                <span class="badge badge-pill badge-danger">danger</span>
                                No existen los datos que buscas.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                        <?php } else { ?>
                        <div class="table-responsive">
                            <table class="table table-bordered table-sm ">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th>Nombre</th>
                                        <th>Seo</th>
                                        <th>Estatus</th>
                                        <th>Accion</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if($categorias['total']){ ?>
                                    <?php foreach($categorias['data'] as $categoria){ ?>
                                    <?php $estatus =  $categoria->estatus ? "Activo" : "Inactivo"; ?>
                                    <tr>
                                    <td><?php echo $categoria->id; ?></td>
                                    <td class="titulo"><?php echo $categoria->nombre; ?></td>
                                    <td><?php echo $categoria->seo; ?></td>
                                    <td><?php echo $estatus; ?></td>
                                    <td class="accion">
                                        <a type="button" class="btn btn-primary btn-sm" href="<?=base_url();?>auth/categorias/editar/<?php echo $categoria->id; ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        <a type="button" class="btn btn-danger btn-sm" href="<?=base_url();?>auth/categorias/borrar/<?php echo $categoria->id; ?>"><i class="fa fa-times" aria-hidden="true"></i></a>
                                    </td>
                                    </tr>
                                    <?php }} ?>
                                </tbody>
                            </table>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="card-footer">
                        
                        <div class="row">
                            <!--<div class="col-sm-12"><center>Mostrando p&aacute;gina 1 de 1</center></div>-->
                            <nav class="pagination-center">
                                <ul class="pagination">
                                    <li class="page-item">
                                        <a class="page-link" href="<?=base_url();?>auth/categorias/page/<?=($paginate['active']-1);?>/">
                                            <i class="fa fa-chevron-circle-left" aria-hidden="true"></i> 
                                            <span class="hide-736">Anterior</span>
                                        </a>
                                    </li>
                                    <li class="page-item">
                                        <a class="page-link" href="#"><span class="hide-736">Mostrando p&aacute;gina</span> <?=($paginate['active']);?> de <?=($paginate['pages']);?></a>
                                    </li>
                                    <li class="page-item">
                                        <a class="page-link" href="<?=base_url();?>auth/categorias/page/<?=($paginate['active']+1);?>/">
                                            <span class="hide-736">Siguiente</span> 
                                            <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                </ul>
                                <div style="clear: both;"></div>
                            </nav>
                        </div>

                    </div>
                </div>
            </div>

        </div>

    </div>
</div>