<?php 
if($canchas['total']){
    foreach($canchas['data'] as $cancha){
        $estatus =  $cancha->estatus; 
        $cancha_servicios =  explode(",",$cancha->servicios);
        echo form_open('auth/canchas/editar/'.$cancha->id);
?>

<div class="album py-5 bg-light">
    <div class="container">

        <div class="row">

            <div class="col-12">
                <div class="card card-default">
                    <div class="card-header pd-12">
                        <div class="row">
                            <div class="col-sm-8">
                                <button class="btn btn-titulo"><i class="fa fa-angle-right" aria-hidden="true"></i> Editar Cancha</button>
                            </div>
                            <div class="col-sm-4">
                                <a type="button" class="btn btn-default float-right-md" href="<?=base_url();?>auth/canchas"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Volver</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive  pd-12">

                                <?php if(validation_errors()){ ?>
                                <div class="alert alert-danger">
                                    <?php echo validation_errors('<p class="error">','</p>'); ?>
                                </div>
                                <?php } ?>

                                <?php if($this->session->flashdata('mensaje')) {
                                  $message = $this->session->flashdata('mensaje');
                                ?>
                                <div class="alert alert-<?php echo $message['class']; ?>">
                                    <span class="badge badge-pill badge-<?php echo $message['class']; ?>"><?php echo $message['class']; ?></span>
                                    <?php echo $message['text']; ?>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php } ?>
                                                            
                                <div class="form-group">
                                    <label for="titulo">Nombre</label>
                                    <input type="text" name="nombre" class="form-control campo" id="nombre" placeholder="Nombre" value="<?php echo $cancha->nombre; ?>">
                                </div>

                                <div class="form-group">
                                    <label for="titulo">Seo</label>
                                    <input type="text" name="seo" class="form-control campo" id="seo" placeholder="Seo url" value="<?php echo $cancha->seo; ?>">
                                </div>

                                <div class="form-group">
                                    <label for="resumen">Descripci&oacute;n</label>
                                    <textarea name="resumen" id="resumen"><?php echo $cancha->resumen; ?></textarea>
                                </div>

                                <div class="form-group">
                                    <label for="foto">Pa&iacute;s</label>
                                    <select name="pais" id="pais" class="form-control select select2" data-value="<?php echo $cancha->pais; ?>">
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="foto">Departamento</label>
                                    <select name="departamento"  id="departamento" class="form-control select select2" data-value="<?php echo $cancha->estado; ?>">
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="foto">Provincia</label>
                                    <select name="provincia"  id="provincia" class="form-control select select2" data-value="<?php echo $cancha->provincia; ?>">
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="foto">Distrito</label>
                                    <select name="distrito"  id="distrito" class="form-control select select2" data-value="<?php echo $cancha->distrito; ?>">
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="resumen">Direcci&oacute;n</label>
                                    <textarea name="direccion" id="direccion" class="form-control textarea"><?php echo $cancha->direccion; ?></textarea>
                                </div>

                                <div class="form-group">
                                    <label for="titulo">Mapa</label>
                                     <textarea name="mapa" id="mapa" class="form-control textarea" placeholder="Pega aqui tu codigo de google map"><?php echo $cancha->mapa; ?></textarea>
                                </div>

                                <div class="form-group">
                                    <label for="foto">Servicios</label>
                                    <select name="servicios[]"  id="servicios" class="form-control select select2" multiple="multiple">
                                        <?php if($servicios['total']){ ?>
                                        <?php foreach($servicios['data'] as $data){ ?>
                                        <option value="<?php echo $data->nombre; ?>" <?php if (in_array($data->nombre, $cancha_servicios)) { echo "selected"; } ?>><?php echo $data->nombre; ?></option>
                                        <?php }} ?>
                                    </select>
                                </div>

                                <div class="form-group pd-12 bg-light bd-1">
                                    <label for="foto" style="width: 100%; float: left;">Foto principal</label>
                                    <input type="hidden" name="foto" class="form-control-file campo" id="foto" value="<?php echo $cancha->imagen; ?>">
                                    <button type="button" class="btn btn-titulo" data-toggle="modal" data-target="#modal-galeria" title="Cambiar Foto" style="border: solid 1px #ccc;">
                                    <img id="image" src="<?php echo base_url(); ?><?php echo $cancha->imagen; ?>" alt="Vista Previa" style="width: 300px; float: left; cursor: pointer;" title="Cambiar foto" />
                                    </button>
                                    <dir style="clear: both;"></dir>
                                </div>

                                <div class="form-group">
                                    <label for="titulo">Video</label>
                                    <textarea name="video" id="video" class="form-control textarea" placeholder="Pega aqui tu codigo de youtube"><?php echo $cancha->video; ?></textarea>
                                </div>

                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <label for="titulo">Abre</label>
                                        <select name="abre"  id="abre" class="form-control select">
                                        <option value="06:00AM" <?php if($cancha->abre == "06:00AM"){ echo "selected";} ?>>06:00AM</option>
                                        <option value="07:00AM" <?php if($cancha->abre == "07:00AM"){ echo "selected";} ?>>07:00AM</option>
                                        <option value="08:00AM" <?php if($cancha->abre == "08:00AM"){ echo "selected";} ?>>08:00AM</option>
                                        <option value="09:00AM" <?php if($cancha->abre == "09:00AM"){ echo "selected";} ?>>09:00AM</option>
                                        <option value="10:00AM" <?php if($cancha->abre == "10:00AM"){ echo "selected";} ?>>10:00AM</option>
                                        <option value="11:00AM" <?php if($cancha->abre == "11:00AM"){ echo "selected";} ?>>11:00AM</option>
                                        <option value="12:00MD" <?php if($cancha->abre == "12:00MD"){ echo "selected";} ?>>12:00MD</option>
                                        <option value="01:00PM" <?php if($cancha->abre == "01:00PM"){ echo "selected";} ?>>01:00PM</option>
                                        <option value="02:00PM" <?php if($cancha->abre == "02:00PM"){ echo "selected";} ?>>02:00PM</option>
                                        <option value="03:00PM" <?php if($cancha->abre == "03:00PM"){ echo "selected";} ?>>03:00PM</option>
                                        <option value="04:00PM" <?php if($cancha->abre == "04:00PM"){ echo "selected";} ?>>04:00PM</option>
                                        <option value="05:00PM" <?php if($cancha->abre == "05:00PM"){ echo "selected";} ?>>05:00PM</option>
                                        <option value="06:00PM" <?php if($cancha->abre == "06:00PM"){ echo "selected";} ?>>06:00PM</option>
                                        <option value="07:00PM" <?php if($cancha->abre == "07:00PM"){ echo "selected";} ?>>07:00PM</option>
                                        <option value="08:00PM" <?php if($cancha->abre == "08:00PM"){ echo "selected";} ?>>08:00PM</option>
                                        <option value="09:00PM" <?php if($cancha->abre == "09:00PM"){ echo "selected";} ?>>09:00PM</option>
                                        <option value="10:00PM" <?php if($cancha->abre == "10:00PM"){ echo "selected";} ?>>10:00PM</option>
                                        <option value="11:00PM" <?php if($cancha->abre == "11:00PM"){ echo "selected";} ?>>11:00PM</option>
                                        <option value="12:00PM" <?php if($cancha->abre == "12:00PM"){ echo "selected";} ?>>12:00AM</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label for="titulo">Cierra</label>
                                        <select name="cierra"  id="cierra" class="form-control select">
                                        <option value="06:00AM" <?php if($cancha->cierra == "06:00AM"){ echo "selected";} ?>>06:00AM</option>
                                        <option value="07:00AM" <?php if($cancha->cierra == "07:00AM"){ echo "selected";} ?>>07:00AM</option>
                                        <option value="08:00AM" <?php if($cancha->cierra == "08:00AM"){ echo "selected";} ?>>08:00AM</option>
                                        <option value="09:00AM" <?php if($cancha->cierra == "09:00AM"){ echo "selected";} ?>>09:00AM</option>
                                        <option value="10:00AM" <?php if($cancha->cierra == "10:00AM"){ echo "selected";} ?>>10:00AM</option>
                                        <option value="11:00AM" <?php if($cancha->cierra == "11:00AM"){ echo "selected";} ?>>11:00AM</option>
                                        <option value="12:00MD" <?php if($cancha->cierra == "12:00MD"){ echo "selected";} ?>>12:00MD</option>
                                        <option value="01:00PM" <?php if($cancha->cierra == "01:00PM"){ echo "selected";} ?>>01:00PM</option>
                                        <option value="02:00PM" <?php if($cancha->cierra == "02:00PM"){ echo "selected";} ?>>02:00PM</option>
                                        <option value="03:00PM" <?php if($cancha->cierra == "03:00PM"){ echo "selected";} ?>>03:00PM</option>
                                        <option value="04:00PM" <?php if($cancha->cierra == "04:00PM"){ echo "selected";} ?>>04:00PM</option>
                                        <option value="05:00PM" <?php if($cancha->cierra == "05:00PM"){ echo "selected";} ?>>05:00PM</option>
                                        <option value="06:00PM" <?php if($cancha->cierra == "06:00PM"){ echo "selected";} ?>>06:00PM</option>
                                        <option value="07:00PM" <?php if($cancha->cierra == "07:00PM"){ echo "selected";} ?>>07:00PM</option>
                                        <option value="08:00PM" <?php if($cancha->cierra == "08:00PM"){ echo "selected";} ?>>08:00PM</option>
                                        <option value="09:00PM" <?php if($cancha->cierra == "09:00PM"){ echo "selected";} ?>>09:00PM</option>
                                        <option value="10:00PM" <?php if($cancha->cierra == "10:00PM"){ echo "selected";} ?>>10:00PM</option>
                                        <option value="11:00PM" <?php if($cancha->cierra == "11:00PM"){ echo "selected";} ?>>11:00PM</option>
                                        <option value="12:00PM" <?php if($cancha->cierra == "12:00PM"){ echo "selected";} ?>>12:00AM</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <label for="titulo">Hora diurna</label>
                                        <input type="number" name="dia" class="form-control campo" id="dia" value="<?php echo $cancha->dia; ?>">
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label for="titulo">Hora nocturna</label>
                                        <input type="number" name="noche" class="form-control campo" id="noche" value="<?php echo $cancha->noche; ?>">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <label for="titulo">Canchas</label>
                                        <input type="number" name="canchas" class="form-control campo" id="canchas" value="<?php echo $cancha->canchas; ?>">
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label for="titulo">Estrellas</label>
                                        <select name="estrellas"  id="estrellas" class="form-control select">
                                        <option value="1" <?php if($cancha->estrellas == 1){ echo "selected";} ?>>1</option>
                                        <option value="2" <?php if($cancha->estrellas == 2){ echo "selected";} ?>>2</option>
                                        <option value="3" <?php if($cancha->estrellas == 3){ echo "selected";} ?>>3</option>
                                        <option value="4" <?php if($cancha->estrellas == 4){ echo "selected";} ?>>4</option>
                                        <option value="5" <?php if($cancha->estrellas == 5){ echo "selected";} ?>>5</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="titulo">Tel&eacute;fonos</label>
                                    <input type="text" name="telefono" class="form-control campo" id="telefono" placeholder="Tel&eacute;fonos" value="<?php echo $cancha->telefonos; ?>">
                                </div>

                                <div class="form-group">
                                    <label for="titulo">P&aacute;gina</label>
                                    <input type="text" name="pagina" class="form-control campo" id="pagina" placeholder="Pagina Web" value="<?php echo $cancha->pagina; ?>">
                                </div>

                                <div class="form-group">
                                    <label for="titulo">Facebook</label>
                                    <input type="text" name="facebook" class="form-control campo" id="facebook" value="<?php echo $cancha->facebook; ?>">
                                </div>

                                <div class="form-group">
                                    <label for="titulo">Twitter</label>
                                    <input type="text" name="twitter" class="form-control campo" id="twitter" value="<?php echo $cancha->twitter; ?>">
                                </div>

                                <div class="form-group">
                                    <label for="titulo">Instagran</label>
                                    <input type="text" name="instagran" class="form-control campo" id="instagran" value="<?php echo $cancha->instagran; ?>">
                                </div>

                                <?php if($login['roles'] == "admin"){ ?>
                                <div class="form-group">
                                    <label for="foto">Autor</label>
                                    <select name="autor"  id="autor" class="form-control select">
                                        <?php if($autor['total']){ ?>
                                        <?php foreach($autor['data'] as $data){ ?>
                                        <option value="<?php echo $data->id; ?>" <?php if($cancha->user == $data->id){ echo "selected";} ?>><?php echo $data->nombre." ".$data->apellido; ?></option>
                                        <?php }} ?>
                                    </select>
                                </div>
                                <?php } ?>

                                <div class="form-group">
                                    <label for="foto">Comentarios</label>
                                    <select name="comentarios"  id="comentarios" class="form-control select">
                                        <option value="1" <?php if($cancha->comentarios == 1){ echo "selected";} ?>>Activar</option>
                                        <option value="0" <?php if($cancha->comentarios == 0){ echo "selected";} ?>>Desactivar</option>
                                    </select>
                                </div>

                                <?php if($login['roles'] == "admin"){ ?>
                                <div class="form-group">
                                    <label for="foto">Destacar</label>
                                    <select name="destacar"  id="destacar" class="form-control select">
                                        <option value="1" <?php if($cancha->destacar == 1){ echo "selected";} ?>>Activar</option>
                                        <option value="0" <?php if($cancha->destacar == 0){ echo "selected";} ?>>Desactivar</option>
                                    </select>
                                </div>
                                <?php } ?>

                                <?php if($login['roles'] == "admin"){ ?>
                                <div class="form-group">
                                    <label for="foto">Premiun</label>
                                    <select name="premiun"  id="premiun" class="form-control select">
                                        <option value="1" <?php if($cancha->premiun == 1){ echo "selected";} ?>>Activar</option>
                                        <option value="0" <?php if($cancha->premiun == 0){ echo "selected";} ?>>Desactivar</option>
                                    </select>
                                </div>
                                <?php } ?>

                                <?php if($login['roles'] == "admin"){ ?>
                                <div class="form-group">
                                    <label for="foto">Estatus</label>
                                    <select name="estatus"  id="estatus" class="form-control select">
                                        <option value="0" <?php if($cancha->estatus == 0){ echo "selected";} ?>>Borrador</option>
                                        <option value="1" <?php if($cancha->estatus == 1){ echo "selected";} ?>>Publicar</option>
                                    </select>
                                </div>
                                <?php } ?>

                        </div>
                    </div>
                    <div class="card-footer pd-12">
                        <div class="row">
                            <div class="col-sm-8">
                                <button type="submit" class="btn btn-success"><i class="fa fa-save" aria-hidden="true"></i> Guardar</button>
                            </div>
                            <div class="col-sm-4">
                                <a type="button" class="btn btn-default float-right-md" href="<?=base_url();?>auth/canchas"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Volver</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <br>
    <br>
    <br>
</div>
</form>
<?php }} ?>

<!-- Modal -->
<div class="modal fade" id="modal-galeria" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Galer&iacute;a de Im&aacute;genes</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="modal-body"></div>
      <div class="modal-footer hide">
        <a type="button" class="btn btn-default float-right-md nueva-galeria" href="#"><i class="fa fa-plus-circle" aria-hidden="true"></i> Nueva</a>
      </div>
    </div>
  </div>
</div>

<script>
    $(document).ready(function(e) {
        $('#resumen').summernote();

        $("#nombre").keyup(function(e){
            mySeoURL("#nombre","#seo");
        });
    
        $('.select2').select2();

        $('#modal-galeria').on('show.bs.modal', function (e) {
            $("#modal-body").load($("#base_url").html()+'auth/galerias/page/1/modal');
        });

        $('#modal-galeria').on('hide.bs.modal', function (e) {
            $("#modal-body").html('');
        });
        
        function api_paises(api ,selector , label = 1, activo = 0){
            $.ajax({
                data: {},
                type: "GET",
                dataType: "json",
                url: $("#base_url").html()+'api_paises'+api,
            })
            .done(function(data, textStatus, jqXHR ){
                if(console && console.log){
                    console.log("Listar paises se ha completado correctamente.");
                }
                var option = '';
                if(label){
                    option += '<option value="0">Elige uno</option>';  
                }
                $.each(data,function(index, value){
                    var selected = '';
                    if(value.id == activo)
                        var selected = 'selected';
                    option += '<option value="'+value.id+'" '+selected+'>'+value.nombre+'<option>';
                });
                $(selector).html(option);
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                if(console && console.log) {
                    console.log("La solicitud de paises a fallado: " +  textStatus);
                }
            });
        }

        api_paises('/listar/id/89','#pais', 0, $('#departamento').data('value'));
        api_paises('/estado/id/1618','#departamento', 0, $('#departamento').data('value'));
        api_paises('/provincia/estado_id/'+$('#departamento').data('value'),'#provincia', 0, $('#provincia').data('value'));
        api_paises('/distrito/provincia_id/'+$('#provincia').data('value'),'#distrito', 0, $('#distrito').data('value'));

        $("#pais").change(function() {
            api_paises('/estado/id/'+$(this).val(),'#departamento', 0);
        });

        $("#departamento").change(function() {
            api_paises('/provincia/estado_id/'+$(this).val(),'#provincia', 0);
        });

        $("#provincia").change(function() {
            api_paises('/distrito/provincia_id/'+$(this).val(),'#distrito', 0);
        });

    });
</script>