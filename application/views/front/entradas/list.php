    <!-- SPECIFIC CSS -->
    <link href="<?=base_url();?>public/theme/front/<?=THEME_FRONT;?>/css/blog.css" rel="stylesheet">

    <div id="breadcrumb" class="bg-1 bg-off">
        <div class="container">
            <ul>
                <li><a href="<?=base_url();?>home">Home</a></li>
                <li><a href="<?=base_url();?>entradas"><?=$breadcrumb_titulo;?></a></li>
                <li><?=$breadcrumb_resumen;?></li>
            </ul>
        </div>
    </div>
    
    <div class="container margin_60">
        <div class="main_title">
            <h1><?=$breadcrumb_titulo;?></h1>
            <p><?=$breadcrumb_resumen;?></p>
        </div>
        <div class="row">
            
            <div class="<?php if(isset($sidebar)){ echo "col-lg-9"; }else{ echo "col-lg-12"; } ?>">

                <?php if($total){ ?>
                <?php foreach($entradas as $entrada){ ?>
                <?php $estatus =  intval($entrada['estatus']) ? "Publicado" : "Borrador"; ?>
                <?php $tipo  =  ($entrada['tipo'] == 1) ? "Entrada" : "Pagina"; ?>
                <?php $comentar  =  $entrada['comentarios'] ? "Permitir" : "Denegar"; ?>
                <article class="blog wow fadeIn animated" style="visibility: visible; animation-name: fadeIn;">
                    <div class="row no-gutters">
                        <div class="col-lg-7">
                            <figure>
                                <a href="<?=base_url();?>eventos/<?php echo $entrada['seo']; ?>"><img src="<?=base_url();?><?php echo $entrada['foto']; ?>" alt=""><div class="preview"><span>Read more</span></div></a>
                            </figure>
                        </div>
                        <div class="col-lg-5">
                            <div class="post_info">
                                <small><?php echo $entrada['fecha']; ?></small>
                                <h3><a href="<?=base_url();?>eventos/<?php echo $entrada['seo']; ?>"><?php echo $entrada['titulo']; ?></a></h3>
                                <p><?php echo $entrada['resumen']; ?></p>
                                <ul>
                                    <li>
                                        <div class="thumb"><img src="<?=base_url();?><?php echo $entrada['avatar']; ?>" alt="Img Autor"></div> <?php echo $entrada['nombre']." ".$entrada['apellido']; ?>
                                    </li>
                                    <li>
                                        <?php foreach($entrada['categorias'] as $cat){ ?>
                                            <a href="<?=base_url();?>eventos/categorias/<?php echo $cat[1]; ?>"><?php echo $cat[0]; ?></a>&nbsp;&nbsp;
                                        <?php } ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </article>
                <!-- /article -->
                <?php }} ?>
                
                <nav aria-label="...">
                    <?php $activa = ($paginate['active']-1 >0) ? ($paginate['active']-1) : 1;  ?>
                    <?php $siguiente = (($activa + 1) <= $paginate['pages']) ? ($activa + 1) : $activa ; ?>
                    <ul class="pagination pagination-sm">
                        <li class="page-item disabled">
                            <a class="page-link" href="<?=base_url();?><?=$vista;?><?=$activa;?>/" tabindex="-1">Previous</a>
                        </li>
                        <?php for($i=1;$i<=$paginate['pages'];$i++){ ?>
                        <li class="page-item"><a class="page-link" href="<?=base_url();?><?=$vista;?><?=$i;?>/"><?=$i;?></a></li>
                        <?php } ?>
                        <li class="page-item">
                            <a class="page-link" href="<?=base_url();?><?=$vista;?><?=$siguiente;?>/">Next</a>
                        </li>
                    </ul>
                </nav>
                <!-- /pagination -->
            </div>

            <?php if(isset($sidebar)){ echo $sidebar; } ?>

        </div>
    </div>