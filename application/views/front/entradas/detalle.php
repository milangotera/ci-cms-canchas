    <!-- SPECIFIC CSS -->
    <link href="<?=base_url();?>public/theme/front/<?=THEME_FRONT;?>/css/blog.css" rel="stylesheet">
    <!-- YOUR CUSTOM CSS -->
    <link href="<?=base_url();?>public/theme/front/<?=THEME_FRONT;?>/css/custom.css" rel="stylesheet">

    <div id="breadcrumb" class="bg-1 bg-off">
        <div class="container">
            <ul>
                <li><a href="<?=base_url();?>home">Home</a></li>
                <li><a href="<?=base_url();?>eventos"><?=$breadcrumb_titulo;?></a></li>
                <li><?=$breadcrumb_resumen;?></li>
            </ul>
        </div>
    </div>

    <div class="container margin_60">

        <div class="row">

            <div class="<?php if(isset($sidebar)){ echo "col-lg-9"; }else{ echo "col-lg-12"; } ?>">
                <?php if(!empty($entradas)){ ?>
                <?php foreach($entradas as $entrada){ ?>
                    <div class="bloglist singlepost">
                        <p><img alt="" class="img-fluid" src="<?=base_url();?><?php echo $entrada['foto']; ?>"></p>
                        <h1><?php echo $entrada['titulo']; ?></h1>
                        <div class="postmeta">
                            <ul>
                                <li style="display: none;"><a href="#"><i class="icon_folder-alt"></i> Collections</a></li>
                                <li><a href="#"><i class="icon_clock_alt"></i> <?php echo $entrada['fecha']; ?></a></li>
                                <li><a href="#"><i class="icon_pencil-edit"></i> <?php echo $entrada['nombre']." ".$entrada['apellido']; ?></a></li>
                                <li><a href="#"><i class="icon_comment_alt"></i> (14) Vistas</a></li>
                            </ul>
                        </div>
                        <!-- /post meta -->
                        <div class="post-content">
                            <?php echo $entrada['contenido']; ?>
                        </div>
                        <!-- /post -->
                    </div>
                    <!-- /single-post -->
                    <?php }} ?>
                    <?php if(isset($layout_comentarios)){ echo $layout_comentarios; } ?>
                    <?php if(isset($layout_comentar)){ echo $layout_comentar; } else { ?>
                    <div class="comment-top">
                    <div class="alert alert-warning">
                        Los comentarios están cerrados.
                    </div>
                    </div>
                    <?php } ?>

            </div>

            <?php if(isset($sidebar)){ echo $sidebar; } ?>

        </div>


