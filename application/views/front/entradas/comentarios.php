                        <div id="comments">
                        <h5>Comentarios</h5>
                        <?php if(count($comentarios['data']) == 0){ ?>
                            <div class="alert alert-warning">
                                Se el primero en participar
                            </div>
                        <?php }?>
                        <ul>
                            <?php if(!empty($comentarios['data'])){ ?>
                            <?php foreach($comentarios['data'] as $data){ ?>
                            <?php $imagen = $data->avatar ? $data->avatar : 'public/img/avatar/default.jpeg'; ?>
                            <li>
                                <div class="avatar">
                                    <a href="#"><img src="<?=base_url().$imagen;?>" alt="">
                                    </a>
                                </div>
                                <div class="comment_right clearfix">
                                    <div class="comment_info">
                                        Por <a href="#"><?php echo $data->autor_nombre; ?></a><span>|</span><?php echo $data->createdat; ?><span>|</span><a href="#">Responder</a>
                                    </div>
                                    <p>
                                        <?php echo $data->comentario; ?>
                                    </p>
                                </div>
                            </li>
                            <?php }} ?>
                        </ul>
                    </div>
                            