    <!-- SPECIFIC CSS -->

    <div id="results" class="bg-1 bg-off">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h4><strong>Montrando <?php echo $canchas['filtred']; ?></strong> de <?php echo $canchas['total']; ?> Resultados</h4>
                </div>
                <div class="col-md-6">
                    <div class="search_bar_list">
                        <input type="text" class="form-control" placeholder="Busca tu cancha aqu&iacute;...">
                        <input type="submit" value="Buscar" class="bg-2">
                    </div>
                </div>
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
        <!-- /results -->

    <div class="filters_listing">
            <div class="container">
                <ul class="clearfix">
                    <li>
                        <h6>Mostrar</h6>
                        <div class="switch-field">
                            <input type="radio" id="all" name="type_patient" value="all" checked>
                            <label for="all">Todas</label>
                            <input type="radio" id="doctors" name="type_patient" value="doctors">
                            <label for="doctors">Fotos</label>
                        </div>
                    </li>
                    <li>
                        <h6>Dise&ntilde;o</h6>
                        <div class="layout_view">
                            <a href="#"><i class="icon-th"></i></a>
                            <a href="#" class="active"><i class="icon-th-list"></i></a>
                            <a href="#"><i class="icon-map-1"></i></a>
                        </div>
                    </li>
                    <li>
                        <h6>Ordenar por</h6>
                        <select name="orderby" class="selectbox">
                        <option value="Closest">Fecha</option>
                        <option value="Best rated">Nombre</option>
                        </select>
                    </li>
                </ul>
            </div>
            <!-- /container -->
        </div>
        <!-- /filters -->

        <div class="container margin_60_35" style="transform: none;">
            <div class="row" style="transform: none;">
                

                <?php if($canchas['total']){ ?>
                <?php foreach($canchas['data'] as $cancha){ ?>
                <?php $estatus =  $cancha->estatus ? "Publicado" : "Borrador"; ?>
                <?php $comentar  =  $cancha->comentarios ? "Permitir" : "Denegar"; ?>
                <div class="col-lg-6 col-md-6">
                <article class="blog wow fadeIn animated" style="visibility: visible; animation-name: fadeIn;">
                    <div class="row no-gutters">
                        <div class="col-lg-4">
                            <figure style="height: 200px;">
                                <a href="<?=base_url();?>canchas/<?php echo $cancha->seo; ?>">
                                    <img style="width: 100%; height: 100%;" src="<?=base_url();?><?php echo $cancha->imagen; ?>" alt="">
                                    <div class="preview"><span>Read more</span></div>
                                </a>
                            </figure>
                        </div>
                        <div class="col-lg-8">
                            <div class="post_info" style="min-height: 0px; padding: 10px;">
                                <small><?php echo $cancha->createdat; ?></small>
                                <h3><a href="<?=base_url();?>canchas/<?php echo $cancha->seo; ?>"><?php echo $cancha->nombre; ?></a></h3>
                                <?php echo $cancha->resumen; ?>
                                    <span class="rating">
                                        <?php for($i = 1; $i < 6; $i++){ ?>
                                        <?php $estrella = ($cancha->estrellas >=$i) ? "voted" : ""; ?>
                                        <i class="icon_star <?=$estrella;?>"></i>
                                        <?php } ?> 
                                        <small>(<?php echo $cancha->user_comentarios; ?>)</small>
                                    </span>
                                    <a href="<?=base_url();?>canchas/<?php echo $cancha->seo; ?>" data-toggle="tooltip" data-placement="top" data-original-title="Badge Level" class="badge_list_1" aria-describedby="tooltip453983"><img src="<?=base_url();?>public/theme/front/<?=THEME_FRONT;?>/img/badges/badge_1.svg" width="15" height="15" alt=""></a>
                                </p>

                                <div class="tags">
                                <?php $servicios = explode(",", $cancha->servicios); ?>
                                <?php foreach($servicios as $servicios){ ?>
                                    <a class="bg-3" href="#"><?php echo $servicios; ?></a>&nbsp;&nbsp;
                                <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                        <div class="post_info" style="min-height: 0px;">
                            <ul>
                                <li style="padding: 12px 0 0 5px;">
                                    <img style="width: 15px;" src="<?=base_url();?>public/theme/front/<?=THEME_FRONT;?>/img/marcador-map-1.png" alt="Img Autor">&nbsp;&nbsp;&nbsp;<?php echo $cancha->pais_estado; ?>, <?php echo $cancha->pais_provincia; ?>, <?php echo $cancha->pais_distrito; ?>, <?php echo $cancha->pais_nombre; ?>.
                                </li>
                                <li>
                                    <a href="<?=base_url();?>canchas/<?php echo $cancha->seo; ?>" class="btn-detalle">Detalle</a>
                                </li>
                            </ul>
                            <div style="clear: both;"></div>
                        </div>
                        </div>
                    </div>
                </article>
                <!-- /article -->
                </div>
                <?php }} ?>

                <div class="col-lg-12">
                    
                    <nav aria-label="...">
                        <?php $activa = ($paginate['active']-1 >0) ? ($paginate['active']-1) : 1;  ?>
                        <?php $siguiente = (($activa + 1) <= $paginate['pages']) ? ($activa + 1) : $activa ; ?>
                        <ul class="pagination pagination-sm">
                            <li class="page-item disabled">
                                <a class="page-link" href="<?=base_url();?>canchas/page/<?=$activa;?>/" tabindex="-1">Previous</a>
                            </li>
                            <?php for($i=1;$i<=$paginate['pages'];$i++){ ?>
                            <li class="page-item"><a class="page-link" href="<?=base_url();?>canchas/page/<?=$i;?>/"><?=$i;?></a></li>
                            <?php } ?>
                            <li class="page-item">
                                <a class="page-link" href="<?=base_url();?>canchas/page/<?=$siguiente;?>/">Next</a>
                            </li>
                        </ul>
                    </nav>
                    <!-- /pagination -->
                </div>
                <!-- /col -->
                
            </div>
            <!-- /row -->
        </div>