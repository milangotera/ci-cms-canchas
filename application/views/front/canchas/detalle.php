    <!-- SPECIFIC CSS -->

    <div id="breadcrumb" class="bg-1 bg-off">
        <div class="container">
            <ul>
                <li><a href="<?=base_url();?>home">Home</a></li>
                <li><a href="<?=base_url();?>canchas">Canchas</a></li>
                <li><?=$detalle['data'][0]->nombre;?></li>
            </ul>
        </div>
    </div>



    <div class="container margin_60">
        <div class="row">
            <div class="col-xl-8 col-lg-8">
                <nav id="secondary_nav" class="bg-1 bg-off hide">
                    <div class="container">
                        <ul class="clearfix">
                            <li><a href="#section_1" class="active">Informaci&oacute;n General</a></li>
                            <li><a href="#section_2">Opiniones</a></li>
                            <li><a href="#sidebar">Recomendada</a></li>
                        </ul>
                    </div>
                </nav>
                <?php if($detalle['total']){ ?>
                <?php foreach($detalle['data'] as $cancha){ ?>
                <?php $estatus =  $cancha->estatus ? "Publicado" : "Borrador"; ?>
                <?php $comentar  =  $cancha->comentarios ? "Permitir" : "Denegar"; ?>
                <div id="section_1">
                    <figure class="box_general_3" style="padding: 0px; margin: 0px;">
                        <img src="<?php echo base_url(); ?><?php echo $cancha->imagen; ?>" alt="" class="img-fluid br-t-5" style="max-height: 289px; width: 100%;">
                    </figure>
                    <div class="box_general_3 br-bt-5">

                        <div class="profile">
                            <div class="row">
                                
                                <div class="col-lg-12 col-md-12">
                                    <small><?php echo $cancha->createdat; ?></small>
                                    <h1><?php echo $cancha->nombre; ?></h1>
                                    <span class="rating">
                                        <?php for($i = 1; $i < 6; $i++){ ?>
                                        <?php $estrella = ($cancha->estrellas >=$i) ? "voted" : ""; ?>
                                        <i class="icon_star <?=$estrella;?>"></i>
                                        <?php } ?> 
                                        <small>(<?php echo $cancha->user_comentarios; ?>)</small>
                                    </span>
                                    <a href="<?=base_url();?>canchas/<?php echo $cancha->seo; ?>" data-toggle="tooltip" data-placement="top" data-original-title="Badge Level" class="badge_list_1" aria-describedby="tooltip453983"><img src="<?=base_url();?>public/theme/front/<?=THEME_FRONT;?>/img/badges/badge_1.svg" width="15" height="15" alt=""></a>
                                    <ul class="contacts">
                                        <li>
                                            <h6>Direcci&oacute;n</h6>
                                            <?php echo $cancha->direccion; ?> -
                                            <a href="<?php echo $cancha->mapa; ?>" target="_blank"> <strong>Ver en el mapa</strong></a>
                                        </li>
                                        <li>
                                            <h6>Tel&eacute;fono</h6> <a href="tel://<?php echo $cancha->telefonos; ?>"><?php echo $cancha->telefonos; ?></a></li>
                                    </ul>
                                </div>

                            </div>
                            <div class="tags" style="margin-top: 30px;">
                            <?php $servicios = explode(",", $cancha->servicios); ?>
                            <?php foreach($servicios as $servicios){ ?>
                                <a class="bg-3" href="#"><?php echo $servicios; ?></a>&nbsp;&nbsp;
                            <?php } ?>
                            </div>
                            <hr>
                            <?php echo $cancha->resumen; ?>
                        </div>

                    </div>
                    <!-- /section_1 -->
                </div>
                <!-- /box_general -->

                <div id="section_2">
                    <div class="box_general_3">
                        <?php if($comentarios['total']){
                        $star[1] = 0;
                        $star[2] = 0;
                        $star[3] = 0;
                        $star[4] = 0;
                        $star[5] = 0;
                        foreach($comentarios['data'] as $comentario){ 
                        $star[$comentario->estrellas]++;  
                        }
                        $estrellas = number_format( (($star[1]+$star[2]*2+$star[3]*3+$star[4]*4+$star[5]*5)/$comentarios['total']) , 1);
                        ?>
                        <div class="reviews-container">
                            <div class="row">
                                <div class="col-lg-3" style="padding: 0px;">
                                    <div id="review_summary" class="bg-4 color-333" style="border: solid 1px #999;">
                                        <strong><?php echo $estrellas; ?></strong>
                                        <div class="rating">
                                            <?php for($i = 1; $i < 6; $i++){ ?>
                                            <?php $estrella = (round($estrellas) >= $i) ? "voted" : ""; ?>
                                            <i class="icon_star <?=$estrella;?>"></i>
                                            <?php } ?>
                                        </div>
                                        <small>Basado en <?php echo $comentarios['total']; ?> opiniones</small>
                                    </div>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-10 col-9">
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" style="width: <?=(($star[5]*100)/$comentarios['total']); ?>%" aria-valuenow="<?=(($star[5]*100)/$comentarios['total']); ?>" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-3" style="padding: 0px;"><small><strong>5 Estrellas</strong></small></div>
                                    </div>
                                    <!-- /row -->
                                    <div class="row">
                                        <div class="col-lg-10 col-9">
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" style="width: <?=(($star[4]*100)/$comentarios['total']); ?>%" aria-valuenow="<?=(($star[4]*100)/$comentarios['total']); ?>" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-3" style="padding: 0px;"><small><strong>4 Estrellas</strong></small></div>
                                    </div>
                                    <!-- /row -->
                                    <div class="row">
                                        <div class="col-lg-10 col-9">
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" style="width: <?=(($star[3]*100)/$comentarios['total']); ?>%" aria-valuenow="<?=(($star[3]*100)/$comentarios['total']); ?>" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-3" style="padding: 0px;"><small><strong>3 Estrellas</strong></small></div>
                                    </div>
                                    <!-- /row -->
                                    <div class="row">
                                        <div class="col-lg-10 col-9">
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" style="width: <?=(($star[2]*100)/$comentarios['total']); ?>%" aria-valuenow="<?=(($star[2]*100)/$comentarios['total']); ?>" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-3" style="padding: 0px;"><small><strong>2 Estrellas</strong></small></div>
                                    </div>
                                    <!-- /row -->
                                    <div class="row">
                                        <div class="col-lg-10 col-9">
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" style="width: <?=(($star[1]*100)/$comentarios['total']); ?>%" aria-valuenow="<?=(($star[1]*100)/$comentarios['total']); ?>" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-3" style="padding: 0px;"><small><strong>1 Estrellas</strong></small></div>
                                    </div>
                                    <!-- /row -->
                                </div>
                            </div>
                            <!-- /row -->

                            <hr>
                            
                            <?php foreach($comentarios['data'] as $comentario){ ?>
                            <div class="review-box clearfix">
                                <figure class="rev-thumb"><img src="<?php echo base_url(); ?><?php echo $comentario->user_avatar; ?>" alt="">
                                </figure>
                                <div class="rev-content">
                                    <div class="rating">
                                        <?php for($i = 1; $i < 6; $i++){ ?>
                                        <?php $estrella = ($comentario->estrellas >=$i) ? "voted" : ""; ?>
                                        <i class="icon_star <?=$estrella;?>"></i>
                                        <?php } ?>
                                    </div>
                                    <div class="rev-info">
                                        <?php echo $comentario->user_nombre; ?> <?php echo $comentario->user_apellido; ?> – <?php echo $comentario->createdat; ?>:
                                    </div>
                                    <div class="rev-text">
                                        <p>
                                            <?php echo $comentario->comentario; ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!-- End review-box -->
                            <?php } ?>
                        </div>
                        <?php } else { ?>
                        <div class="alert alert-warning">
                            Se el primero en participar
                        </div>
                        <?php } ?>

                        <hr>
                        <?php if($login['signin']){ ?>
                        <div class="comment-top contact">
                            <h4>Deja un comentario</h4>
                            <form name="contactform" id="contactform" method="post" action="" autocomplete="off">
                                <?php if(validation_errors()){ ?>
                                <div class="alert alert-danger">
                                    <?php echo validation_errors('<p class="error">','</p>'); ?>
                                </div>
                                <?php } ?>
                                <?php if($this->session->flashdata('mensaje')) {
                                  $message = $this->session->flashdata('mensaje');
                                ?>
                                <div class="alert alert-<?php echo $message['class']; ?>">
                                    <span class="badge badge-pill badge-<?php echo $message['class']; ?>"><?php echo $message['class']; ?></span>
                                    <?php echo $message['text']; ?>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php } ?>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group ">
                                            <label>Nombre</label>
                                            <input type="text" class="form-control" id="comentar_nombre" placeholder="Escribe tu Nombre" name="comentar_nombre" value="<?php echo $login['nombre']; ?> <?php echo $login['apellido']; ?>" readonly>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="email" class="form-control" id="comentar_email" placeholder="Escribe tu Email" name="comentar_email" value="<?php echo $login['email']; ?>" readonly>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Web</label>
                                            <input type="text" class="form-control" id="comentar_web" placeholder="Tu perfil o p&aacute;gina" name="comentar_web" rows="6" value="<?php echo $login['web']; ?>" readonly>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="titulo">Calificaci&oacute;n</label>
                                            <select name="estrellas"  id="estrellas" class="form-control select">
                                            <option value="1">1  Estrellas</option>
                                            <option value="2">2  Estrellas</option>
                                            <option value="3">3  Estrellas</option>
                                            <option value="4">4  Estrellas</option>
                                            <option value="5">5  Estrellas</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Enviar comentario</label>
                                    <textarea name="comentar_comentario" class="form-control input_comentar" id="comentar_comentario" placeholder="Escribe tu comentario"></textarea>
                                </div>
                                <button type="submit" class="btn_1">Enviar</button>
                            </form>
                        </div>
                        <?php }else{ ?>
                            <div class="comment-top">
                            <div class="alert alert-warning">
                                Ingresa y d&eacute;janos tu opini&oacute;n.
                            </div>
                            </div>
                        <?php } ?>
                        <!-- End review-container -->
                    </div>
                </div>
                <!-- /section_2 -->
                <?php }} ?>

                <!-- CANCHAS DESTACADAS -->
                <div id="row">
                    <h4>Canchas destacadas</h4>
                    <hr>
                    <?php if($canchas['total']){ ?>
                    <?php foreach($canchas['data'] as $cancha){ ?>
                    <?php $estatus =  $cancha->estatus ? "Publicado" : "Borrador"; ?>
                    <?php $comentar  =  $cancha->comentarios ? "Permitir" : "Denegar"; ?>
                    <article class="blog wow fadeIn animated" style="visibility: visible; animation-name: fadeIn;">
                        <div class="row no-gutters">
                            <div class="col-lg-4">
                                <figure style="height: 200px;">
                                    <a href="<?=base_url();?>canchas/<?php echo $cancha->seo; ?>">
                                        <img style="width: 100%; height: 100%;" src="<?=base_url();?><?php echo $cancha->imagen; ?>" alt="">
                                        <div class="preview"><span>Read more</span></div>
                                    </a>
                                </figure>
                            </div>
                            <div class="col-lg-8">
                                <div class="post_info" style="min-height: 0px; padding: 10px;">
                                    <small><?php echo $cancha->createdat; ?></small>
                                    <h3><a href="<?=base_url();?>canchas/<?php echo $cancha->seo; ?>"><?php echo $cancha->nombre; ?></a></h3>
                                    <?php echo $cancha->resumen; ?>
                                        <span class="rating">
                                        <?php for($i = 1; $i < 6; $i++){ ?>
                                        <?php $estrella = ($cancha->estrellas >=$i) ? "voted" : ""; ?>
                                        <i class="icon_star <?=$estrella;?>"></i>
                                        <?php } ?> 
                                        <small>(<?php echo $cancha->user_comentarios; ?>)</small>
                                    </span>
                                    <a href="<?=base_url();?>canchas/<?php echo $cancha->seo; ?>" data-toggle="tooltip" data-placement="top" data-original-title="Badge Level" class="badge_list_1" aria-describedby="tooltip453983"><img src="<?=base_url();?>public/theme/front/<?=THEME_FRONT;?>/img/badges/badge_1.svg" width="15" height="15" alt=""></a>
                                    </p>

                                    <div class="tags">
                                    <?php $servicios = explode(",", $cancha->servicios); ?>
                                    <?php foreach($servicios as $servicios){ ?>
                                        <a class="bg-3" href="#"><?php echo $servicios; ?></a>&nbsp;&nbsp;
                                    <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                            <div class="post_info" style="min-height: 0px;">
                                <ul>
                                    <li style="padding: 12px 0 0 5px;">
                                        <img style="width: 15px;" src="<?=base_url();?>public/theme/front/<?=THEME_FRONT;?>/img/marcador-map-1.png" alt="Img Autor">&nbsp;&nbsp;&nbsp;<?php echo $cancha->pais_estado; ?>, <?php echo $cancha->pais_provincia; ?>, <?php echo $cancha->pais_distrito; ?>, <?php echo $cancha->pais_nombre; ?>.
                                    </li>
                                    <li>
                                        <a href="<?=base_url();?>canchas/<?php echo $cancha->seo; ?>" class="btn-detalle">Detalle</a>
                                    </li>
                                </ul>
                                <div style="clear: both;"></div>
                            </div>
                            </div>
                        </div>
                    </article>
                    <!-- /article -->
                    <?php }} ?>
                </div>
                <!-- CANCHAS DESTACADAS -->

            </div>
            <!-- /col -->

            <aside class="col-xl-4 col-lg-4" id="sidebar">
                <?php if($premiuns['total']){
                foreach($premiuns['data'] as $premiun){ 
                ?>
                <div class="box_profile">
                    <figure>
                        <img src="<?=base_url();?><?php echo $premiun->imagen; ?>" alt="" class="img-fluid">
                    </figure>
                    <small><?php echo $premiun->createdat; ?></small>
                    <h1><?php echo $premiun->nombre; ?></h1>
                    <span class="rating">
                        <?php for($i = 1; $i < 6; $i++){ ?>
                        <?php $estrella = ($cancha->estrellas >=$i) ? "voted" : ""; ?>
                        <i class="icon_star <?=$estrella;?>"></i>
                        <?php } ?>
                        <small>(<?php echo $premiun->user_comentarios; ?>)</small>
                        <a href="<?=base_url();?>canchas/<?php echo $premiun->seo; ?>" data-toggle="tooltip" data-placement="top" data-original-title="Badge Level" class="badge_list_1"><img src="img/badges/badge_1.svg" width="15" height="15" alt=""></a>
                    </span>
                    <ul class="tags">
                        <?php $servicios = explode(",", $premiun->servicios); ?>
                        <?php foreach($servicios as $servicios){ ?>
                            <a class="bg-3" href="#"><?php echo $servicios; ?></a>&nbsp;&nbsp;
                        <?php } ?>
                    </ul>
                    <ul class="contacts">
                        <li><h6>Direcci&oacute;n</h6><?php echo $premiun->direccion; ?></li>
                        <li><h6>Phone</h6><a href="tel://<?php echo $premiun->telefonos; ?>"><?php echo $premiun->telefonos; ?></a></li>
                    </ul>
                    <div class="text-center">
                        <a href="<?=base_url();?>canchas/<?php echo $premiun->seo; ?>" class="btn_1 outline">
                            <div class="fs1" aria-hidden="true" data-icon="&#xe033;"></div> Destacada
                        </a>
                    </div>
                </div>
                <?php }} ?>
                <!-- /box_general -->
            </aside>
            <!-- /asdide -->
        </div>

    </div>