<div class="hero_home version_3 bg-1" style="background-image: url('<?php echo base_url()."public/theme/front/".THEME_FRONT; ?>/img/banner/5.jpg');background-repeat: no-repeat; background-position: 0% 0%; background-size: 100% 100%;">
    <div class="content">
        <h3 class="fadeInUp animated">¿Pichanga hoy día?</h3>
        <p class="fadeInUp animated">
            Busca y reserva tu cancha favorita.
        </p>
        <form method="post" action="list.html" class="fadeInUp animated">
            <div id="custom-search-input">
                <div class="input-group">
                    <input type="text" class=" search-query" placeholder="Escribe aquí ....">
                    <input type="submit" class="btn_search bg-2" value="Buscar">
                </div>
                <ul>
                    <li>
                        <input type="radio" id="all" name="radio_search" value="all" checked="">
                        <label for="all">Canchas</label>
                    </li>
                    <li>
                        <input type="radio" id="doctor" name="radio_search" value="doctor">
                        <label for="doctor">Equipos</label>
                    </li>
                    <li>
                        <input type="radio" id="clinic" name="radio_search" value="clinic">
                        <label for="clinic">Torneo</label>
                    </li>
                </ul>
            </div>
        </form>
    </div>
</div>

<div class="container margin_120_95">
<div class="main_title">
    <h2>Reserva <strong>canchas</strong> sin complicaciones!</h2>
    <p>Encuentra la cancha que quieres, en la zona que quiere y a la hora que tu quieres.</p>
</div>
<div class="row add_bottom_30">
    <div class="col-lg-4">
        <div class="box_feat" id="icon_1">
            <span></span>
            <h3>Busca</h3>
            <p style="text-align: justify;">Selecciona entre las canchas disponibles de tu ciudad en el directorio.</p>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="box_feat" id="icon_2">
            <span></span>
            <h3>Reserva</h3>
            <p style="text-align: justify;">Reserva la hora para que puedas cuadrar tu pichanga sin inconvenientes.</p>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="box_feat" id="icon_3">
            <h3>Comparte</h3>
            <p style="text-align: justify;">Invita a tus amigos, reta a los demás equipos y rankea como el mejor de la pichanga.</p>
        </div>
    </div>
</div>
</div>