    
    <div id="breadcrumb" class="bg-off bg-1">
        <div class="container">
            <ul>
                <li><a href="<?=base_url();?>">Home</a></li>
                <li><a href="<?=base_url();?>">Error</a></li>
                <li>Esa página no se puede encontrar.</li>
            </ul>
        </div>
    </div>

    <section class="container margin_60">
            <div class="inner-sec-wthree py-lg-3 py-3">
                <div class="main_title">
                    <h1>Lo siento!</h1>
                    <p>Esa página no se puede encontrar</p>
                </div>
                <img src="<?=base_url();?>public/theme/front/<?=THEME_FRONT;?>/img/404-error.jpg" alt="" class="img-fluid" style="width: 90%; margin-left: 5%;">
            </div>
        </div>
    </section>