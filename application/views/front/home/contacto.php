        
        <div class="container margin_60_35">
            <div class="row">
                <aside class="col-lg-3 col-md-4">
                    <div id="contact_info">
                        <h3>Contacto directo</h3>
                        <p>
                            Tacna - Per&uacute;<br> + 51(9)24866666<br>
                            <a href="#">info@tupichanga.com</a>
                        </p>
                        <h4>Obtener las direcciones</h4>
                        <form action="http://maps.google.com/maps" method="get" target="_blank">
                            <div class="form-group">
                                <input type="text" name="saddr" placeholder="Enter your location" class="form-control styled">
                                <input type="hidden" name="daddr" value="Tacna - Per&uacute;">
                                <!-- Write here your end point -->
                            </div>
                            <input type="submit" value="Buscar" class="btn_1 add_bottom_45">
                        </form>
                        <ul>
                            <li><strong>Administrador</strong>
                                <a href="tel://+51924866666">+51924866666</a><br><a href="tel://+51924866666">milangotera@gmail.com</a><br>
                                <small>Lunes a Viernes 9am - 7pm</small>
                            </li>
                            <li><strong>Concato General</strong>
                                <a href="tel://+51924866666">+51924866666</a><br><a href="tel://+51924866666">info@tupichanga.com</a><br>
                                <p><small>Lunes a Viernes 9am - 7pm</small></p>
                            </li>
                        </ul>
                    </div>
                </aside>
                <!--/aside -->
                <div class=" col-lg-8 col-md-8 ml-auto">
                    <div class="box_general">
                        <h3>Contactar</h3>
                        <p>
                            Env&iacute;a tus dudas y sugerencias.
                        </p>
                        <?php if(validation_errors()){ ?>
                        <div class="alert alert-danger">
                            <?php echo validation_errors('<p class="error">','</p>'); ?>
                        </div>
                        <?php } ?>

                        <?php if($this->session->flashdata('mensaje')) {
                          $message = $this->session->flashdata('mensaje');
                        ?>
                        <div class="alert alert-<?php echo $message['class']; ?>">
                            <span class="badge badge-pill badge-<?php echo $message['class']; ?>"><?php echo $message['class']; ?></span>
                            <?php echo $message['text']; ?>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <?php } ?>
                        <div>
                            <div id="message-contact"></div>
                            <form method="post" action="<?php echo base_url();?>contacto" id="contactform">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="name_contact" name="name_contact" placeholder="nombre">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="lastname_contact" name="lastname_contact" placeholder="Apellido">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <input type="email" id="email_contact" name="email_contact" class="form-control" placeholder="Email">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <input type="text" id="phone_contact" name="phone_contact" class="form-control" placeholder="Tel&eacute;fono">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <textarea rows="5" id="message_contact" name="message_contact" class="form-control" style="height:100px;" placeholder="Escribe aqu&iacute;..."></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" name="verify_contact" id="verify_contact" class=" form-control" placeholder=" 3 + 1 =">
                                        </div>
                                    </div>
                                </div>
                                <input type="submit" value="Enviar" class="btn_1 add_top_20" id="submit-contact">
                            </form>
                        </div>
                        <!-- /col -->
                    </div>
                </div>
                <!-- /col -->
            </div>
            <!-- End row -->
        </div>