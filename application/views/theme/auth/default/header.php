        <header class="py-1">
            <div class="container">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="navbar-brand" href="<?=base_url();?>">
                       	<img class="logo-lg" src="<?=base_url();?>public/theme/auth/<?=THEME_AUTH;?>/images/logo_w3ya.png" style="width: 60px;">
                        <img class="logo-sm" src="<?=base_url();?>public/theme/auth/<?=THEME_AUTH;?>/images/logo_w3ya_blanco.png" style="width: 60px;">
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" style="border: solid 1px #cccccc;">
						<span class="navbar-toggler-icon"></span>
					</button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-4 m-auto">

                            <?php if($login['roles'] == 'admin' || $login['roles'] == 'editor'){ ?>

                            <?php if($login['roles'] == 'editor'){ ?>
                                <li class="nav-item <?php if($menu==2){ ?>active<?php } ?>">
                                    <a class="nav-link" href="<?=base_url();?>">
                                        <i class="fa fa-home" aria-hidden="true"></i> Home
                                    </a>
                                </li>
                            <?php } ?>

                            <li class="nav-item <?php if($menu==1){ ?>active<?php } ?>">
                                <a class="nav-link" href="<?=base_url();?>auth/panel">
                                    <i class="fa fa-tachometer" aria-hidden="true"></i> Panel
                                </a>
                            </li>

                            <?php if($login['roles'] == 'admin'){ ?>
                                <li class="nav-item dropdown <?php if($menu==2){ ?>active<?php } ?>">
                                    <a class="nav-link dropdown-toggle" href="<?=base_url();?>auth/entradas" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                      <i class="fa fa-newspaper-o" aria-hidden="true"></i> Eventos
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="<?=base_url();?>auth/entradas"> Entradas</a>
                                        <a class="dropdown-item" href="<?=base_url();?>auth/categorias"> Categor&iacute;as</a>
                                        <a class="dropdown-item" href="<?=base_url();?>auth/etiquetas"> Etiquetas</a>
                                        <a class="dropdown-item hide" href="<?=base_url();?>auth/comentarios"> Comentarios</a>
                                        <a class="dropdown-item hide" href="<?=base_url();?>auth/Banner"> Banner</a>
                                    </div>
                                </li>
                                <li class="nav-item dropdown <?php if($menu==4){ ?>active<?php } ?>">
                                    <a class="nav-link dropdown-toggle" href="<?=base_url();?>auth/entradas" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                      <i class="fa fa-newspaper-o" aria-hidden="true"></i> Publicar
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="<?=base_url();?>auth/canchas"> Canchas</a>
                                        <a class="dropdown-item" href="<?=base_url();?>auth/servicios"> Servicios</a>
                                    </div>
                                </li>
                            <?php } ?>
                            
                            <?php if($login['roles'] == 'editor'){ ?>
                                <li class="nav-item <?php if($menu==2){ ?>active<?php } ?>">
                                    <a class="nav-link" href="<?=base_url();?>auth/entradas">
                                        <i class="fa fa-newspaper-o" aria-hidden="true"></i> Eventos
                                    </a>
                                </li>
                                    <li class="nav-item <?php if($menu==4){ ?>active<?php } ?>">
                                    <a class="nav-link" href="<?=base_url();?>auth/canchas">
                                        <i class="fa fa-picture-o" aria-hidden="true"></i> Canchas
                                    </a>
                                </li>
                            <?php } ?>

                            <?php if($login['roles'] == 'admin'){ ?>
                                <li class="nav-item <?php if($menu==3){ ?>active<?php } ?>">
                                    <a class="nav-link" href="<?=base_url();?>auth/usuarios">
                                        <i class="fa fa-user-circle" aria-hidden="true"></i> Usuarios
                                    </a>
                                </li>

                                <li class="nav-item <?php if($menu==5){ ?>active<?php } ?>">
                                    <a class="nav-link" href="<?=base_url();?>auth/ajustes">
                                        <i class="fa fa-sliders" aria-hidden="true"></i> Ajustes
                                    </a>
                                </li>
                            <?php } ?>
                            <?php } ?>

                        </ul>
                        <div class="header-right">
                            <a href="<?=base_url();?>auth/perfil" class="contact mr-2" style="background: #007b83;"><i class="fa fa-user-circle-o"></i> Perfil</a>
                            <a href="<?=base_url();?>auth/logout" class="contact"><i class="fa fa-sign-out" aria-hidden="true"></i> Salir</a>
                        </div>
                    </div>
                </nav>
            </div>
        </header>