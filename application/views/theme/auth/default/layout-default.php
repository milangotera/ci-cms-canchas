<!--
Author: W3ya
Author URL: http://w3ya.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="es" class="bg-light">
<head>
    <title><?=$meta_titulo;?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="<?=$meta_keywords;?>" />
    <meta property="og:locale" content="es_ES" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="<?=$meta_titulo;?>" />
    <meta property="og:description" content="<?=$meta_description;?>" />
    <meta property="og:url" content="<?=$meta_titulo;?>" />
    <meta property="og:site_name" content="<?=$meta_site;?>" />
    <meta property="article:publisher" content="<?=$meta_titulo;?>" />
    <meta property="article:author" content="<?=$meta_autor;?>" />
    <meta property="article:tag" content="<?=$meta_titulo;?>" />
    <meta property="article:tag" content="<?=$meta_titulo;?>" />
    <meta property="article:section" content="<?=$meta_titulo;?>" />
    <meta property="article:published_time" content="<?=$meta_titulo;?>" />
    <meta property="og:image" content="<?=$meta_image;?>" />
    <meta property="og:image:width" content="880" />
    <meta property="og:image:height" content="440" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description" content="<?=$meta_description;?>" />
    <meta name="twitter:title" content="<?=$meta_titulo;?>" />
    <meta name="twitter:site" content="<?=$meta_site;?>" />
    <meta name="twitter:image" content="<?=$meta_image;?>" />
    <meta name="twitter:creator" content="<?=$meta_autor_twitter;?>" />
    <link rel="icon" type="image/png" href="<?=base_url();?>public/theme/auth/<?=THEME_AUTH;?>/images/favicon_w3ya.png" />
    <script>
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <link rel="stylesheet" href="<?=base_url();?>public/theme/auth/<?=THEME_AUTH;?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?=base_url();?>public/theme/auth/<?=THEME_AUTH;?>/library/summernote/summernote-bs4.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?=base_url();?>public/theme/auth/<?=THEME_AUTH;?>/css/style.css" type="text/css" media="all" />
    <link rel="stylesheet" href="<?=base_url();?>public/theme/auth/<?=THEME_AUTH;?>/css/style-auth.css" type="text/css" media="all" />
    <script src="<?=base_url();?>public/theme/auth/<?=THEME_AUTH;?>/js/jquery-2.2.3.min.js"></script>
    <script src="<?=base_url();?>public/theme/auth/<?=THEME_AUTH;?>/js/popper.min.js"></script>
    <script src="<?=base_url();?>public/theme/auth/<?=THEME_AUTH;?>/js/bootstrap.js"></script>
    <script src="<?=base_url();?>public/theme/auth/<?=THEME_AUTH;?>/library/summernote/summernote-bs4.js"></script>
    <script src="<?=base_url();?>public/theme/auth/<?=THEME_AUTH;?>/library/select2/select2.min.js"></script>
    <script src="<?=base_url();?>public/theme/auth/<?=THEME_AUTH;?>/library/jquery-seourl/jquery.seourl.min.js"></script>
    <script>
        function mySeoURL(input,target) {
            var options = { 
                'translitarate': true, 
                'uppercase': false, 
                "lowercase": true, 
                "divider": '-' 
            }
            var text = $(input).val();
            $(target).val(text.seoURL(options));
        }
    </script>
    <link href="<?=base_url();?>public/theme/auth/<?=THEME_AUTH;?>/css/font-awesome.css" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Playfair+Display:400,400i,700i" rel="stylesheet">
</head>
<body>
    <!-- mian-content -->
    <div class="main-content" id="home">
        <!-- header -->
        <?php if(isset($header)){ echo $header; } ?>
        <!-- //header -->
    </div>
    <?php if(isset($breadcrumbs)){ echo $breadcrumbs; } ?>
    <?php if($content){ echo $content; } ?>
    <div class="hide" id="base_url"><?=base_url();?></div>
</body>
</html>
