                        <style type="text/css">
                            a.publicidad img{
                                border: 4px;
                                border-color:  #e9ecef;
                                border-style: dashed solid;
                            }

                            a.publicidad img:hover{
                                border-color: #dee2e6;
                            }
                        </style>
                        <aside class="col-lg-4 blog-sldebar-right mt-4">
                            
                            <div class="single-gd" style="background: none; border: none; padding: 0px;">
                                <a href="<?= base_url() ?>contacto" class="publicidad">
                                    <img src="<?=base_url();?>public/theme/front/<?=THEME_FRONT;?>/images/tipos-de-web.png" class="img-fluid" alt="Publicidad">
                                </a>
                            </div>
                                
                            <div class="single-gd">
                                <h4>Recibe Notificaciones</h4>
                                <form action="#" method="post">
                                    <input type="email" name="Email" placeholder="Email" required="">
                                    <div class="button">
                                        <input type="submit" value="Subscribirme">
                                    </div>
                                </form>
                            </div>
                            <div class="single-gd tech-btm aos-init aos-animate" data-aos="fade-down">
                                <h4>&Uacute;ltimas publicaciones </h4>
                                <?php if(!count($entradas_sidebar['data'])){ ?>
                                    <div class="alert alert-warning">
                                        No existen entradas publicadas.
                                    </div>
                                <?php } else { ?>
                                <?php if($entradas_sidebar['total']){ ?>
                                <?php foreach($entradas_sidebar['data'] as $data){ ?>
                                <div class="blog-grids">
                                    <div class="blog-grid-left">
                                        <a href="<?=base_url(); ?><?= $data->seo; ?>">
                                            <img src="<?=base_url();?><?= $data->foto; ?>" class="img-fluid" alt="">
                                        </a>
                                    </div>
                                    <div class="blog-grid-right">
                                        <h5>
                                            <a href="<?=base_url(); ?><?= $data->seo; ?>"><?= $data->titulo; ?></a>
                                        </h5>
                                    </div>
                                    <div class="clearfix"> </div>
                                </div>
                                <?php }}} ?>
                            </div>
                            <div class="single-gd">
                                <h4>Servicios profesionales</h4>
                                <div id="accordion">
                                    <div class="card">
                                        <div class="card-header" id="headingOne">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            ¿Necesitas una p&aacute;gina web?
                                        </button>
                                            </h5>
                                        </div>

                                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                            <div class="card-body">
                                                Dise&ntilde;o de sitios web profesionales y adaptados a tus necesidades y cualquier dispositivo.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header" id="headingTwo">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            Software a medida
                                        </button>
                                            </h5>
                                        </div>
                                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                            <div class="card-body">
                                                Desarrollo de aplicativos que te ayudan a llevar un mejor control y en tiempo real de tus actividades.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header" id="headingThree">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            Asesoramiento y cursos
                                        </button>
                                            </h5>
                                        </div>
                                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                            <div class="card-body">
                                                Trabajemos juntos en crear un proyecto fant&aacute;stico o en llevar a cabo esa idea que tienes para tu negocio.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </aside>