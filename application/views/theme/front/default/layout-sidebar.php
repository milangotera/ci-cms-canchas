<!--
Author: W3ya
Author URL: http://w3ya.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="es">
<head>
    <title><?=$meta_titulo;?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="<?=$meta_keywords;?>" />
    <meta property="og:locale" content="es_ES" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="<?=$meta_titulo;?>" />
    <meta property="og:description" content="<?=$meta_description;?>" />
    <meta property="og:url" content="<?=$meta_titulo;?>" />
    <meta property="og:site_name" content="<?=$meta_site;?>" />
    <meta property="article:publisher" content="<?=$meta_titulo;?>" />
    <meta property="article:author" content="<?=$meta_autor;?>" />
    <meta property="article:tag" content="<?=$meta_titulo;?>" />
    <meta property="article:tag" content="<?=$meta_titulo;?>" />
    <meta property="article:section" content="<?=$meta_titulo;?>" />
    <meta property="article:published_time" content="<?=$meta_titulo;?>" />
    <meta property="og:image" content="<?=$meta_image;?>" />
    <meta property="og:image:width" content="880" />
    <meta property="og:image:height" content="440" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description" content="<?=$meta_description;?>" />
    <meta name="twitter:title" content="<?=$meta_titulo;?>" />
    <meta name="twitter:site" content="<?=$meta_site;?>" />
    <meta name="twitter:image" content="<?=$meta_image;?>" />
    <meta name="twitter:creator" content="<?=$meta_autor_twitter;?>" />
    <link rel="icon" type="image/png" href="<?=base_url();?>public/theme/front/<?=THEME_FRONT;?>/images/logo_w3ya.png" />
    <script>
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- //Meta tag Keywords -->
    <!-- Custom-Files -->
    <link rel="stylesheet" href="<?=base_url();?>public/theme/front/<?=THEME_FRONT;?>/css/bootstrap.css">
    <!-- Bootstrap-Core-CSS -->
    <link rel="stylesheet" href="<?=base_url();?>public/theme/front/<?=THEME_FRONT;?>/css/style.css" type="text/css" media="all" />
    <link rel="stylesheet" href="<?=base_url();?>public/theme/front/<?=THEME_FRONT;?>/css/portfolio.css">
    <link rel="stylesheet" href="<?=base_url();?>public/theme/front/<?=THEME_FRONT;?>/css/aos.css">
    <link href='<?=base_url();?>public/theme/front/<?=THEME_FRONT;?>/css/aos-animation.css' rel='stylesheet prefetch' type="text/css" media="all" />
    <!-- Style-CSS -->
    <!-- font-awesome-icons -->
    <link href="<?=base_url();?>public/theme/front/<?=THEME_FRONT;?>/css/font-awesome.css" rel="stylesheet">
    <!-- //font-awesome-icons -->
    <!-- /Fonts -->
    <link href="//fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Playfair+Display:400,400i,700i" rel="stylesheet">
    <!-- //Fonts -->
</head>
<body>
    <!-- mian-content -->
    <div class="main-content" id="home">
        <!-- header -->
        <?php if(isset($header)){ echo $header; } ?>
        <!-- //header -->
        <?php if(isset($banner)){ echo $banner; } ?>
    </div>
    <?php if(isset($breadcrumbs)){ echo $breadcrumbs; } ?>
    <div class="container">
        <div class="row mt-2 mb-5">
            <div class="col-lg-8 blog-left-content mt-4">
            <?php if(isset($content)){ echo $content; } ?>
            </div>
            <?php if(isset($sidebar)){ echo $sidebar; } ?>
        </div>
    </div>
    <!--footer -->
    <?php if(isset($footer)){ echo $footer; } ?>
    <!-- //footer -->
    <!-- copyright -->
    <?php if(isset($copyright)){ echo $copyright; } ?>
    <!-- //copyright -->
    <!--model-forms-->
    <script src="<?=base_url();?>public/theme/front/<?=THEME_FRONT;?>/js/jquery-2.2.3.min.js"></script>
    <script src="<?=base_url();?>public/theme/front/<?=THEME_FRONT;?>/js/popper.min.js"></script>
    <script src="<?=base_url();?>public/theme/front/<?=THEME_FRONT;?>/js/bootstrap.js"></script>
    <!--/aos -->
    <script src="<?=base_url();?>public/theme/front/<?=THEME_FRONT;?>/js/aos.js"></script>
    <script>
        AOS.init({
            easing: 'ease-out-back',
            duration: 1000
        });
    </script>
    <!--//aos -->
    <!--/counter-->
    <script src="<?=base_url();?>public/theme/front/<?=THEME_FRONT;?>/js/counternew.js"></script>
    <!--//counter-->
    <!--/ start-smoth-scrolling -->
    <script src="<?=base_url();?>public/theme/front/<?=THEME_FRONT;?>/js/move-top.js"></script>
    <script src="<?=base_url();?>public/theme/front/<?=THEME_FRONT;?>/js/easing.js"></script>
    <script>
        jQuery(document).ready(function($) {
            $(".scroll").click(function(event) {
                event.preventDefault();
                $('html,body').animate({
                    scrollTop: $(this.hash).offset().top
                }, 900);
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            /*
            var defaults = {
            containerID: 'toTop', // fading element id
            containerHoverID: 'toTopHover', // fading element hover id
            scrollSpeed: 1200,
            easingType: 'linear' 
            };
            */
            $().UItoTop({
                easingType: 'easeOutQuart'
            });
        });
    </script>
    <!--// end-smoth-scrolling -->
    <!-- //js -->
</body>
</html>
