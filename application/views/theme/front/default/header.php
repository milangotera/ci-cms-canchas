        <header class="py-1">
            <div class="container">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="navbar-brand" href="<?=base_url();?>">
                       	<img class="logo-lg" src="<?=base_url();?>public/theme/front/<?=THEME_FRONT;?>/images/logo_w3ya.png" style="width: 60px;">
                      	<img class="logo-sm" src="<?=base_url();?>public/theme/front/<?=THEME_FRONT;?>/images/logo_w3ya_blanco.png" style="width: 60px;">
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" style="border: solid 1px #cccccc;">
						<span class="navbar-toggler-icon"></span>
					</button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-4 m-auto">
                            <?php if($menu_itens['total']){ ?>
                            <?php foreach($menu_itens['data'] as $data){ ?>
                            <li class="nav-item">
                                <a class="nav-link" target="<?php echo $data->target; ?>" href="<?=base_url();?><?php echo $data->path; ?>">
                                    <i class="<?php echo $data->icono; ?>" aria-hidden="true"></i> <?php echo $data->nombre; ?>
                                </a>
                            </li>
                            <?php }} ?>
                        </ul>
                        <div class="header-right">
                            <?php if($login['signin']){ ?>
                                <?php if($login['roles'] != 'suscriptor'){ ?>
                                    <a href="<?=base_url();?>auth" class="contact mr-2" style="background: #007b83;"><i class="fa fa-tachometer"></i> Panel</a>
                                <?php }else{ ?>
                                    <a href="<?=base_url();?>auth/perfil" class="contact mr-2" style="background: #007b83;"><i class="fa fa-user-circle-o"></i> Perfil </a>
                                <?php } ?>
                                <a href="<?=base_url();?>auth/logout" class="contact"><i class="fa fa-sign-out"></i> Salir</a>
                            <?php }else{ ?>
                                <a href="<?=base_url();?>auth/login" class="contact mr-2" style="background: #007b83;"><i class="fa fa-user-circle-o"></i> Ingresar</a>
                                <a href="<?=base_url();?>auth/signup" class="contact"><i class="fa fa-user-plus"></i> Reg&iacute;strate</a>
                            <?php } ?>
                        </div>
                    </div>
                </nav>
            </div>
        </header>