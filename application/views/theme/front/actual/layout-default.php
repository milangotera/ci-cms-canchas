<!--
Author: W3ya
Author URL: http://w3ya.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=$meta_titulo;?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="<?=$meta_keywords;?>" />
    <meta property="og:locale" content="es_ES" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="<?=$meta_titulo;?>" />
    <meta property="og:description" content="<?=$meta_description;?>" />
    <meta property="og:url" content="<?=$meta_titulo;?>" />
    <meta property="og:site_name" content="<?=$meta_site;?>" />
    <meta property="article:publisher" content="<?=$meta_titulo;?>" />
    <meta property="article:author" content="<?=$meta_autor;?>" />
    <meta property="article:tag" content="<?=$meta_titulo;?>" />
    <meta property="article:tag" content="<?=$meta_titulo;?>" />
    <meta property="article:section" content="<?=$meta_titulo;?>" />
    <meta property="article:published_time" content="<?=$meta_titulo;?>" />
    <meta property="og:image" content="<?=$meta_image;?>" />
    <meta property="og:image:width" content="880" />
    <meta property="og:image:height" content="440" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description" content="<?=$meta_description;?>" />
    <meta name="twitter:title" content="<?=$meta_titulo;?>" />
    <meta name="twitter:site" content="<?=$meta_site;?>" />
    <meta name="twitter:image" content="<?=$meta_image;?>" />
    <meta name="twitter:creator" content="<?=$meta_autor_twitter;?>" />
    <link rel="icon" type="image/png" href="<?=base_url();?>public/theme/front/<?=THEME_FRONT;?>/img/favicon_w3ya.png" />
    <!-- BASE CSS -->
    <link href="<?=base_url();?>public/theme/front/<?=THEME_FRONT;?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url();?>public/theme/front/<?=THEME_FRONT;?>/css/style.css" rel="stylesheet">
    <link href="<?=base_url();?>public/theme/front/<?=THEME_FRONT;?>/css/menu.css" rel="stylesheet">
    <link href="<?=base_url();?>public/theme/front/<?=THEME_FRONT;?>/css/vendors.css" rel="stylesheet">
    <link href="<?=base_url();?>public/theme/front/<?=THEME_FRONT;?>/css/icon_fonts/css/all_icons_min.css" rel="stylesheet">
    <!-- YOUR CUSTOM CSS -->
    <link href="<?=base_url();?>public/theme/front/<?=THEME_FRONT;?>/css/custom.css" rel="stylesheet">
    <link href="<?=base_url();?>public/theme/front/<?=THEME_FRONT;?>/css/blog.css" rel="stylesheet">
</head>
<body>
    <div id="preloader" class="Fixed">
        <center><img src="<?=base_url();?>public/theme/front/<?=THEME_FRONT;?>/img/loading-img.gif" style="width: 320px; margin-top: 20%;"></center>
    </div>
    <!-- /Preload-->
    <div id="page">     
        <?php if(isset($header)){ echo $header; } ?>
        <main style="transform: none;">
            <?php if(isset($content)){ echo $content; } ?>
        </main>
        <!-- /main -->
        <?php if(isset($footer)){ echo $footer; } ?>
        <!--/footer-->
    </div>
    <!-- page -->
    <div id="toTop"></div>
    <!-- Back to top button -->
    <!-- COMMON SCRIPTS -->
    <script src="<?=base_url();?>public/theme/front/<?=THEME_FRONT;?>/js/jquery-2.2.4.min.js"></script>
    <script src="<?=base_url();?>public/theme/front/<?=THEME_FRONT;?>/js/common_scripts.min.js"></script>
    <script src="<?=base_url();?>public/theme/front/<?=THEME_FRONT;?>/js/functions.js"></script>
</body>
</html>