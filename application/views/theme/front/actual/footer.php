<footer>
        <div class="container margin_60_35">
            <div class="row">
                <div class="col-lg-3 col-md-12">
                    <p>
                        <a href="index.html" title="Findoctor">
                            <img src="<?=base_url();?>public/theme/front/<?=THEME_FRONT;?>/img/logo_w3ya_footer.png" data-retina="true" alt="" width="163" height="36" class="img-fluid">
                        </a>
                    </p>
                </div>
                <div class="col-lg-3 col-md-4">
                    <h5>Acerca De</h5>
                    <p style="text-align: justify;">Tu Pichanga es un sitio web donde puedes conseguir tus canchas favoritas sin tantas complicaciones; solo b&uacute;cala, reserva y comparte con tus amigos.</p>
                </div>
                <div class="col-lg-3 col-md-4">
                    <h5>Enlaces &uacute;tiles</h5>
                    <ul class="links">
                        <?php if($menu_itens['total']){ ?>
                        <?php foreach($menu_itens['data'] as $data){ ?>
                        <li>
                        <a target="<?php echo $data->target; ?>" href="<?=base_url();?><?php echo $data->path; ?>"><span class="<?php echo $data->icono; ?>" style="display: none;"></span> <?php echo $data->nombre; ?></a>
                        </li>
                        <?php }} ?>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-4">
                    <h5>Datos de contacto</h5>
                    <ul class="contacts">
                        <li><a href="tel://51924866333"><i class="icon_mobile"></i> + 51 924 866 333</a></li>
                        <li><a href="mailto:contacto@w3ya.com"><i class="icon_mail_alt"></i> info@tupichanga.com</a></li>
                    </ul>
                    <div class="follow_us">
                        <h5>S&iacute;guenos</h5>
                        <ul>
                            <li><a href="#"><i class="social_facebook"></i></a></li>
                            <li><a href="#"><i class="social_twitter"></i></a></li>
                            <li><a href="#"><i class="social_linkedin"></i></a></li>
                            <li><a href="#"><i class="social_instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--/row-->
            <hr>
            <?php if(isset($copyright)){ echo $copyright; } ?>
        </div>
    </footer>