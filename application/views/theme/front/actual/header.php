    <header class="<?=$header_class;?>">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-6">
                    <div id="logo_home">
                        <a href="<?=base_url();?>" title="Findoctor">
                            <img src="<?=base_url();?>public/theme/front/<?=THEME_FRONT;?>/img/logo_w3ya.png" data-retina="true" alt="" width="50" height="50" style="float: left;">
                        </a>
                    </div>
                </div>
                <nav class="col-lg-8 col-6">
                    <a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="#0"><span>Menu mobile</span></a>
                    <ul id="top_access">
                        <?php if($login['signin']){ ?>
                        <?php if($login['roles'] != 'suscriptor'){ ?>
                        
                        <?php } ?>
                        <li id="user">
                            <a href="<?=base_url();?>auth/perfil">
                                <figure><img src="<?=base_url();?><?=$login['avatar'];?>" alt=""></figure>
                                <?=$login['nombre'];?> <?=$login['apellido'];?>
                            </a>
                        </li>
                        <?php }else{ ?>
                        <li><a href="<?=base_url();?>auth/login"><i class="pe-7s-user"></i></a></li>
                        <li><a href="<?=base_url();?>auth/signup"><i class="pe-7s-add-user"></i></a></li>
                        <?php } ?>
                    </ul>
                    <div class="main-menu">
                        <ul>
                            <?php if($menu_itens['total']){ ?>
                            <?php foreach($menu_itens['data'] as $data){ ?>
                            <li>
                                <span>
                                    <a target="<?php echo $data->target; ?>" href="<?=base_url();?><?php echo $data->path; ?>">
                                        <i class="<?php echo $data->icono; ?>" style="display: block; float: left; margin-top: 2px;"></i>&nbsp;<?php echo strtoupper($data->nombre); ?>
                                    </a>
                                </span>
                            </li>
                            <?php }} ?>
                        </ul>
                    </div>
                    <!-- /main-menu -->
                </nav>
            </div>
        </div>
        <!-- /container -->
    </header>