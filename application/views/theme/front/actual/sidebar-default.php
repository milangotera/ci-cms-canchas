<aside class="col-lg-3">
    <div class="widget">
        <form>
            <div class="form-group">
                <input type="text" name="search" id="search" class="form-control" placeholder="Escribe aqu&iacute;...">
            </div>
            <button type="submit" id="submit" class="btn_1"> Buscar</button>
        </form>
    </div>
    <!-- /widget -->

    <div class="widget">
        <div class="widget-title">
            <h4>Eventos recientes</h4>
        </div>
        <ul class="comments-list">
            <?php if($sidebar_entradas['total']){ ?>
            <?php foreach($sidebar_entradas['data'] as $entrada){ ?>
            <li>
                <div class="alignleft">
                    <a href="<?=base_url();?>eventos/<?php echo $entrada->seo; ?>"><img style="width: 80px;" src="<?=base_url();?><?php echo $entrada->foto; ?>" alt=""></a>
                </div>
                <small><?php echo $entrada->fecha; ?></small>
                <h3><a href="<?=base_url();?>eventos/<?php echo $entrada->seo; ?>" title=""><?php echo $entrada->titulo; ?></a></h3>
            </li>
            <?php }} ?>
        </ul>
    </div>
    <!-- /widget -->

    <div class="widget">
        <div class="widget-title">
            <h4>Categor&iacute;as</h4>
        </div>
        <ul class="cats">
            <?php if(count($sidebar_categorias)){ ?>
            <?php foreach($sidebar_categorias as $data){ ?>
            <li><a href="<?=base_url();?>eventos/categorias/<?php echo $data->seo; ?>"><?php echo $data->nombre; ?> <span>(<?php echo $data->total; ?>)</span></a></li>
            <?php }} ?>
        </ul>
    </div>
    <!-- /widget -->

    <div class="widget">
        <div class="widget-title">
            <h4>Etiquetas</h4>
        </div>
        <div class="tags">
            <?php if(count($sidebar_etiquetas)){ ?>
            <?php foreach($sidebar_etiquetas as $data){ ?>
            <a href="#"><?php echo $data->nombre; ?></a>
            <?php }} ?>
        </div>
    </div>
    <!-- /widget -->  
</aside>