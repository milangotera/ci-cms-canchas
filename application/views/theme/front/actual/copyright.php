<div class="row">
    <div class="col-md-8">
        <ul id="additional_links">
        	<li><a href="#">Aviso Legal</a></li>
            <li><a href="#">Pol&iacute;tica de Privacidad</a></li>
            <li><a href="#">Pol&iacute;tica de Cookies</a></li>
        </ul>
    </div>
    <div class="col-md-4">
        <div id="copy">Desarrollado por <a href="https://twitter.com/milang90" target="_blank">Milan Gotera</a></div>
    </div>
</div>