<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . 'core/My_Controller.php';

class Home extends My_Controller {

	public function index($page = null)
	{
		$this->load_front(
			'front/home/home'
		);
	}

	public function error(){
		$this->load_front(
			'front/home/error'
		);
	}
}
