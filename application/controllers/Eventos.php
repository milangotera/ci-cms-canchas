<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . 'core/My_Controller.php';

class Eventos extends My_Controller {

	public function index(){
		$page = 1;
		$this->data['categorias'] = $this->entradas_Model->categorias(['entrada !='=>NULL]);
		$this->data['paginate'] = $this->paginate_Model->paginate('entrada');
		$this->data['paginate']['active'] = $page;
		$start = ($page - 1) * $this->data['paginate']['limit'];
		$end   = $this->data['paginate']['limit'];
		$this->data['entradas'] = $this->entradas_Model->list_public($end,$start);
		$this->data['total'] = count($this->data['entradas']);
		if($this->data['total']){
			for($i = 0; $i < $this->data['total']; $i++){
				$this->data['entradas'][$i]['categorias'] = [];
				for($j = 0; $j < $this->data['categorias']['total']; $j++){
					if($this->data['entradas'][$i]['id'] == $this->data['categorias']['data'][$j]->entrada){
						array_push(
							$this->data['entradas'][$i]['categorias'],
							[$this->data['categorias']['data'][$j]->nombre, $this->data['categorias']['data'][$j]->seo]
						);
					}
				}
			}
		}
		$this->data['sidebar_entradas'] = $this->entradas_Model->list(null,null,0,5,['estatus'=>1, 'tipo'=>1], ['column'=>'id','dir'=>'desc']);
		$this->data['sidebar_categorias'] = $query = $this->db->query("SELECT C.*, (SELECT COUNT(0) FROM entrada_taxonomia T WHERE T.taxonomia = C.id AND T.tipo = 'categoria') AS total FROM categoria C WHERE C.estatus = 1")->result();
		$this->data['sidebar_etiquetas'] = $query = $this->db->query("SELECT E.*, (SELECT COUNT(0) FROM entrada_taxonomia T WHERE T.taxonomia = E.id AND T.tipo = 'etiqueta') AS total FROM etiqueta E WHERE E.estatus = 1")->result();
		$this->data['sidebar']    = $this->load->view('theme/front/'.THEME_FRONT.'/sidebar-default',$this->data, true);
		$this->data['breadcrumb_resumen']    = "Novedades, notitcias y eventos";
		$this->data['breadcrumb_titulo']    = "Eventos";
		$this->data['menu'] = 2;
		$this->data['vista'] = 'eventos/page/';
		$this->load_front(
			'front/entradas/list'
		);
	}

	public function page($page = null){
		//var_dump(intval($page)); exit();
		$page = intval($page) ? intval($page) : 0;
		if(!$page || $page < 0)
			redirect(base_url().'eventos/page/1/');
		$this->data['categorias'] = $this->entradas_Model->categorias(['entrada !='=>NULL]);
		$this->data['paginate'] = $this->paginate_Model->paginate('entrada');
		$this->data['paginate']['active'] = $page;
		$start = ($page - 1) * $this->data['paginate']['limit'];
		$end   = $this->data['paginate']['limit'];
		$this->data['entradas'] = $this->entradas_Model->list_public($end,$start);
		$this->data['total'] = count($this->data['entradas']);
		if(empty($this->data['entradas']))
			$this->load_front('front/home/error');
		else{
			if($this->data['total']){
				for($i = 0; $i < $this->data['total']; $i++){
					$this->data['entradas'][$i]['categorias'] = [];
					for($j = 0; $j < $this->data['categorias']['total']; $j++){
						if($this->data['entradas'][$i]['id'] == $this->data['categorias']['data'][$j]->entrada){
							array_push(
								$this->data['entradas'][$i]['categorias'],
								[$this->data['categorias']['data'][$j]->nombre, $this->data['categorias']['data'][$j]->seo]
							);
						}
					}
				}
			}
		$this->data['sidebar_entradas'] = $this->entradas_Model->list(null,null,0,5,['estatus'=>1, 'tipo'=>1], ['column'=>'id','dir'=>'desc']);
		$this->data['sidebar_categorias'] = $query = $this->db->query("SELECT C.*, (SELECT COUNT(0) FROM entrada_taxonomia T WHERE T.taxonomia = C.id AND T.tipo = 'categoria') AS total FROM categoria C WHERE C.estatus = 1")->result();
		$this->data['sidebar_etiquetas'] = $query = $this->db->query("SELECT E.*, (SELECT COUNT(0) FROM entrada_taxonomia T WHERE T.taxonomia = E.id AND T.tipo = 'etiqueta') AS total FROM etiqueta E WHERE E.estatus = 1")->result();
		$this->data['sidebar']    = $this->load->view('theme/front/'.THEME_FRONT.'/sidebar-default',$this->data, true);
		$this->data['breadcrumb_resumen']    = "Novedades, notitcias y eventos";
		$this->data['breadcrumb_titulo']    = "Eventos";
			$this->data['menu'] = 2;
			$this->data['vista'] = 'eventos/page/';
			$this->load_front(
				'front/entradas/list'
			);
		}	
	}

	public function categorias($categoria = null, $page = null){
		//$page = intval($page) ? intval($page) : 0;
		if(!$page || $page < 0){
			redirect(base_url().'eventos/categorias/'.$categoria.'/page/1/');
		}

		$this->data['paginate'] = $this->paginate_Model->paginate('entrada');
		$this->data['paginate']['active'] = $page;

		$this->data['categorias'] = $this->entradas_Model->categorias(['entrada !='=>NULL]);
		
		$start = ($page - 1) * $this->data['paginate']['limit'];
		$end   = $this->data['paginate']['limit'];
		if(!$categoria){
			$this->data['entradas'] = $this->entradas_Model->list_public($end,$start);
		}
		else{
			$this->data['entradas'] = $this->entradas_Model->list_categorias($categoria,$end,$start);
		}
		$this->data['total'] = count($this->data['entradas']);
		if(empty($this->data['entradas']))
			$this->load_front('front/home/error');
		else{
			if($this->data['total']){
				for($i = 0; $i < $this->data['total']; $i++){
					$this->data['entradas'][$i]['categorias'] = [];
					for($j = 0; $j < $this->data['categorias']['total']; $j++){
						if($this->data['entradas'][$i]['id'] == $this->data['categorias']['data'][$j]->entrada){
							array_push(
								$this->data['entradas'][$i]['categorias'],
								[$this->data['categorias']['data'][$j]->nombre, $this->data['categorias']['data'][$j]->seo]
							);
						}
					}
				}
			}
			$this->data['sidebar_entradas'] = $this->entradas_Model->list(null,null,0,5,['estatus'=>1, 'tipo'=>1], ['column'=>'id','dir'=>'desc']);
			$this->data['sidebar_categorias'] = $query = $this->db->query("SELECT C.*, (SELECT COUNT(0) FROM entrada_taxonomia T WHERE T.taxonomia = C.id AND T.tipo = 'categoria') AS total FROM categoria C WHERE C.estatus = 1")->result();
			$this->data['sidebar_etiquetas'] = $query = $this->db->query("SELECT E.*, (SELECT COUNT(0) FROM entrada_taxonomia T WHERE T.taxonomia = E.id AND T.tipo = 'etiqueta') AS total FROM etiqueta E WHERE E.estatus = 1")->result();
			$this->data['sidebar']    = $this->load->view('theme/front/'.THEME_FRONT.'/sidebar-default',$this->data, true);
			$this->data['breadcrumb_resumen']    = "Novedades, notitcias y eventos";
			$this->data['breadcrumb_titulo']    = "Eventos";
			$this->data['breadcrumb_titulo']    = ucwords($categoria);
			$this->data['menu'] = 2;
			$this->data['vista'] = 'eventos/categorias/'.$categoria.'/page/';
			$this->load_front(
				'front/entradas/list'
			);
		}
	}

	public function detalle($seo = null){
		if(!$seo)
			redirect(base_url().'entradas');
		$this->data['entradas'] = $this->entradas_Model->list_seo($seo);
		if(empty($this->data['entradas']))
			$this->load_front('front/home/error');
		else{
			$this->data['categorias'] = $this->entradas_Model->categorias(['entrada !='=>NULL]);
			$this->data['sidebar_entradas'] = $this->entradas_Model->list(null,null,0,5,['estatus'=>1, 'tipo'=>1], ['column'=>'id','dir'=>'desc']);
			$this->data['sidebar_categorias'] = $query = $this->db->query("SELECT C.*, (SELECT COUNT(0) FROM entrada_taxonomia T WHERE T.taxonomia = C.id AND T.tipo = 'categoria') AS total FROM categoria C WHERE C.estatus = 1")->result();
			$this->data['sidebar_etiquetas'] = $query = $this->db->query("SELECT E.*, (SELECT COUNT(0) FROM entrada_taxonomia T WHERE T.taxonomia = E.id AND T.tipo = 'etiqueta') AS total FROM etiqueta E WHERE E.estatus = 1")->result();
			$this->data['sidebar']    = $this->load->view('theme/front/'.THEME_FRONT.'/sidebar-default',$this->data, true);
			if($this->input->method() == "post"){
				$this->form_validation->set_rules('comentar_nombre', 'Nombre', 'required', ['required' => 'El %s es requerido.']);
				$this->form_validation->set_rules('comentar_email', 'Email', 'required', ['required' => 'El %s es requerido.']);
				$this->form_validation->set_rules('comentar_comentario', 'Comentarios', 'required', ['required' => 'Escribe un comentario.']);
				if($this->form_validation->run()){
					$registrar_comentario = $this->comentarios_Model->insert([
	                    "autor_nombre" => $this->input->post('comentar_nombre'),
	                    "autor_email" => $this->input->post('comentar_email'),
	                    "comentario" => $this->input->post('comentar_comentario'),
	                    "autor_web" => $this->input->post('comentar_web'),
	                    "entrada" => $this->data['entradas'][0]['id'],
	                    "avatar" => $this->session->userdata('avatar'),
	                    "estatus" => 1
	                ]);
	                if($registrar_comentario){
	                	$this->session->set_flashdata('mensaje', ["class"=>"success","text"=>"Gracias, Se ha registrado tu comentario."]);
	                	redirect(base_url().$this->data['entradas'][0]['seo']);
	                }
				}
			}
			if($this->data['entradas'][0]['comentarios']==1){
				$this->data['comentarios'] = $this->comentarios_Model->list(null,null,['estatus'=>1,'entrada'=>$this->data['entradas'][0]['id']]);
				$this->data['layout_comentarios']    = $this->load->view('front/entradas/comentarios',$this->data, true);
				$this->data['layout_comentar']    = $this->load->view('front/entradas/comentar',$this->data, true);
			}

			$this->data['meta_titulo']   = "W3yA :: ".$this->data['entradas'][0]['titulo'];
			$this->data['meta_description']   = $this->data['entradas'][0]['resumen'];
			$this->data['meta_image']   = base_url().$this->data['entradas'][0]['foto'];

			$this->data['sidebar_entradas'] = $this->entradas_Model->list(null,null,0,5,['estatus'=>1, 'tipo'=>1], ['column'=>'id','dir'=>'desc']);
			$this->data['sidebar_categorias'] = $query = $this->db->query("SELECT C.*, (SELECT COUNT(0) FROM entrada_taxonomia T WHERE T.taxonomia = C.id AND T.tipo = 'categoria') AS total FROM categoria C WHERE C.estatus = 1")->result();
			$this->data['sidebar_etiquetas'] = $query = $this->db->query("SELECT E.*, (SELECT COUNT(0) FROM entrada_taxonomia T WHERE T.taxonomia = E.id AND T.tipo = 'etiqueta') AS total FROM etiqueta E WHERE E.estatus = 1")->result();
			$this->data['sidebar']    = $this->load->view('theme/front/'.THEME_FRONT.'/sidebar-default',$this->data, true);
			$this->data['breadcrumb_resumen']    = $this->data['entradas'][0]['titulo'];
			$this->data['breadcrumb_titulo']    = "Eventos";
			
			$this->load_front(
				'front/entradas/detalle'
			);
		}
	}

}
