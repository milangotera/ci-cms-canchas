<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . 'core/My_Controller.php';

class Auth extends My_Controller {

	public function __construct(){
		parent::__construct();
		$this->data['menu']   = 1;
	}

	public function index()
	{
		switch ($this->session->userdata('roles')) {
			case '':
				$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"Identificate para continuar."]);
				redirect(base_url().'auth/login');
			break;
			case 'admin':
				redirect(base_url().'auth/panel');
			break;
			case 'editor':
				redirect(base_url().'auth/panel');
			break;
			case 'suscriptor':
				redirect(base_url().'home');
			break;
			default:
				$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"Identificate para continuar."]);
				redirect(base_url().'auth/login');
			break;
		}
	}

	public function panel(){
		$this->usuarios_Model->is_login(['admin','editor']);
		$this->data['menu'] = 1;
		$this->data['mensajes'] = [];
		$this->data['entradas'] = $this->entradas_Model->count();
		$this->data['usuarios'] = $this->usuarios_Model->count();
		$this->data['comentarios'] = $this->comentarios_Model->count();
		$this->data['newsletter'] = $this->newsletter_Model->count();
		$this->data['revisar'] = $this->comentarios_Model->count(['estatus'=>0]);
		$this->data['borrador'] = $this->entradas_Model->count(['estatus'=>2,]);
		$this->data['activar']  = $this->usuarios_Model->count(['estatus'=>0]);
		if($this->data['borrador'])
			array_push($this->data['mensajes'], ['danger','Hay '.$this->data['borrador'].' entradas en borrador para publicar.']);
		if($this->data['revisar'])
			array_push($this->data['mensajes'], ['danger','Hay '.$this->data['revisar'].' comentarios esperando para revisar.']);
		if($this->data['activar'])
			array_push($this->data['mensajes'], ['danger','Hay '.$this->data['activar'].' usuarios esperando por activación.']);
		$this->load_auth('auth/panel/estadisticas');
	}

	public function entradas($vista = null, $opciones = null){
		$this->usuarios_Model->is_login(['admin','editor']);
		$this->data['menu'] = 2;
		switch ($vista) {
			case '':
				$page = 1;
				$this->data['paginate'] = $this->paginate_Model->paginate('entrada');
				$this->data['paginate']['active'] = $page;
				$start = ($page - 1) * $this->data['paginate']['limit'];
				$end   = $this->data['paginate']['limit'];
				$this->data['entradas'] = $this->entradas_Model->list(null,null,$start,$end, null, ['column'=>'id', 'dir'=>'desc']);
				$this->data['entradas']['total'] = $this->data['paginate']['itens'];
				//$this->data['categorias'] = $this->categorias_Model->list();
				//$this->data['autor'] = $this->usuarios_Model->list('id, nombre, apellido',null,null,['roles'=>'suscriptor']);
				$this->load_auth('auth/entradas/listar');
			break;
			case 'page':
				$page = intval($opciones) ? intval($opciones) : false;
				if((!$page) || ($page < 0))
					redirect(base_url().'auth/entradas');
				$this->data['paginate'] = $this->paginate_Model->paginate('entrada');
				$this->data['paginate']['active'] = $page;
				$start = ($page - 1) * $this->data['paginate']['limit'];
				$end   = $this->data['paginate']['limit'];
				$this->data['entradas'] = $this->entradas_Model->list(null,null,$start,$end, null, ['column'=>'id', 'dir'=>'desc']);
				$this->data['entradas']['total'] = $this->data['paginate']['itens'];
				$this->load_auth('auth/entradas/listar');
			break;
			case 'nueva':
				$this->data['fecha'] = $this->input->post('fecha') ? $this->input->post('fecha') : date("Y-m-d");
				$previa = $this->input->post('foto') ? $this->input->post('foto') : "/public/theme/".THEME_ASSETS."/images/icono-galeria.png";
				$this->data['previa'] = base_url().$previa;
				$this->data['etiquetas'] = $this->etiquetas_Model->list(null, null, ['estatus'=>1]);
				$this->data['categorias'] = $this->categorias_Model->list(null, null, ['estatus'=>1]);
				$this->data['autor'] = $this->usuarios_Model->list('id, nombre, apellido',null,null, ['roles !='=>'suscriptor']);
				if($this->input->method() == "post"){
					$this->form_validation->set_rules('titulo', 'Titulo', 'required', ['required' => '%s es requerido.']);
	                $this->form_validation->set_rules('seo', 'Seo Url', 'required', ['required' => '%s es requerido.']);
	                $this->form_validation->set_rules('foto', 'Foto principal', 'required', ['required' => '%s es requerido.']);
	                $this->form_validation->set_rules('contenido', 'Contenido', 'required', ['required' => '%s es requerido.']);
	                $this->form_validation->set_rules('resumen', 'Resumen', 'required', ['required' => '%s es requerido.']);
	                $this->form_validation->set_rules('autor', 'Autor', 'required', ['required' => '%s es requerido.']);
	                //$this->form_validation->set_rules('categoria[]', 'Categoria', 'required', ['required' => '%s es requerido.']);
	                $this->form_validation->set_rules('fecha', 'Fecha de publicacion', 'required', ['required' => '%s es requerido.']);
	                
	                if($this->form_validation->run()){
	                    $buscar = $this->entradas_Model->count([
	                    	"seo"=>$this->input->post('seo')
	                    ]);
	                    if($buscar == 0){
	                    	$entrada = $this->entradas_Model->insert([
	                    		"seo" => $this->input->post('seo'),
	                    		"titulo" => $this->input->post('titulo'),
	                    		"resumen" => $this->input->post('resumen'),
	                    		"contenido" => $this->input->post('contenido'),
	                    		"foto" => $previa,
	                    		"autor" => $this->input->post('autor'),
	                    		"fecha" => $this->input->post('fecha'),
	                    		"comentarios" => $this->input->post('comentarios'),
	                    		"tipo" => $this->input->post('tipo'),
	                    		"estatus" => $this->input->post('estatus'),
	                    		"user" => $this->session->userdata('id'),
	                    	]);
	                    	if($entrada){
	                    		$categorias = $this->input->post('categoria');
			                    $etiquetas  = $this->input->post('etiqueta');
	                    		if(count($categorias)){
	                    			for ($i=0; $i < count($categorias); $i++) { 
	                    				$taxonomia = $this->entradas_Model->add_taxonomia([
				                    		"entrada" => $entrada,
				                    		"taxonomia" => $categorias[$i],
				                    		"tipo" => "categoria"
				                    	]);
	                    			}
	                    		}
	                    		if(count($etiquetas)){
	                    			for ($i=0; $i < count($etiquetas); $i++) { 
	                    				$taxonomia = $this->entradas_Model->add_taxonomia([
				                    		"entrada" => $entrada,
				                    		"taxonomia" => $etiquetas[$i],
				                    		"tipo" => "etiqueta"
				                    	]);
	                    			}
	                    		}
	                    		$this->session->set_flashdata('mensaje', ["class"=>"success","text"=>"Se ha registrado una nueva entrada."]);
	                    		redirect(base_url().'auth/entradas');
	                    	}
	                    	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"no se pudo guardar los datos de la entrada."]);
	                    }
	                    else{
	                    	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"Ya existe una entrada con un seo igual."]);
	                    }
	                }
				}
				$this->load_auth('auth/entradas/nueva');
			break;
			// EDITAR CATEGORIA
			case 'editar':
				$this->usuarios_Model->is_login(['admin'], 'auth/entradas', 'Sin permisos para editar este contenido.');
				$opciones = $opciones ? $opciones : false;
				if($opciones){
					$this->data['entradas'] = $this->entradas_Model->list(
						null,
	                	null,
	                	null,
	                	null,
	                	["id"=>$opciones]
	                );
	                if($this->data['entradas']['total'] > 0){
	                	// VALIDAMOS QUE HA SOLICITADO ACTUALIZAR
	                	$this->data['etiquetas'] = $this->etiquetas_Model->list(null, null, ['estatus'=>1]);
						$this->data['categorias'] = $this->categorias_Model->list(null, null, ['estatus'=>1]);
						$this->data['autor'] = $this->usuarios_Model->list('id, nombre, apellido',null,null, ['roles !='=>'suscriptor']);
						$this->data['taxonomias'] = $this->entradas_Model->taxonomias(['entrada'=>$this->data['entradas']['data'][0]->id]);
	                	if($this->input->method() == "post"){
							$this->form_validation->set_rules('titulo', 'Titulo', 'required', ['required' => '%s es requerido.']);
			                $this->form_validation->set_rules('seo', 'Seo Url', 'required', ['required' => '%s es requerido.']);
			                $this->form_validation->set_rules('foto', 'Foto principal', 'required', ['required' => '%s es requerido.']);
			                $this->form_validation->set_rules('contenido', 'Contenido', 'required', ['required' => '%s es requerido.']);
			                $this->form_validation->set_rules('resumen', 'Resumen', 'required', ['required' => '%s es requerido.']);
			                $this->form_validation->set_rules('autor', 'Autor', 'required', ['required' => '%s es requerido.']);
			                //$this->form_validation->set_rules('categoria[]', 'Categoria', 'required', ['required' => '%s es requerido.']);
			                $this->form_validation->set_rules('fecha', 'Fecha de publicacion', 'required', ['required' => '%s es requerido.']);
			                if($this->form_validation->run()){
			                    $buscar = $this->entradas_Model->count([
			                    	"seo"=>$this->input->post('seo'),
			                    	"id !=" => $opciones
			                    ]);
			                    if($buscar == 0){
			                    	$actualizar = $this->entradas_Model->update(
			                    		[
				                    		"seo" => $this->input->post('seo'),
				                    		"titulo" => $this->input->post('titulo'),
				                    		"resumen" => $this->input->post('resumen'),
				                    		"contenido" => $this->input->post('contenido'),
				                    		"foto" => $this->input->post('foto'),
				                    		"autor" => $this->input->post('autor'),
				                    		"fecha" => $this->input->post('fecha'),
				                    		"comentarios" => $this->input->post('comentarios'),
				                    		"tipo" => $this->input->post('tipo'),
				                    		"estatus" => $this->input->post('estatus'),
				                    	],
			                    		["id"=>$opciones]
			                    	);
			                    	if($actualizar){
			                    		$categorias = $this->input->post('categoria');
			                    		$etiquetas  = $this->input->post('etiqueta');
			                    		$taxonomias  = $this->entradas_Model->rem_taxonomia(["entrada" => $opciones]);
			                    		if(count($categorias)){
			                    			for ($i=0; $i < count($categorias); $i++) { 
			                    				$taxonomia = $this->entradas_Model->add_taxonomia([
						                    		"entrada" => $opciones,
						                    		"taxonomia" => $categorias[$i],
						                    		"tipo" => "categoria"
						                    	]);
			                    			}
			                    		}
			                    		if(count($etiquetas)){
			                    			for ($i=0; $i < count($etiquetas); $i++) { 
			                    				$taxonomia = $this->entradas_Model->add_taxonomia([
						                    		"entrada" => $opciones,
						                    		"taxonomia" => $etiquetas[$i],
						                    		"tipo" => "etiqueta"
						                    	]);
			                    			}
			                    		}
			                    		$this->session->set_flashdata('mensaje', ["class"=>"success","text"=>"Se ha actualizado los detalles de la entrada."]);
			                    		redirect(base_url().'auth/entradas/editar/'.$opciones);
			                    	}
			                    	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"Ya existe una categoría con los datos enviados."]);
			                    }
			                    else{
			                    	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"Ya existe una entrada igual."]);
			                    }
			                }
						}
	                }
	                else{
	                	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"No se ha encontrado la entrada que buscas."]);
	                	redirect(base_url().'auth/entradas');
	                }
					
				}
				else{
					$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"ingresa una entrada para editar."]);
					redirect(base_url().'auth/entradas');
				}
				$this->load_auth('auth/entradas/editar');
			break;
			// BORRAR CATEGORIA
			case 'borrar':
				$this->usuarios_Model->is_login(['admin'], 'auth/entradas', 'Sin permisos para borrar este contenido.');
				$opciones = $opciones ? $opciones : false;
				if($opciones){
					$buscar = $this->entradas_Model->count([
	                    "id"=>$opciones
	                ]);
	                if($buscar > 0){
	                	$entrada = $this->entradas_Model->delete([
	                    	"id"=>$opciones
	                	]);
	                	$taxonomias = $this->entradas_Model->rem_taxonomia([
	                    	"entrada"=>$opciones
	                	]);
	                	$comentarios = $this->comentarios_Model->delete([
	                    	"entrada"=>$opciones
	                	]);
	                	$this->session->set_flashdata('mensaje', ["class"=>"success","text"=>"Se ha borrado una entrada exitosamente."]);
	                }
	                else{
	                	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"No se ha encontrado la entrada que buscas."]);
	                }
					
				}
				else{
					$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"ingresa una entrada para borar."]);
				}
				redirect(base_url().'auth/entradas');
			break;
			default:
				redirect(base_url().'auth/entradas');
			break;
		}
	}

	public function categorias($vista = null, $opciones = null){
		$this->usuarios_Model->is_login(['admin','editor']);
		$this->data['menu'] = ($this->session->userdata('roles')=="admin") ? 2 : 6;
		switch ($vista) {
			// LISTAR CATEGORIAS
			case '':
				$page = 1;
				$this->data['paginate'] = $this->paginate_Model->paginate('categoria');
				$this->data['paginate']['active'] = $page;
				$start = ($page - 1) * $this->data['paginate']['limit'];
				$end   = $this->data['paginate']['limit'];
				$this->data['categorias'] = $this->categorias_Model->list($start,$end,null,['column'=>'id', 'dir'=>'desc']);
				$this->data['categorias']['total'] = $this->data['paginate']['itens'];
				$this->load_auth('auth/categorias/listar');
			break;
			case 'page':
				$page = intval($opciones) ? intval($opciones) : false;
				if((!$page) || ($page < 0))
					redirect(base_url().'auth/categorias');
				$this->data['paginate'] = $this->paginate_Model->paginate('categoria');
				$this->data['paginate']['active'] = $page;
				$start = ($page - 1) * $this->data['paginate']['limit'];
				$end   = $this->data['paginate']['limit'];
				$this->data['categorias'] = $this->categorias_Model->list($start,$end, null,['column'=>'id', 'dir'=>'desc']);
				$this->data['categorias']['total'] = $this->data['paginate']['itens'];
				$this->load_auth('auth/categorias/listar');
			break;
			// CREAR NUEVA CATEGORIA
			case 'nueva':
				if($this->input->method() == "post"){
					$this->form_validation->set_rules('nombre', 'Nombre', 'required', ['required' => '%s es requerido.']);
	                $this->form_validation->set_rules('seo', 'Seo Url', 'required', ['required' => '%s es requerido.']);
	                if($this->form_validation->run()){
	                    $buscar = $this->categorias_Model->count([
	                    	"seo"=>$this->input->post('seo')
	                    ]);
	                    if($buscar == 0){
	                    	$insertar = $this->categorias_Model->insert([
	                    		"nombre" => $this->input->post('nombre'),
	                    		"seo" => $this->input->post('seo'),
	                    		"estatus" => $this->input->post('estatus'),
	                    	]);
	                    	if($insertar){
	                    		$this->session->set_flashdata('mensaje', ["class"=>"success","text"=>"Se ha registrado una nueva categoría"]);
	                    		redirect(base_url().'auth/categorias');
	                    	}
	                    	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"no se pudo guardar los datos de la categoría."]);
	                    }
	                    else{
	                    	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"Ya existe una categoría igual."]);
	                    }
	                }
				}
				$this->load_auth('auth/categorias/nueva');
			break;
			// EDITAR CATEGORIA
			case 'editar':
				$this->usuarios_Model->is_login(['admin'], 'auth/categorias', 'Sin permisos para editar este contenido.');
				$opciones = $opciones ? $opciones : false;
				if($opciones){
					$this->data['categorias'] = $this->categorias_Model->list(
	                	null,
	                	null,
	                	["id"=>$opciones]
	                );
	                if($this->data['categorias']['total'] > 0){
	                	// VALIDAMOS QUE HA SOLICITADO ACTUALIZAR
	                	if($this->input->method() == "post"){
							$this->form_validation->set_rules('nombre', 'Nombre', 'required', ['required' => '%s es requerido.']);
			                $this->form_validation->set_rules('seo', 'Seo Url', 'required', ['required' => '%s es requerido.']);
			                if($this->form_validation->run()){
			                    $buscar = $this->categorias_Model->count([
			                    	"seo"=>$this->input->post('seo'),
			                    	"id !=" => $opciones
			                    ]);
			                    if($buscar == 0){
			                    	$actualizar = $this->categorias_Model->update(
			                    		[
			                    		"nombre" => $this->input->post('nombre'),
			                    		"seo" => $this->input->post('seo'),
			                    		"estatus" => $this->input->post('estatus')
			                    		],
			                    		["id"=>$opciones]
			                    	);
			                    	if($actualizar){
			                    		$this->session->set_flashdata('mensaje', ["class"=>"success","text"=>"Se ha actualizado una nueva categoría"]);
			                    		redirect(base_url().'auth/categorias');
			                    	}
			                    	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"Ya existe una categoría con los datos enviados."]);
			                    }
			                    else{
			                    	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"Ya existe una categoría igual."]);
			                    }
			                }
						}
	                }
	                else{
	                	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"No se ha encontrado la categoría que buscas."]);
	                	redirect(base_url().'auth/categorias');
	                }
					
				}
				else{
					$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"ingresa una categoría para editar."]);
					redirect(base_url().'auth/categorias');
				}
				$this->load_auth('auth/categorias/editar');
			break;
			// BORRAR CATEGORIA
			case 'borrar':
				$this->usuarios_Model->is_login(['admin'], 'auth/categorias', 'Sin permisos para borrar este contenido.');
				$opciones = $opciones ? $opciones : false;
				if($opciones){
					$buscar = $this->categorias_Model->count([
	                    "id"=>$opciones
	                ]);
	                if($buscar > 0){
	                	$borrar = $this->categorias_Model->delete([
	                    	"id"=>$opciones
	                	]);
	                	$this->session->set_flashdata('mensaje', ["class"=>"success","text"=>"Se ha borrado una categoría exitosamente."]);
	                }
	                else{
	                	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"No se ha encontrado la categoría que buscas."]);
	                }
					
				}
				else{
					$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"ingresa una categoría para borar."]);
				}
				redirect(base_url().'auth/categorias');
			break;
			
			default:
				redirect(base_url().'auth/categorias');
			break;
		}
	}

	public function canchas($vista = null, $opciones = null){
		$this->usuarios_Model->is_login(['admin','editor']);
		$this->data['menu'] = 4;
		switch ($vista) {
			case '':
				$page = 1;
				$this->data['paginate'] = $this->paginate_Model->paginate('cancha');
				$this->data['paginate']['active'] = $page;
				$start = ($page - 1) * $this->data['paginate']['limit'];
				$end   = $this->data['paginate']['limit'];
				$this->data['canchas'] = $this->canchas_Model->list(null,null,$start,$end, null, ['column'=>'id', 'dir'=>'desc']);
				$this->data['canchas']['total'] = $this->data['paginate']['itens'];
				$this->load_auth('auth/canchas/listar');
			break;
			case 'page':
				$page = intval($opciones) ? intval($opciones) : false;
				if((!$page) || ($page < 0))
					redirect(base_url().'auth/canchas');
				$this->data['paginate'] = $this->paginate_Model->paginate('cancha');
				$this->data['paginate']['active'] = $page;
				$start = ($page - 1) * $this->data['paginate']['limit'];
				$end   = $this->data['paginate']['limit'];
				$this->data['canchas'] = $this->canchas_Model->list(null,null,$start,$end, null, ['column'=>'id', 'dir'=>'desc']);
				$this->data['canchas']['total'] = $this->data['paginate']['itens'];
				$this->load_auth('auth/canchas/listar');
			break;
			case 'nueva':
				$previa = $this->input->post('foto') ? $this->input->post('foto') : "public/theme/auth/".THEME_AUTH."/images/icono-cancha-gris.png";
				$this->data['previa'] = base_url().$previa;
				$this->data['servicios'] = $this->servicios_Model->list(null, null, ['estatus'=>1]);
				$this->data['autor'] = $this->usuarios_Model->list('id, nombre, apellido',null,null, ['roles !='=>'suscriptor']);
				if($this->input->method() == "post"){
					$this->form_validation->set_rules('nombre', 'Nombre', 'required', ['required' => '%s es requerido.']);
					$this->form_validation->set_rules('seo', 'Seo', 'required', ['required' => '%s es requerido.']);
					$this->form_validation->set_rules('resumen', 'Descripcion', 'required', ['required' => '%s es requerido.']);
					$this->form_validation->set_rules('pais', 'Pais', 'required', ['required' => '%s es requerido.']);
					$this->form_validation->set_rules('departamento', 'Departamento', 'required', ['required' => '%s es requerido.']);
					$this->form_validation->set_rules('provincia', 'Provincia', 'required', ['required' => '%s es requerido.']);
					$this->form_validation->set_rules('distrito', 'Distrito', 'required', ['required' => '%s es requerido.']);
					$this->form_validation->set_rules('direccion', 'Direccion', 'required', ['required' => '%s es requerido.']);
					$this->form_validation->set_rules('servicios[]', 'Servicios', 'required', ['required' => '%s es requerido.']);
					//$this->form_validation->set_rules('mapa', 'Mapa', 'required', ['required' => '%s es requerido.']);
					//$this->form_validation->set_rules('foto', 'Imagen', 'required', ['required' => '%s es requerido.']);
					//$this->form_validation->set_rules('video', 'Video', 'required', ['required' => '%s es requerido.']);
					$this->form_validation->set_rules('abre', 'Abre', 'required', ['required' => '%s es requerido.']);
					$this->form_validation->set_rules('cierra', 'Cierra', 'required', ['required' => '%s es requerido.']);
					$this->form_validation->set_rules('dia', 'Hora diurna', 'required', ['required' => '%s es requerido.']);
					$this->form_validation->set_rules('noche', 'Hora nocturna', 'required', ['required' => '%s es requerido.']);
					$this->form_validation->set_rules('canchas', 'Canchas', 'required', ['required' => '%s es requerido.']);
					$this->form_validation->set_rules('estrellas', 'Estrellas', 'required', ['required' => '%s es requerido.']);
					$this->form_validation->set_rules('telefono', 'Telefono', 'required', ['required' => '%s es requerido.']);
					//$this->form_validation->set_rules('pagina', 'Pagina', 'required', ['required' => '%s es requerido.']);
					//$this->form_validation->set_rules('facebook', 'Facebook', 'required', ['required' => '%s es requerido.']);
					//$this->form_validation->set_rules('twitter', 'Twitter', 'required', ['required' => '%s es requerido.']);
					//$this->form_validation->set_rules('instagran', 'Instagran', 'required', ['required' => '%s es requerido.']);
					//$this->form_validation->set_rules('autor', 'Autor', 'required', ['required' => '%s es requerido.']);
					//$this->form_validation->set_rules('comentarios', 'Comentarios', 'required', ['required' => '%s es requerido.']);
					//$this->form_validation->set_rules('destacar', 'Destacar', 'required', ['required' => '%s es requerido.']);
					//$this->form_validation->set_rules('estatus', 'Estatus', 'required', ['required' => '%s es requerido.']);
					//$this->form_validation->set_rules('premiun', 'Premiun', 'required', ['required' => '%s es requerido.']);

	                if($this->form_validation->run()){
	                    $buscar = $this->canchas_Model->count([
	                    	"seo"=>$this->input->post('seo')
	                    ]);
	                    if($buscar == 0){
	                    	$admin =  $this->usuarios_Model->is_login(['admin'],null,null);
							if($admin){

							}
	                    	$cancha = $this->canchas_Model->insert([
	                    		"nombre" => $this->input->post('nombre'),
	                    		"seo" => $this->input->post('seo'),
	                    		"resumen" => $this->input->post('resumen'),
	                    		"direccion" => $this->input->post('direccion'),
	                    		"servicios" => implode(",",$this->input->post('servicios')),
	                    		"dia" => $this->input->post('dia'),
	                    		"noche" => $this->input->post('noche'),
	                    		"abre" => $this->input->post('abre'),
	                    		"cierra" => $this->input->post('cierra'),
	                    		"imagen" => ($this->input->post('foto') ? $this->input->post('foto') : "public/theme/auth/".THEME_AUTH."/images/icono-cancha.png"),
	                    		"pais" => $this->input->post('pais'),
	                    		"estado" => $this->input->post('departamento'),
	                    		"provincia" => $this->input->post('provincia'),
	                    		"distrito" => $this->input->post('distrito'),
	                    		"video" => $this->input->post('video'),
	                    		"mapa" => $this->input->post('mapa'),
	                    		"estrellas" => $this->input->post('estrellas'),
	                    		"telefonos" => $this->input->post('telefono'),
	                    		"facebook" => $this->input->post('facebook'),
	                    		"twitter" => $this->input->post('twitter'),
	                    		"instagran" => $this->input->post('instagran'),
	                    		"pagina" => $this->input->post('pagina'),
	                    		"canchas" => $this->input->post('canchas'),
	                    		"user" => ($admin ? $this->input->post('autor') : $this->session->userdata('id')),
	                    		"comentarios" => $this->input->post('comentarios'),
	                    		"destacar" => ($admin ? $this->input->post('destacar') : 0),
	                    		"estatus" => ($admin ? $this->input->post('estatus') : 0),
	                    		"premiun" => ($admin ? $this->input->post('premiun') : 0),
	                    	]);
	                    	if($cancha){
	                    		$this->session->set_flashdata('mensaje', ["class"=>"success","text"=>"Se ha registrado una nueva cancha."]);
	                    		redirect(base_url().'auth/canchas');
	                    	}
	                    	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"no se pudo guardar los datos de la cancha."]);
	                    }
	                    else{
	                    	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"Ya existe una cancha con un seo igual."]);
	                    }
	                }
				}
				$this->load_auth('auth/canchas/nueva');
			break;
			// EDITAR CATEGORIA
			case 'editar':
				$this->usuarios_Model->is_login(['admin'], 'auth/canchas', 'Sin permisos para editar este contenido.');
				$opciones = $opciones ? $opciones : false;
				if($opciones){
					$this->data['canchas'] = $this->canchas_Model->list(
						null,
	                	null,
	                	null,
	                	null,
	                	["id"=>$opciones]
	                );
	                if($this->data['canchas']['total'] > 0){
						$this->data['servicios'] = $this->servicios_Model->list(null, null, ['estatus'=>1]);
						$this->data['autor'] = $this->usuarios_Model->list('id, nombre, apellido',null,null, ['roles !='=>'suscriptor']);
	                	if($this->input->method() == "post"){
							$this->form_validation->set_rules('nombre', 'Nombre', 'required', ['required' => '%s es requerido.']);
							$this->form_validation->set_rules('seo', 'Seo', 'required', ['required' => '%s es requerido.']);
							$this->form_validation->set_rules('resumen', 'Descripcion', 'required', ['required' => '%s es requerido.']);
							$this->form_validation->set_rules('pais', 'Pais', 'required', ['required' => '%s es requerido.']);
							$this->form_validation->set_rules('departamento', 'Departamento', 'required', ['required' => '%s es requerido.']);
							$this->form_validation->set_rules('provincia', 'Provincia', 'required', ['required' => '%s es requerido.']);
							$this->form_validation->set_rules('distrito', 'Distrito', 'required', ['required' => '%s es requerido.']);
							$this->form_validation->set_rules('direccion', 'Direccion', 'required', ['required' => '%s es requerido.']);
							$this->form_validation->set_rules('servicios[]', 'Servicios', 'required', ['required' => '%s es requerido.']);
							//$this->form_validation->set_rules('mapa', 'Mapa', 'required', ['required' => '%s es requerido.']);
							//$this->form_validation->set_rules('foto', 'Imagen', 'required', ['required' => '%s es requerido.']);
							//$this->form_validation->set_rules('video', 'Video', 'required', ['required' => '%s es requerido.']);
							$this->form_validation->set_rules('abre', 'Abre', 'required', ['required' => '%s es requerido.']);
							$this->form_validation->set_rules('cierra', 'Cierra', 'required', ['required' => '%s es requerido.']);
							$this->form_validation->set_rules('dia', 'Hora diurna', 'required', ['required' => '%s es requerido.']);
							$this->form_validation->set_rules('noche', 'Hora nocturna', 'required', ['required' => '%s es requerido.']);
							$this->form_validation->set_rules('canchas', 'Canchas', 'required', ['required' => '%s es requerido.']);
							$this->form_validation->set_rules('estrellas', 'Estrellas', 'required', ['required' => '%s es requerido.']);
							$this->form_validation->set_rules('telefono', 'Telefono', 'required', ['required' => '%s es requerido.']);
							//$this->form_validation->set_rules('pagina', 'Pagina', 'required', ['required' => '%s es requerido.']);
							//$this->form_validation->set_rules('facebook', 'Facebook', 'required', ['required' => '%s es requerido.']);
							//$this->form_validation->set_rules('twitter', 'Twitter', 'required', ['required' => '%s es requerido.']);
							//$this->form_validation->set_rules('instagran', 'Instagran', 'required', ['required' => '%s es requerido.']);
							//$this->form_validation->set_rules('autor', 'Autor', 'required', ['required' => '%s es requerido.']);
							//$this->form_validation->set_rules('comentarios', 'Comentarios', 'required', ['required' => '%s es requerido.']);
							//$this->form_validation->set_rules('destacar', 'Destacar', 'required', ['required' => '%s es requerido.']);
							//$this->form_validation->set_rules('estatus', 'Estatus', 'required', ['required' => '%s es requerido.']);
							//$this->form_validation->set_rules('premiun', 'Premiun', 'required', ['required' => '%s es requerido.']);
			                if($this->form_validation->run()){
			                    $buscar = $this->entradas_Model->count([
			                    	"seo"=>$this->input->post('seo'),
			                    	"id !=" => $opciones
			                    ]);
			                    if($buscar == 0){
			                    	$datos = [
			                    		"nombre" => $this->input->post('nombre'),
			                    		"seo" => $this->input->post('seo'),
			                    		"resumen" => $this->input->post('resumen'),
			                    		"direccion" => $this->input->post('direccion'),
			                    		"servicios" => implode(",",$this->input->post('servicios')),
			                    		"dia" => $this->input->post('dia'),
			                    		"noche" => $this->input->post('noche'),
			                    		"abre" => $this->input->post('abre'),
			                    		"cierra" => $this->input->post('cierra'),
			                    		"imagen" => ($this->input->post('foto') ? $this->input->post('foto') : "public/theme/auth/".THEME_AUTH."/images/icono-cancha.png"),
			                    		"pais" => $this->input->post('pais'),
			                    		"estado" => $this->input->post('departamento'),
			                    		"provincia" => $this->input->post('provincia'),
			                    		"distrito" => $this->input->post('distrito'),
			                    		"video" => $this->input->post('video'),
			                    		"mapa" => $this->input->post('mapa'),
			                    		"estrellas" => $this->input->post('estrellas'),
			                    		"telefonos" => $this->input->post('telefono'),
			                    		"facebook" => $this->input->post('facebook'),
			                    		"twitter" => $this->input->post('twitter'),
			                    		"instagran" => $this->input->post('instagran'),
			                    		"pagina" => $this->input->post('pagina'),
			                    		"canchas" => $this->input->post('canchas'),
			                    		"comentarios" => $this->input->post('comentarios'),
			                    	];
			                    	$admin =  $this->usuarios_Model->is_login(['admin'],null,null);
									if($admin){
										$datos['user'] = $this->input->post('autor');
										$datos['destacar'] = $this->input->post('destacar');
										$datos['estatus'] = $this->input->post('estatus');
										$datos['premiun'] = $this->input->post('premiun');
									}
			                    	$actualizar = $this->canchas_Model->update(
			                    		$datos,
			                    		["id"=>$opciones]
			                    	);
			                    	if($actualizar){
			                    		$this->session->set_flashdata('mensaje', ["class"=>"success","text"=>"Se ha actualizado los detalles de la cancha."]);
			                    		redirect(base_url().'auth/canchas/editar/'.$opciones);
			                    	}
			                    	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"Ya existe una cancha con los datos enviados."]);
			                    }
			                    else{
			                    	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"Ya existe una cancha igual."]);
			                    }
			                }
						}
	                }
	                else{
	                	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"No se ha encontrado la cancha que buscas."]);
	                	redirect(base_url().'auth/canchas');
	                }
					
				}
				else{
					$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"ingresa una cancha para editar."]);
					redirect(base_url().'auth/canchas');
				}
				$this->load_auth('auth/canchas/editar');
			break;
			// BORRAR cancha
			case 'borrar':
				$this->usuarios_Model->is_login(['admin'], 'auth/canchas', 'Sin permisos para borrar este contenido.');
				$opciones = $opciones ? $opciones : false;
				if($opciones){
					$buscar = $this->canchas_Model->count([
	                    "id"=>$opciones
	                ]);
	                if($buscar > 0){
	                	$canchas = $this->canchas_Model->delete([
	                    	"id"=>$opciones
	                	]);
	                	$this->session->set_flashdata('mensaje', ["class"=>"success","text"=>"Se ha borrado una cancha exitosamente."]);
	                }
	                else{
	                	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"No se ha encontrado la cancha que buscas."]);
	                }
					
				}
				else{
					$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"ingresa una cancha para borrar."]);
				}
				redirect(base_url().'auth/canchas');
			break;
			default:
				redirect(base_url().'auth/canchas');
			break;
		}
	}

	public function servicios($vista = null, $opciones = null){
		$this->usuarios_Model->is_login(['admin','editor']);
		$this->data['menu'] = ($this->session->userdata('roles')=="admin") ? 4 : 6;
		switch ($vista) {
			// LISTAR servicios
			case '':
				$page = 1;
				$this->data['paginate'] = $this->paginate_Model->paginate('servicios');
				$this->data['paginate']['active'] = $page;
				$start = ($page - 1) * $this->data['paginate']['limit'];
				$end   = $this->data['paginate']['limit'];
				$this->data['servicios'] = $this->servicios_Model->list($start,$end,null,['column'=>'id', 'dir'=>'desc']);
				$this->data['servicios']['total'] = $this->data['paginate']['itens'];
				$this->load_auth('auth/servicios/listar');
			break;
			case 'page':
				$page = intval($opciones) ? intval($opciones) : false;
				if((!$page) || ($page < 0))
					redirect(base_url().'auth/servicios');
				$this->data['paginate'] = $this->paginate_Model->paginate('servicios');
				$this->data['paginate']['active'] = $page;
				$start = ($page - 1) * $this->data['paginate']['limit'];
				$end   = $this->data['paginate']['limit'];
				$this->data['servicios'] = $this->servicios_Model->list($start,$end, null,['column'=>'id', 'dir'=>'desc']);
				$this->data['servicios']['total'] = $this->data['paginate']['itens'];
				$this->load_auth('auth/servicios/listar');
			break;
			// CREAR NUEVA servicios
			case 'nuevo':
				if($this->input->method() == "post"){
					$this->form_validation->set_rules('nombre', 'Nombre', 'required', ['required' => '%s es requerido.']);
	                $this->form_validation->set_rules('seo', 'Seo Url', 'required', ['required' => '%s es requerido.']);
	                if($this->form_validation->run()){
	                    $buscar = $this->servicios_Model->count([
	                    	"seo"=>$this->input->post('seo')
	                    ]);
	                    if($buscar == 0){
	                    	$insertar = $this->servicios_Model->insert([
	                    		"nombre" => $this->input->post('nombre'),
	                    		"seo" => $this->input->post('seo'),
	                    		"icono" => $this->input->post('icono'),
	                    		"estatus" => $this->input->post('estatus'),
	                    	]);
	                    	if($insertar){
	                    		$this->session->set_flashdata('mensaje', ["class"=>"success","text"=>"Se ha registrado un nuevo servicio."]);
	                    		redirect(base_url().'auth/servicios');
	                    	}
	                    	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"no se pudo guardar los datos del servicio."]);
	                    }
	                    else{
	                    	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"Ya existe un servicio igual."]);
	                    }
	                }
				}
				$this->load_auth('auth/servicios/nueva');
			break;
			// EDITAR CATEGORIA
			case 'editar':
				$this->usuarios_Model->is_login(['admin'], 'auth/servicios', 'Sin permisos para editar este contenido.');
				$opciones = $opciones ? $opciones : false;
				if($opciones){
					$this->data['servicios'] = $this->servicios_Model->list(
	                	null,
	                	null,
	                	["id"=>$opciones]
	                );
	                if($this->data['servicios']['total'] > 0){
	                	// VALIDAMOS QUE HA SOLICITADO ACTUALIZAR
	                	if($this->input->method() == "post"){
							$this->form_validation->set_rules('nombre', 'Nombre', 'required', ['required' => '%s es requerido.']);
			                $this->form_validation->set_rules('seo', 'Seo Url', 'required', ['required' => '%s es requerido.']);
			                if($this->form_validation->run()){
			                    $buscar = $this->categorias_Model->count([
			                    	"seo"=>$this->input->post('seo'),
			                    	"id !=" => $opciones
			                    ]);
			                    if($buscar == 0){
			                    	$actualizar = $this->servicios_Model->update(
			                    		[
			                    		"nombre" => $this->input->post('nombre'),
			                    		"seo" => $this->input->post('seo'),
			                    		"icono" => $this->input->post('icono'),
			                    		"estatus" => $this->input->post('estatus')
			                    		],
			                    		["id"=>$opciones]
			                    	);
			                    	if($actualizar){
			                    		$this->session->set_flashdata('mensaje', ["class"=>"success","text"=>"Se ha actualizado un nuevo servicios."]);
			                    		redirect(base_url().'auth/servicios');
			                    	}
			                    	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"Ya existe un servicio con los datos enviados."]);
			                    }
			                    else{
			                    	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"Ya existe un servicios igual."]);
			                    }
			                }
						}
	                }
	                else{
	                	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"No se ha encontrado el servicio que buscas."]);
	                	redirect(base_url().'auth/servicios');
	                }
					
				}
				else{
					$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"ingresa un servicio para editar."]);
					redirect(base_url().'auth/servicios');
				}
				$this->load_auth('auth/servicios/editar');
			break;
			// BORRAR CATEGORIA
			case 'borrar':
				$this->usuarios_Model->is_login(['admin'], 'auth/servicios', 'Sin permisos para borrar este contenido.');
				$opciones = $opciones ? $opciones : false;
				if($opciones){
					$buscar = $this->categorias_Model->count([
	                    "id"=>$opciones
	                ]);
	                if($buscar > 0){
	                	$borrar = $this->servicios_Model->delete([
	                    	"id"=>$opciones
	                	]);
	                	$this->session->set_flashdata('mensaje', ["class"=>"success","text"=>"Se ha borrado un servicio exitosamente."]);
	                }
	                else{
	                	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"No se ha encontrado el servicio que buscas."]);
	                }
					
				}
				else{
					$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"ingresa un servicio para borrar."]);
				}
				redirect(base_url().'auth/servicios');
			break;
			
			default:
				redirect(base_url().'auth/servicios');
			break;
		}
	}

	public function etiquetas($vista = null, $opciones = null){
		$this->usuarios_Model->is_login(['admin','editor']);
		$this->data['menu'] = ($this->session->userdata('roles')=="admin") ? 2 : 7;
		switch ($vista) {
			// LISTAR ETIQUETA
			case '':
				$page = 1;
				$this->data['paginate'] = $this->paginate_Model->paginate('etiqueta');
				$this->data['paginate']['active'] = $page;
				$start = ($page - 1) * $this->data['paginate']['limit'];
				$end   = $this->data['paginate']['limit'];
				$this->data['etiquetas'] = $this->etiquetas_Model->list($start,$end,null,['column'=>'id', 'dir'=>'desc']);
				$this->data['etiquetas']['total'] = $this->data['paginate']['itens'];
				$this->load_auth('auth/etiquetas/listar');
			break;
			case 'page':
				$page = intval($opciones) ? intval($opciones) : false;
				if((!$page) || ($page < 0))
					redirect(base_url().'auth/etiquetas');
				$this->data['paginate'] = $this->paginate_Model->paginate('etiqueta');
				$this->data['paginate']['active'] = $page;
				$start = ($page - 1) * $this->data['paginate']['limit'];
				$end   = $this->data['paginate']['limit'];
				$this->data['etiquetas'] = $this->galerias_Model->list($start,$end,null,['column'=>'id', 'dir'=>'desc']);
				$this->data['etiquetas']['total'] = $this->data['paginate']['itens'];
				$this->load_auth('auth/etiquetas/listar');
			break;
			// CREAR NUEVA ETIQUETA
			case 'nueva':
				if($this->input->method() == "post"){
					$this->form_validation->set_rules('nombre', 'Nombre', 'required', ['required' => '%s es requerido.']);
	                $this->form_validation->set_rules('seo', 'Seo Url', 'required', ['required' => '%s es requerido.']);
	                if($this->form_validation->run()){
	                    $buscar = $this->etiquetas_Model->count([
	                    	"seo"=>$this->input->post('seo')
	                    ]);
	                    if($buscar == 0){
	                    	$insertar = $this->etiquetas_Model->insert([
	                    		"nombre" => $this->input->post('nombre'),
	                    		"seo" => $this->input->post('seo'),
	                    		"estatus" => $this->input->post('estatus'),
	                    	]);
	                    	if($insertar){
	                    		$this->session->set_flashdata('mensaje', ["class"=>"success","text"=>"Se ha registrado una nueva etiqueta."]);
	                    		redirect(base_url().'auth/etiquetas');
	                    	}
	                    	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"no se pudo guardar los datos de la etiqueta."]);
	                    }
	                    else{
	                    	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"Ya existe una etiqueta igual."]);
	                    }
	                }
				}
				$this->load_auth('auth/etiquetas/nueva');
			break;
			// EDITAR ETIQUETA
			case 'editar':
				$this->usuarios_Model->is_login(['admin'], 'auth/etiquetas', 'Sin permisos para editar este contenido.');
				$opciones = $opciones ? $opciones : false;
				if($opciones){
					$this->data['etiquetas'] = $this->etiquetas_Model->list(
	                	null,
	                	null,
	                	["id"=>$opciones]
	                );
	                if($this->data['etiquetas']['total'] > 0){
	                	// VALIDAMOS QUE HA SOLICITADO ACTUALIZAR
	                	if($this->input->method() == "post"){
							$this->form_validation->set_rules('nombre', 'Nombre', 'required', ['required' => '%s es requerido.']);
			                $this->form_validation->set_rules('seo', 'Seo Url', 'required', ['required' => '%s es requerido.']);
			                if($this->form_validation->run()){
			                    $buscar = $this->etiquetas_Model->count([
			                    	"seo"=>$this->input->post('seo'),
			                    	"id !=" => $opciones
			                    ]);
			                    if($buscar == 0){
			                    	$actualizar = $this->etiquetas_Model->update(
			                    		[
			                    		"nombre" => $this->input->post('nombre'),
			                    		"seo" => $this->input->post('seo'),
			                    		"estatus" => $this->input->post('estatus')
			                    		],
			                    		["id"=>$opciones]
			                    	);
			                    	if($actualizar){
			                    		$this->session->set_flashdata('mensaje', ["class"=>"success","text"=>"Se ha actualizado una nueva etiqueta."]);
			                    		redirect(base_url().'auth/etiquetas');
			                    	}
			                    	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"Ya existe una etiqueta con los datos enviados."]);
			                    }
			                    else{
			                    	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"Ya existe una etiqueta igual."]);
			                    }
			                }
						}
	                }
	                else{
	                	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"No se ha encontrado la etiqueta que buscas."]);
	                	redirect(base_url().'auth/etiquetas');
	                }
					
				}
				else{
					$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"ingresa una etiqueta para editar."]);
					redirect(base_url().'auth/etiquetas');
				}
				$this->load_auth('auth/etiquetas/editar');
			break;
			// BORRAR ETIQUETA
			case 'borrar':
				$this->usuarios_Model->is_login(['admin'], 'auth/etiquetas', 'Sin permisos para borrar este contenido.');
				$opciones = $opciones ? $opciones : false;
				if($opciones){
					$buscar = $this->etiquetas_Model->count([
	                    "id"=>$opciones
	                ]);
	                if($buscar > 0){
	                	$borrar = $this->etiquetas_Model->delete([
	                    	"id"=>$opciones
	                	]);
	                	$this->session->set_flashdata('mensaje', ["class"=>"success","text"=>"Se ha borrado una etiqueta exitosamente."]);
	                }
	                else{
	                	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"No se ha encontrado la etiqueta que buscas."]);
	                }
					
				}
				else{
					$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"ingresa una etiqueta para borar."]);
				}
				redirect(base_url().'auth/etiquetas');
			break;
			
			default:
				redirect(base_url().'auth/etiquetas');
			break;
		}
	}

	public function galerias($vista = null, $opciones = null, $modal = false){
		$this->usuarios_Model->is_login(['admin','editor']);
		$this->data['menu'] = 4;
		switch ($vista) {
			// LISTAR GALERIAS
			case '':
				$page = 1;
				$this->data['paginate'] = $this->paginate_Model->paginate('galeria');
				$this->data['paginate']['active'] = $page;
				$start = ($page - 1) * $this->data['paginate']['limit'];
				$end   = $this->data['paginate']['limit'];
				$this->data['galerias'] = $this->galerias_Model->list($start,$end,null,['column'=>'id', 'dir'=>'desc']);
				$this->data['galerias']['total'] = $this->data['paginate']['itens'];
				$this->load_auth('auth/galerias/listar');
			break;
			case 'page':
				$page = intval($opciones) ? intval($opciones) : false;
				$modal = ($modal=="modal") ? true : false;
				if((!$page) || ($page < 0))
					redirect(base_url().'auth/galerias');
				$this->data['paginate'] = $this->paginate_Model->paginate('galeria');
				$this->data['paginate']['active'] = $page;
				$start = ($page - 1) * $this->data['paginate']['limit'];
				$end   = $this->data['paginate']['limit'];
				$this->data['galerias'] = $this->galerias_Model->list($start,$end,null,['column'=>'id', 'dir'=>'desc']);
				$this->data['galerias']['total'] = $this->data['paginate']['itens'];
				if(!$modal)
					$this->load_auth('auth/galerias/listar');
				if($modal)
					$this->load->view('auth/galerias/listar-modal', $this->data);
			break;
			// CREAR NUEVA GALERIAS
			case 'nueva':
				$modal = ($opciones=="modal") ? true : false;
				$form  = true;
				if($this->input->method() == "post"){
					$this->form_validation->set_rules('nombre', 'Descripcion', 'required', ['required' => '%s es requerido.']);
	                if($this->form_validation->run()){
	                	$tamano = explode("x", $this->input->post('tamano'));
	                    $config['upload_path'] = './public/img/galeria';
				        $config['allowed_types'] = 'gif|jpg|png';
				        $config['max_size'] = '2000';
				        $config['max_width'] = '2024';
				        $config['max_height'] = '2008';
				        $config['encrypt_name'] = TRUE;
				        $this->load->library('upload', $config);
				        //SI LA IMAGEN FALLA AL SUBIR MOSTRAMOS EL ERROR EN LA VISTA UPLOAD_VIEW
				        if(!$this->upload->do_upload('foto')){
				            $this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>str_replace("p", "span", $this->upload->display_errors())]);
				        }
				        else{
				        	$file = $this->upload->data();
				        	$insertar = $this->galerias_Model->insert([
		                    	"nombre" => $this->input->post('nombre'),
		                    	"path" => '/public/img/galeria/'.$file['file_name'],
		                    	'user' => $this->data['login']["id"],
		                    	'width' => $tamano[0],
		                    	'height' => $tamano[1]
	                    	]);
	                    	$this->load->library('image_lib');
							$config['image_library'] = 'gd2';
							$config['source_image'] = $file['full_path'];
							$config['new_image'] = './public/img/galeria';
							$config['maintain_ratio'] = false;
							$config['create_thumb'] = FALSE;
							$config['width'] = $tamano[0];
							$config['height'] = $tamano[1];
							$this->image_lib->initialize($config);
							if (!$this->image_lib->resize()) {
								echo $this->image_lib->display_errors();
							}
	                    	$this->session->set_flashdata('mensaje', ["class"=>"success","text"=>"Has cargado tu imagen exitosamente."]);
	                    	$form  = false;
	                    	if(!$modal)
								redirect(base_url().'auth/galerias');
							if($modal)
								$this->galerias('page',1,'modal');
	                    }
	                }
				}
				if($modal && $form)
					$this->load->view('auth/galerias/nueva-modal', $this->data);
				if(!$modal && $form)
					$this->load_auth('auth/galerias/nueva');
			break;
			// Borrar GALERIAS
			case 'borrar':
				$this->usuarios_Model->is_login(['admin'], 'auth/galerias', 'Sin permisos para borrar este contenido.');
				$opciones = $opciones ? $opciones : false;
				if($opciones){
					$buscar = $this->galerias_Model->list(
	                	null,
	                	null,
	                	["id"=>$opciones]
	                );
	                if($buscar['total'] > 0){
	                	$borrar = $this->galerias_Model->delete([
	                    	"id"=>$opciones
	                	]);
	                	unlink(".".$buscar['data'][0]->path);
	                	$this->session->set_flashdata('mensaje', ["class"=>"success","text"=>"Se ha borrado un elemento exitosamente."]);
	                }
	                else{
	                	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"No se ha encontrado el elemento que buscas."]);
	                }
					
				}
				else{
					$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"Selecciona un elemento para borrar."]);
				}
				redirect(base_url().'auth/galerias');
			break;
			
			default:
				redirect(base_url().'auth/galerias');
			break;
		}
	}

	public function usuarios($vista = null, $opciones = null){
		$this->usuarios_Model->is_login(['admin']);
		$this->data['menu'] = 3;
		switch ($vista) {
			// LISTAR CATEGORIAS
			case '':
				$page = 1;
				$this->data['paginate'] = $this->paginate_Model->paginate('user');
				$this->data['paginate']['active'] = $page;
				$start = ($page - 1) * $this->data['paginate']['limit'];
				$end   = $this->data['paginate']['limit'];
				$this->data['usuarios'] = $this->usuarios_Model->list(null,$start,$end,null,['column'=>'id', 'dir'=>'desc']);
				$this->data['usuarios']['total'] = $this->data['paginate']['itens'];
				$this->load_auth('auth/usuarios/listar');
			break;
			case 'page':
				$page = intval($opciones) ? intval($opciones) : false;
				if((!$page) || ($page < 0))
					redirect(base_url().'auth/usuarios');
				$this->data['paginate'] = $this->paginate_Model->paginate('user');
				$this->data['paginate']['active'] = $page;
				$start = ($page - 1) * $this->data['paginate']['limit'];
				$end   = $this->data['paginate']['limit'];
				$this->data['usuarios'] = $this->usuarios_Model->list(null,$start,$end,null,['column'=>'id', 'dir'=>'desc']);
				$this->data['usuarios']['total'] = $this->data['paginate']['itens'];
				$this->load_auth('auth/usuarios/listar');
			break;
			// CREAR NUEVA CATEGORIA
			case 'nuevo':
				$this->data['fecha'] = $this->input->post('fecha') ? $this->input->post('fecha') : date("Y-m-d");
				if($this->input->method() == "post"){
					$this->form_validation->set_rules('username', 'Username', 'required', ['required' => '%s es requerido.']);
	                $this->form_validation->set_rules('email', 'Email', 'required|valid_email', ['required' => '%s es requerido.']);
	                $this->form_validation->set_rules('sexo', 'Sexo', 'required', ['required' => '%s es requerido.']);
	                //$this->form_validation->set_rules('fecha', 'Fecha Nacimiento', 'required', ['required' => '%s es requerido.']);
	                //$this->form_validation->set_rules('apellido', 'Apellido', 'required', ['required' => '%s es requerido.']);
	                $this->form_validation->set_rules('password', 'Password', 'required', ['required' => '%s es requerido.']);
	                $this->form_validation->set_rules('grupo', 'Grupo', 'required', ['required' => '%s es requerido.']);
	                if($this->form_validation->run()){
	                    $buscar_email = $this->usuarios_Model->count([
	                    	"email"=>$this->input->post('email')
	                    ]);
	                    $buscar_username = $this->usuarios_Model->count([
	                    	"username"=>$this->input->post('username')
	                    ]);
	                    if($buscar_email == 0 && $buscar_username == 0){
	                    	$insertar = $this->usuarios_Model->insert([
	                    		"nombre" => $this->input->post('nombre'),
	                    		"apellido" => $this->input->post('apellido'),
	                    		"username" => $this->input->post('username'),
	                    		"email" => $this->input->post('email'),
	                    		"estatus" => $this->input->post('estatus'),
	                    		"sexo" => $this->input->post('sexo'),
	                    		"nacimiento" => $this->input->post('fecha'),
	                    		"password" => md5($this->input->post('password')),
	                    		"roles" => $this->input->post('grupo'),
	                    		"pais" => $this->input->post('pais'),
	                    	]);
	                    	$insertar = true;
	                    	if($insertar){
	                    		$this->session->set_flashdata('mensaje', ["class"=>"success","text"=>"Se ha registrado un nuevo usuario."]);
	                    		redirect(base_url().'auth/usuarios');
	                    	}
	                    	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"no se pudo guardar los datos del usuario."]);
	                    }
	                    if($buscar_email > 0 ){
	                    	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"El email ya no están disponible."]);
	                    }
	                    if($buscar_username > 0 ){
	                    	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"El username ya no está disponible."]);
	                    }
	                }
				}
				$this->data['paises'] = $this->paises_Model->list();
				$this->data['roles'] = $this->roles_Model->list();
				$this->load_auth('auth/usuarios/nueva');
			break;
			// EDITAR CATEGORIA
			case 'editar':
				$opciones = $opciones ? $opciones : false;
				if($opciones){
					$this->data['usuarios'] = $this->usuarios_Model->list(
						null,
	                	null,
	                	null,
	                	["id"=>$opciones]
	                );
	                if($this->data['usuarios']['total'] > 0){
	                	// VALIDAMOS QUE HA SOLICITADO ACTUALIZAR
	                	$this->data['roles'] = $this->roles_Model->list();
	                	$this->data['paises'] = $this->paises_Model->list();
	                	if($this->input->method() == "post"){
							$this->form_validation->set_rules('username', 'Username', 'required', ['required' => '%s es requerido.']);
			                $this->form_validation->set_rules('email', 'Email', 'required', ['required|valid_email' => '%s es requerido.']);
			                $this->form_validation->set_rules('sexo', 'Sexo', 'required', ['required' => '%s es requerido.']);
			                //$this->form_validation->set_rules('fecha', 'Fecha Nacimiento', 'required', ['required' => '%s es requerido.']);
			                //$this->form_validation->set_rules('apellido', 'Apellido', 'required', ['required' => '%s es requerido.']);
			                $this->form_validation->set_rules('grupo', 'Grupo', 'required', ['required' => '%s es requerido.']);
			                if($this->form_validation->run()){
			                    $buscar_email = $this->usuarios_Model->count([
			                    	"email"=>$this->input->post('email'),
			                    	"id !=" => $opciones
			                    ]);
			                    $buscar_username = $this->usuarios_Model->count([
			                    	"username"=>$this->input->post('username'),
			                    	"id !=" => $opciones
			                    ]);
			                    if($buscar_email == 0 && $buscar_username == 0){
			                    	$actualizar = $this->usuarios_Model->update(
			                    		[
				                    		"nombre" => $this->input->post('nombre'),
				                    		"apellido" => $this->input->post('apellido'),
				                    		"username" => $this->input->post('username'),
				                    		"email" => $this->input->post('email'),
				                    		"estatus" => $this->input->post('estatus'),
				                    		"sexo" => $this->input->post('sexo'),
				                    		"nacimiento" => $this->input->post('fecha'),
				                    		"roles" => $this->input->post('grupo'),
				                    		"pais" => $this->input->post('pais'),
				                    	],
			                    		["id"=>$opciones]
			                    	);
			                    	if($actualizar){
			                    		$this->session->set_flashdata('mensaje', ["class"=>"success","text"=>"Se ha actualizado un nueva usuario."]);
			                    		redirect(base_url().'auth/usuarios');
			                    	}
			                    	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"Ya existe un usuarios con los datos enviados."]);
			                    }
			                    if($buscar_email > 0 ){
			                    	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"El email ya no están disponible."]);
			                    }
			                    if($buscar_username > 0 ){
			                    	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"El username ya no está disponible."]);
			                    }
			                }
						}
	                }
	                else{
	                	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"No se ha encontrado el usuario que buscas."]);
	                	redirect(base_url().'auth/usuarios');
	                }
					
				}
				else{
					$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"ingresa un usuario para editar."]);
					redirect(base_url().'auth/categorias');
				}
				$this->load_auth('auth/usuarios/editar');
			break;
			// BORRAR USUARIO
			case 'borrar':
				$opciones = $opciones ? $opciones : false;
				if($opciones){
					$buscar = $this->usuarios_Model->count([
	                    "id"=>$opciones
	                ]);
	                if($buscar > 0){
	                	$borrar = $this->usuarios_Model->delete([
	                    	"id"=>$opciones
	                	]);
	                	$this->session->set_flashdata('mensaje', ["class"=>"success","text"=>"Se ha borrado un usuario exitosamente."]);
	                }
	                else{
	                	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"No se ha encontrado el usuario que buscas."]);
	                }
					
				}
				else{
					$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"ingresa un usuario para borar."]);
				}
				redirect(base_url().'auth/usuarios');
			break;
			
			default:
				redirect(base_url().'auth/usuarios');
			break;
		}
	}

	public function ajustes(){
		$this->usuarios_Model->is_login(['admin']);
		$this->data['menu'] = 5;
		redirect(base_url().'auth/menu');
		$this->load_auth();
	}

	public function menu($vista = null, $opciones = null){
		$this->usuarios_Model->is_login(['admin']);
		$this->data['menu'] = 5;
		switch ($vista) {
			// LISTAR CATEGORIAS
			case '':
				$page = 1;
				$this->data['paginate'] = $this->paginate_Model->paginate('menu');
				$this->data['paginate']['active'] = $page;
				$start = ($page - 1) * $this->data['paginate']['limit'];
				$end   = $this->data['paginate']['limit'];
				$this->data['menus'] = $this->menu_Model->list($start,$end,null,['column'=>'id', 'dir'=>'desc']);
				$this->data['menus']['total'] = $this->data['paginate']['itens'];
				$this->load_auth('auth/menu/listar');
			break;
			case 'page':
				$page = intval($opciones) ? intval($opciones) : false;
				if((!$page) || ($page < 0))
					redirect(base_url().'auth/menu');
				$this->data['paginate'] = $this->paginate_Model->paginate('menu');
				$this->data['paginate']['active'] = $page;
				$start = ($page - 1) * $this->data['paginate']['limit'];
				$end   = $this->data['paginate']['limit'];
				$this->data['menus'] = $this->categorias_Model->list($start,$end, null,['column'=>'id', 'dir'=>'desc']);
				$this->data['menus']['total'] = $this->data['paginate']['itens'];
				$this->load_auth('auth/menu/listar');
			break;
			// CREAR NUEVA CATEGORIA
			case 'nuevo':
				if($this->input->method() == "post"){
					$this->form_validation->set_rules('nombre', 'Nombre', 'required', ['required' => '%s es requerido.']);
	                $this->form_validation->set_rules('path', 'Path Url', 'required', ['required' => '%s es requerido.']);
	                if($this->form_validation->run()){
	                    $insertar = $this->menu_Model->insert([
	                    	"nombre" => $this->input->post('nombre'),
	                    	"path" => $this->input->post('path'),
	                    	"icono" => $this->input->post('icono'),
	                    	"estatus" => $this->input->post('estatus'),
	                    	"target" => $this->input->post('target'),
	                    	"externo" => $this->input->post('externo'),
	                    ]);
	                    if($insertar){
	                    	$this->session->set_flashdata('mensaje', ["class"=>"success","text"=>"Se ha registrado un nuevo menu."]);
	                    	redirect(base_url().'auth/menu');
	                    }
	                    $this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"no se pudo guardar los datos del menu."]);
	                }
				}
				$this->load_auth('auth/menu/nueva');
			break;
			// EDITAR CATEGORIA
			case 'editar':
				$opciones = $opciones ? $opciones : false;
				if($opciones){
					$this->data['menus'] = $this->menu_Model->list(
	                	null,
	                	null,
	                	["id"=>$opciones]
	                );
	                if($this->data['menus']['total'] > 0){
	                	// VALIDAMOS QUE HA SOLICITADO ACTUALIZAR
	                	if($this->input->method() == "post"){
							$this->form_validation->set_rules('nombre', 'Nombre', 'required', ['required' => '%s es requerido.']);
			                $this->form_validation->set_rules('path', 'Path Url', 'required', ['required' => '%s es requerido.']);
			                if($this->form_validation->run()){			                   
		                    	$actualizar = $this->menu_Model->update(
									[
				                    	"nombre" => $this->input->post('nombre'),
				                    	"path" => $this->input->post('path'),
				                    	"icono" => $this->input->post('icono'),
				                    	"estatus" => $this->input->post('estatus'),
				                    	"target" => $this->input->post('target'),
				                    	"externo" => $this->input->post('externo'),
	                    			],
		                    		["id"=>$opciones]
		                    	);
		                    	if($actualizar){
		                    		$this->session->set_flashdata('mensaje', ["class"=>"success","text"=>"Se ha actualizado un nuev menu."]);
		                    		redirect(base_url().'auth/menu');
		                    	}
		                    	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"No se pudieron actualizar los datos."]);
			                }
						}
	                }
	                else{
	                	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"No se ha encontrado el menu que buscas."]);
	                	redirect(base_url().'auth/menu');
	                }
					
				}
				else{
					$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"ingresa un menu para editar."]);
					redirect(base_url().'auth/menu');
				}
				$this->load_auth('auth/menu/editar');
			break;
			// BORRAR CATEGORIA
			case 'borrar':
				$opciones = $opciones ? $opciones : false;
				if($opciones){
					$buscar = $this->menu_Model->count([
	                    "id"=>$opciones
	                ]);
	                if($buscar > 0){
	                	$borrar = $this->menu_Model->delete([
	                    	"id"=>$opciones
	                	]);
	                	$this->session->set_flashdata('mensaje', ["class"=>"success","text"=>"Se ha borrado un itens de menu exitosamente."]);
	                }
	                else{
	                	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"No se ha encontrado el itens que buscas."]);
	                }
					
				}
				else{
					$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"ingresa un itens para borar."]);
				}
				redirect(base_url().'auth/menu');
			break;
			
			default:
				redirect(base_url().'auth/menu');
			break;
		}
	}

	public function login(){
		if($this->data['login']['signin']){
			redirect(base_url().'auth');
		}
		if($this->input->method() == "post"){
			$this->form_validation->set_rules('email', 'Email', 'required', ['required' => '%s es requerido.']);
	        $this->form_validation->set_rules('password', 'Password', 'required', ['required' => '%s es requerido.']);
	        if($this->form_validation->run()){
	        	$usuario = $this->usuarios_Model->list(
	        		null,
	        		null,
	        		null,
	        		[
	        			'email' => $this->input->post('email'),
	        			'password' => md5($this->input->post('password')),
	        		]
	        	);
	        	if($usuario['total'] > 0){
	        		if($usuario['data'][0]->estatus){
	        			$data = [
				          	'signin' => true,
				            'id' => $usuario['data'][0]->id,
				            'roles' => $usuario['data'][0]->roles,
							'email' => $usuario['data'][0]->email,
							'nombre' => $usuario['data'][0]->nombre,
							'web' => $usuario['data'][0]->web,
							'apellido' => $usuario['data'][0]->apellido,
				            'username' => $usuario['data'][0]->username,
				            'avatar' => ($usuario['data'][0]->avatar ? $usuario['data'][0]->avatar : '/public/img/avatar/default.jpeg')
				        ];
				 		$this->session->set_userdata($data);
				 		$this->session->set_flashdata('mensaje', ["class"=>"success","text"=>"Bienvenido, gracias por regresar."]);
	        			redirect(base_url().'auth');
	        		}
	        		else{
	        			$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"Disculpa, cuenta inhabilitada."]);
	        		}
	        		
	        	}
	        	else{
	        		$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"Revisa tu email y password."]);
	        	}
	        }
		}
		$this->data['menu'] = 0;
		$this->load_front('auth/login/login');
	}

	public function logout(){
		$this->session->sess_destroy();
        redirect(base_url());
	}

	public function signup(){
		if($this->data['login']['signin']){
			redirect(base_url().'auth');
		}
		$this->data['menu'] = 0;
		$this->data['fecha'] = $this->input->post('fecha') ? $this->input->post('fecha') : date("Y-m-d");
		if($this->input->method() == "post"){
			$this->form_validation->set_rules('username', 'Username', 'required', ['required' => '%s es requerido.']);
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email', ['required' => '%s es requerido.']);
            $this->form_validation->set_rules('password', 'Password', 'required', ['required' => '%s es requerido.']);
            if($this->form_validation->run()){
                $buscar_email = $this->usuarios_Model->count([
                	"email"=>$this->input->post('email')
                ]);
                $buscar_username = $this->usuarios_Model->count([
                	"username"=>$this->input->post('username')
                ]);
                if($buscar_email == 0 && $buscar_username == 0){
                	$insertar = $this->usuarios_Model->insert([
                		"username" => $this->input->post('username'),
                		"email" => $this->input->post('email'),
                		"estatus" => 1,
                		"nacimiento" => $this->data['fecha'],
                		"password" => md5($this->input->post('password')),
                		"roles" => 'suscriptor',
                		"avatar" => '/public/img/avatar/default.jpeg'
                	]);
                	$insertar = true;
                	if($insertar){
                		$this->session->set_flashdata('mensaje', ["class"=>"success","text"=>"Se ha registrado con éxito."]);
                		redirect(base_url().'auth/login');
                	}
                	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"no se pudo guardar los datos del usuario."]);
                }
                if($buscar_email > 0 ){
                	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"El email ya no están disponible."]);
                }
                if($buscar_username > 0 ){
                	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"El username ya no está disponible."]);
                }
            }
		}
		$this->load_front('auth/login/signup');
	}

	public function perfil($vista = null, $opciones = null){
		$this->usuarios_Model->is_login();
		$this->data['menu'] = 0;
		switch ($vista) {
			case '':
				$opciones = $this->session->userdata('id') ? $this->session->userdata('id') : false;
				if($opciones){
					$this->data['usuarios'] = $this->usuarios_Model->list(
						null,
	                	null,
	                	null,
	                	["id"=>$opciones]
	                );
	                if($this->data['usuarios']['total'] > 0){
	                	// VALIDAMOS QUE HA SOLICITADO ACTUALIZAR
	                	$this->data['paises'] = $this->paises_Model->list();
	                	if($this->input->method() == "post"){
							$this->form_validation->set_rules('username', 'Username', 'required', ['required' => '%s es requerido.']);
			                $this->form_validation->set_rules('email', 'Email', 'required', ['required|valid_email' => '%s es requerido.']);
			                $this->form_validation->set_rules('sexo', 'Sexo', 'required', ['required' => '%s es requerido.']);
			                $this->form_validation->set_rules('fecha', 'Fecha Nacimiento', 'required', ['required' => '%s es requerido.']);
			                $this->form_validation->set_rules('apellido', 'Apellido', 'required', ['required' => '%s es requerido.']);
			                if($this->form_validation->run()){
			                    $buscar_email = $this->usuarios_Model->count([
			                    	"email"=>$this->input->post('email'),
			                    	"id !=" => $opciones
			                    ]);
			                    $buscar_username = $this->usuarios_Model->count([
			                    	"username"=>$this->input->post('username'),
			                    	"id !=" => $opciones
			                    ]);
			                    if($buscar_email == 0 && $buscar_username == 0){
			                    	$tamano = explode("x", "200x200");
				                    $config['upload_path'] = './public/img/avatar';
							        $config['allowed_types'] = 'gif|jpg|png';
							        $config['max_size'] = '2000';
							        $config['max_width'] = '2024';
							        $config['max_height'] = '2008';
							        $config['encrypt_name'] = TRUE;
							        $this->load->library('upload', $config);
							        //SI LA IMAGEN FALLA AL SUBIR MOSTRAMOS EL ERROR EN LA VISTA UPLOAD_VIEW
							        if(!$this->upload->do_upload('foto')){
							            $this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>str_replace("p", "span", $this->upload->display_errors())]);
							        }
							        else{
							        	$file = $this->upload->data();
				                    	$this->load->library('image_lib');
										$config['image_library'] = 'gd2';
										$config['source_image'] = $file['full_path'];
										$config['new_image'] = './public/img/avatar';
										$config['maintain_ratio'] = false;
										$config['create_thumb'] = FALSE;
										$config['width'] = $tamano[0];
										$config['height'] = $tamano[1];
										$this->image_lib->initialize($config);
										if (!$this->image_lib->resize()) {
											//echo $this->image_lib->display_errors();
										}
				                    }
				                    $datos = [
				                    		"nombre" => $this->input->post('nombre'),
				                    		"apellido" => $this->input->post('apellido'),
				                    		"username" => $this->input->post('username'),
				                    		"email" => $this->input->post('email'),
				                    		"sexo" => $this->input->post('sexo'),
				                    		"nacimiento" => $this->input->post('fecha'),
				                    		"pais" => $this->input->post('pais'),
				                    		"facebook" => $this->input->post('facebook'),
				                    		"twitter" => $this->input->post('twitter'),
				                    		"google" => $this->input->post('google'),
				                    		"linkedin" => $this->input->post('linkedin'),
				                    		"whatsapp" => $this->input->post('whatsapp'),
				                    		"about" => $this->input->post('about'),
				                    		"web" => $this->input->post('web'),
 				                    ];
 				                    if(isset($file['file_name']))
 				                    	$datos['avatar'] = 'public/img/avatar/'.$file['file_name'];
				                    $actualizar = $this->usuarios_Model->update(
			                    		$datos,
				                    	["id"=>$opciones]
				                    );
				                    if($actualizar){
				                    	$this->session->sess_destroy();
				                    	$this->session->set_flashdata('mensaje', ["class"=>"success","text"=>"Se han actualizado los datos exitosamente."]);
				                    	redirect(base_url().'auth/login');
				                    }
				                    else{
			                    		$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"Ya existe un usuario con los datos enviados."]);
				                    }
			                    }
			                    if($buscar_email > 0 ){
			                    	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"El email ya no están disponible."]);
			                    }
			                    if($buscar_username > 0 ){
			                    	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"El username ya no está disponible."]);
			                    }
			                }
						}
	                }
	                else{
	                	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"No se ha encontrado el usuario que buscas."]);
	                	redirect(base_url().'auth/perfil/editar');
	                }
					
				}
				else{
					$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"ingresa un usuario para editar."]);
					redirect(base_url().'auth/perfil');
				}
				$this->load_auth('auth/perfil/editar',null,'front');
			break;

			case 'clave':
				$opciones = $this->session->userdata('id') ? $this->session->userdata('id') : false;
				if($opciones){
					$this->data['usuarios'] = $this->usuarios_Model->list(
						null,
	                	null,
	                	null,
	                	["id"=>$opciones]
	                );
	                if($this->data['usuarios']['total'] > 0){
	                	// VALIDAMOS QUE HA SOLICITADO ACTUALIZAR
	                	if($this->input->method() == "post"){
							$this->form_validation->set_rules('password', 'Clave actual', 'required', ['required' => '%s es requerido.']);
							$this->form_validation->set_rules('clave', 'Clave nueva', 'required', ['required' => '%s es requerido.']);
							$this->form_validation->set_rules('claver', 'Repite nueva', 'required', ['required' => '%s es requerido.']);
			                if($this->form_validation->run()){
			                	if($this->data['usuarios']['data'][0]->password != md5($this->input->post('password'))){
			                		$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"La clave ingresada no es la correcta."]);
			                    		redirect(base_url().'auth/perfil/clave');
			                	}
			                	if($this->input->post('clave') != $this->input->post('claver')){
			                		$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"las claves nuevas no son iguales."]);
			                    		redirect(base_url().'auth/perfil/clave');
			                	}
			                    $actualizar = $this->usuarios_Model->update(
			                    	[
				                    	"password" => md5($this->input->post('clave')),
				                    ],
			                    	["id"=>$opciones]
			                    );
			                    if($actualizar){
			                    		$this->session->set_flashdata('mensaje', ["class"=>"success","text"=>"Se han actualizado los datos exitosamente."]);
			                    		redirect(base_url().'auth/logout');
			                    }
			                    $this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"No se pudo actualizar los datos."]);
			                }
						}
	                }
	                else{
	                	$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"No se ha encontrado el usuario que buscas."]);
	                	redirect(base_url().'auth/perfil/clave');
	                }
					
				}
				else{
					$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"ingresa un usuario para editar."]);
					redirect(base_url().'auth/perfil/clave');
				}
				$this->load_auth('auth/perfil/clave',null,'front');
			break;
			
			default:
				redirect(base_url().'auth/perfil');
			break;
		}
	}
}
