<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . 'core/My_Controller.php';

class Canchas extends My_Controller {

	public function index(){
		$this->data['menu'] = 2;
		$page = 1;
		$this->data['paginate'] = $this->paginate_Model->paginate('cancha');
		$this->data['paginate']['active'] = $page;
		$start = ($page - 1) * $this->data['paginate']['limit'];
		$end   = $this->data['paginate']['limit'];
		$this->data['canchas'] = $this->canchas_Model->list('canchas_view',null,$start,$end, null, ['column'=>'id', 'dir'=>'desc']);
		$this->data['canchas']['total'] = $this->data['paginate']['itens'];
		$this->data['canchas']['filtred'] = count($this->data['canchas']['data']);
		$this->load_front(
			'front/canchas/list'
		);
	}

	public function page($page = null){
		$this->data['menu'] = 2;
		$page = intval($page) ? intval($page) : false;
		if((!$page) || ($page < 0))
			redirect(base_url().'canchas');
		$this->data['paginate'] = $this->paginate_Model->paginate('cancha');
		$this->data['paginate']['active'] = $page;
		$start = ($page - 1) * $this->data['paginate']['limit'];
		$end   = $this->data['paginate']['limit'];
		$this->data['canchas'] = $this->canchas_Model->list('canchas_view',null,$start,$end, null, ['column'=>'id', 'dir'=>'desc']);
		$this->data['canchas']['total'] = $this->data['paginate']['itens'];
		$this->data['canchas']['filtred'] = count($this->data['canchas']['data']);
		if(!empty($this->data['canchas']['data'])){
			$this->load_front(
				'front/canchas/list'
			);
		}
		else{
			$this->load_front(
				'front/home/error'
			);
		}	
	}

	public function detalle($seo = null){
		$this->data['menu'] = 2;
		$page = 1;
		
		$this->data['detalle'] = $this->canchas_Model->list('canchas_view',null,null,null, ['seo'=>$seo], ['column'=>'id', 'dir'=>'desc']);
		
		if(!empty($this->data['detalle']['data']) && $seo != null){
			if($this->input->method() == "post"){
				$this->form_validation->set_rules('comentar_nombre', 'Nombre', 'required', ['required' => 'El %s es requerido.']);
				$this->form_validation->set_rules('comentar_email', 'Email', 'required', ['required' => 'El %s es requerido.']);
				$this->form_validation->set_rules('comentar_comentario', 'Comentarios', 'required|min_length[20]|max_length[500]', ['required' => 'Escribe un comentario.','min_length' => 'Escribe al menos 20 caracteres.','max_length' => 'Escribe menos de 500 caracteres.']);
				$this->form_validation->set_rules('estrellas', 'Estrellas', 'required', ['required' => 'El %s es requerido.']);
				if($this->form_validation->run()){
					$registrar_comentario = $this->canchas_Model->insert_comentario([
	                    "estrellas" => $this->input->post('estrellas'),
	                    "comentario" => $this->input->post('comentar_comentario'),
	                    "cancha" => $this->data['detalle']['data'][0]->id,
	                    "autor" => $this->session->userdata('id'),
	                    "estatus" => 1
	                ]);
	                if($registrar_comentario){
	                	$this->session->set_flashdata('mensaje', ["class"=>"success","text"=>"Gracias, Se ha registrado tu comentario."]);
	                }
				}
			}
			$this->data['canchas'] = $this->canchas_Model->list('canchas_view',null,0,5, ['destacar'=>1], ['column'=>'id', 'dir'=>'desc']);
			$this->data['premiuns'] = $this->canchas_Model->list('canchas_view',null,0,5, ['premiun'=>1], ['column'=>'id', 'dir'=>'desc']);
			$this->data['comentarios'] = $this->canchas_Model->list('canchas_comentarios_view',null,null,null, ['cancha'=>$this->data['detalle']['data'][0]->id], ['column'=>'id', 'dir'=>'desc']);
			$this->load_front(
				'front/canchas/detalle'
			);
		}
		else{
			$this->load_front(
				'front/home/error'
			);
		}
	}

}
