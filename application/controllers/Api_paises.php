<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . 'core/My_Controller.php';

class Api_paises extends My_Controller {

	public function index(){
		$data = [];
		$data = ["listar","region","provincia","distrito"];
		$data = json_encode($data);
		echo $data;
	}

	public function listar($col = null, $id = null){
		$data  = [];
		$where = "";
		if($col && $id)
			$where = "WHERE P.".$col." = '".$id."'";
		$data = $query = $this->db->query("SELECT P.* FROM pais P ".$where)->result();
		$data = json_encode($data);
		echo $data;
	}

	public function estado($col = null, $id = null){
		$data  = [];
		$where = "";
		if($col && $id)
			$where = "WHERE P.".$col." = '".$id."'";
		$data = $query = $this->db->query("SELECT P.* FROM pais_estado P ".$where)->result();
		$data = json_encode($data);
		echo $data;
	}

	public function provincia($col = null, $id = null){
		$data  = [];
		$where = "";
		if($col && $id)
			$where = "WHERE P.".$col." = '".$id."'";
		$data = $query = $this->db->query("SELECT P.* FROM pais_provincia P ".$where)->result();
		$data = json_encode($data);
		echo $data;
	}

	public function distrito($col = null, $id = null){
		$data  = [];
		$where = "";
		if($col && $id)
			$where = "WHERE P.".$col." = '".$id."'";
		$data = $query = $this->db->query("SELECT P.* FROM pais_distrito P ".$where)->result();
		$data = json_encode($data);
		echo $data;
	}

}
